/**
 * @format
 */

// import {AppRegistry} from 'react-native';
// import App from './App';
// import {name as appName} from './app.json';

// AppRegistry.registerComponent(appName, () => App);



/**
 * @format
 */
import 'react-native-gesture-handler';
/**
 * @format
 */
import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import App from './App';
import configureStore from './configureStore';
const store = configureStore();

export default class Emtyaz extends Component {
    render() {
        return (<Provider store={store}>
            <App />
        </Provider>
        );
    }
}
AppRegistry.registerComponent(appName, () => Emtyaz);
