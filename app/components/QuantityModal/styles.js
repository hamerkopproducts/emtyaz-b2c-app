import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";

const styles = StyleSheet.create({


  quantityModalView:{
    margin:0,
},
quantityModalContent:{
  //height: '40%',
  marginTop: 'auto',
  backgroundColor:'white',
  paddingLeft:'5%',
  paddingRight:'5%',
  paddingBottom:'3%',
  borderWidth:1,
  borderTopLeftRadius:20,
  borderTopRightRadius:20,

},
closeButton:{
  //justifyContent:'flex-end',
  alignItems:'flex-end',
  alignSelf:'flex-end',
  paddingRight:'3%',
   paddingTop:'4%',
  // paddingRight:'3%',
    paddingLeft:'3%',
  //   //paddingTop:'2%',
    paddingBottom:'2%',
},
closeText:{
  fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
    fontSize: 20,
    color:'grey',
},
quantityHeading:{
  flexDirection: 'row',
  paddingTop:'8%',
  paddingBottom:'2%',
},
quantityHeadingStyle:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
fontSize: 17,
},
lineOne:{
  flexDirection:'row',
  justifyContent:'space-between',
  paddingTop:'3%',
  paddingBottom:'3%',
},
smallText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
  fontSize: 14,
},
butView:{
 // paddingLeft:'30%',
 // paddingRight:'2%',
  marginLeft:'40%',
    backgroundColor: 'green',
    width: 52,
    height: 28,
    justifyContent: "center",
    alignItems: "center",
    
},
saveText:{
  textAlign:'center',
  color: globals.COLOR.white,
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  fontSize: 12,
  
},
rest:{
 // paddingLeft:'5%',
  //paddingRight:'5%',
  backgroundColor: 'orange',
width: 52,
height: 28,
justifyContent: "center",
alignItems: "center",
marginLeft:'4%',
},

});

export { styles };