import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text,TouchableOpacity} from 'react-native';
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import Modal from 'react-native-modal';
import CustomRadioButton from '../../components/CustomRadioButton';


const QuantityModal = (props) => {
  const {
        toggleQuantityModal,
        isQuantityModalVisible
      } = props;
      


  return (
    <Modal animationIn="slideInUp" 
    animationOut="slideOutRight" 
  
    swipeDirection={["left", "right", "up", "down"]}
    isVisible={isQuantityModalVisible} 
    style={styles.quantityModalView}
    >
      <View style={styles.quantityModalContent}>
      <View style={styles.closeButton}>
            <TouchableOpacity onPress={() => {toggleQuantityModal();}}>
              <Text style={styles.closeText}>X</Text>
            </TouchableOpacity>
          </View> 
        <View style={styles.quantityHeading}>
          <Text style={styles.quantityHeadingStyle}>{appTexts.DETAILSSCREEN.heading}</Text>
          <View style={styles.butView}>
          <Text style={styles.saveText}>{appTexts.REQUEST.Save}
          </Text>
          </View>
          <View style={styles.rest}>
          <Text style={styles.saveText}>{appTexts.CATEGORY_CONTENT.Button2}</Text>
          </View>
          {/* <TouchableOpacity onPress={() => {toggleQuantityModal();}}>
          <View style={styles.clooo}>
          <Text style={styles.saveText}>{appTexts.QUANTITY.close}</Text>
          </View>
          </TouchableOpacity> */}
        </View>
        <View style={styles.lineOne}>
          <Text style={styles.smallText}>{appTexts.DETAILSSCREEN.OptionOne}</Text>
          <CustomRadioButton />
        </View>
        <View style={styles.lineOne}>
          <Text style={styles.smallText}>{appTexts.DETAILSSCREEN.OptionTwo}</Text>
          <CustomRadioButton />
        </View>
        <View style={styles.lineOne}>
          <Text style={styles.smallText}>{appTexts.DETAILSSCREEN.OptionThree}</Text>
          <CustomRadioButton />
        </View>
        <View style={styles.lineOne}>
          <Text style={styles.smallText}>{appTexts.DETAILSSCREEN.OptionFour}</Text>
          <CustomRadioButton />
        </View>
        <View style={styles.lineOne}>
          <Text style={styles.smallText}>{appTexts.DETAILSSCREEN.OptionFive}</Text>
          <CustomRadioButton />
        </View>
        <View style={styles.lineOne}>
          <Text style={styles.smallText}>{appTexts.DETAILSSCREEN.OptionSix}</Text>
          <CustomRadioButton />
        </View>
      </View>
    </Modal>
    
  );
};
QuantityModal.propTypes = {
  
  
};

export default QuantityModal;