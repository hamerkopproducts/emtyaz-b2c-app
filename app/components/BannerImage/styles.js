import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  bannerContainer: {
    width: '100%',
    height:100,
  },
  imageContainer:{
    width: '100%',
    height: '100%'
  },
  imageStyle:{
    width: '100%',
    height: '100%'
  }
});

export { styles };
