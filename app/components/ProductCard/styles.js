import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  itemContainer: {
     width: (globals.INTEGER.screenWidthWithMargin - globals.INTEGER.leftRightMargin) / 2,
     marginLeft: globals.INTEGER.leftRightMargin,
    marginBottom: globals.INTEGER.leftRightMargin - 5,
    alignItems: 'center',
  //  borderColor:'#ddd',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.2,
    elevation: Platform.OS === 'ios' ? 1: 3,
    backgroundColor:  '#ffffff',
    marginTop:'1%',
    
    
  },
  labelView: {
    width: '90%',
    height: 25,
    flexDirection: 'row',
    
  },
  discountTextContainer: {
    width: '90%',
    height: 40,
    justifyContent: 'center'
  },
  textContainer:{
    marginLeft:'2%',
    marginRight: '2%',
    width: '85%',
    marginVertical:'3%',
    //flexDirection: 'row',
    //backgroundColor:'red'
  },
  favIconContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    width: '10%',
    height: 40
  },
  image: {
    height: 100,
    width: 118,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  discountText:{
    color: globals.COLOR.greenTexeColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsLight,
    fontSize: 11
  },
  nameText:{
    
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 10.2
  },
  ratingRow:{
    flexDirection:'row',
    marginTop:'4%',
   // marginBottom:"1%",
  },
  numberRating:{
    fontSize:10,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  star:{
    paddingLeft:'1%',
   alignSelf:'center',
  },
  starStyle:{
    width:10,
    height:10,
    resizeMode:'contain',
  },
  ratingK:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize:10
  },
  eye:{
    paddingLeft:'1%',
    alignSelf:'center',
  },
  eyeStyle:{
    width:10,
    height:10,
    resizeMode:'contain',
  },

  
  secondContainer: {
    width: '90%',
    height: 35,
    //alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop:'3%',
    paddingLeft:'2%',
    
  },
  thirdContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    width:'100%',
    paddingLeft:'6%',
    paddingBottom:'5%',
   // paddingTop:'0%',
    paddingRight:'5%',
  },
  priceStyle:{
    justifyContent: 'flex-start',
    
    
  },
  lightPrice:{
    fontSize:10,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    color:globals.COLOR.lightGray,
    textDecorationStyle: 'solid', 
    textDecorationLine: 'line-through',
  },
  darkPrice:{
    fontSize:12,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
    color:'black',
  },

  plus:{
    alignSelf:'center',
    paddingLeft:'10%',
    paddingBottom:"2%"
  },
  greenContainer:{
    height: 14,
    width: 14,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: globals.COLOR.greenTexeColor,
    borderWidth:1
  },
  greenDot: {
    height:5,
    width: 5,
    backgroundColor: globals.COLOR.greenTexeColor,
    borderRadius: 5
  },
  sizeSelectView: {
    marginRight: 10,
    width:55,
    height: 25,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.lightGray,
    borderWidth: 1
  },
  sizeText: {
    marginRight: 10,
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 9.5
  },
  dropDownArrow:{
    width:10,
    height:10,
    marginRight:1
  }
});

export { styles };
