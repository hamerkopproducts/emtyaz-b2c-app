import React, { Component,useState } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text,TouchableOpacity,I18nManager} from 'react-native';
import { styles } from "./styles";
import DotedContainerItem from "./DotedContainerItem";
import MoreItem from "./MoreItem";
import PlusButton from "./PlusButton";
import DividerLine from "../DividerLine";
import BannerModal from '../../components/BannerModal';
import appTexts from '../../lib/appTexts';
const ProductCard = (props) => {
  const {
        item,
    itemClick
      } = props;

      const [isBannerModalVisible, setIsBannerModalVisible] = useState(false);

      const closeBannerModal = () => {
        setIsBannerModalVisible(false)
      };
      const openBannerModal = () => {
        setIsBannerModalVisible(true)
      };

  return (
    <TouchableOpacity style={styles.itemContainer} onPress={() => { itemClick() }}>
      <View style={styles.labelView}>
        <View style={styles.discountTextContainer}>
          <Text style={styles.discountText}>
          {(I18nManager.isRTL ? 'الخصم %30' :'30% Discount')}
          </Text>
        </View>
         <View style={styles.favIconContainer}>
          <Image source={item.isFav ? require('../../assets/images/products/heart-active.png') : require('../../assets/images/products/favourite.png')} />
        </View> 
      </View>
      <View style={styles.imageContainer}>
        <Image resizeMode="contain" source={item.image} style={styles.image} />
      </View>
      <View style={styles.textContainer}>
        {/* <View style={styles.greenContainer}>
          <View style={styles.greenDot}></View>
        </View> */}
        <Text style={styles.nameText}>{item.name}</Text>
        <View style={styles.ratingRow}>
        <Text style={styles.numberRating}>4.2</Text>
        <View style={styles.star}>
          <Image  style={styles.starStyle}source={require ('../../assets/images/home/starfill.png')}></Image>
        </View>
        <View style={{paddingLeft:'8%'}}>
        <Text style={styles.ratingK}>1.3K</Text>
        </View>
        <View style={styles.eye}>
          <Image style={styles.eyeStyle} source={require ('../../assets/images/home/visibility.png')}></Image>
        </View>
      </View>
      </View>
      <BannerModal isBannerModalVisible={isBannerModalVisible} closeModal={closeBannerModal} bannerText={appTexts.BANNER.WishList}/>
      <View style={styles.secondContainer}>
      
          <View style={styles.sizeSelectView}>
            <Text style={styles.sizeText}>{'Small'}</Text>
            <Image style={styles.dropDownArrow} source={require('../../assets/images/home/down-arrown.png')} />
            </View> 
        {/*<DotedContainerItem />*/}
        {/*<PlusButton />*/}
      </View>
      <View style={styles.thirdContainer}>
        <View style={styles.priceStyle}>
          <Text style={styles.lightPrice}>SAR 130</Text>
          <Text style={styles.darkPrice}>SAR 100</Text>
        </View>
        <View style={styles.plus}>
        <PlusButton  onButtonClick={openBannerModal}/>
        </View>
        
      </View>
    </TouchableOpacity>
  );
};
ProductCard.propTypes = {
  item: PropTypes.object
};

export default ProductCard;