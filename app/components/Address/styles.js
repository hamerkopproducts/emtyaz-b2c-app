import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";

const images = {
  edit:require('../../assets/images/profileItem/edit.png')  
};

const styles = StyleSheet.create({
    userDetails:{
        paddingTop:'1%',
        borderBottomColor:'#CFCFCF',
        borderBottomWidth:1,
        paddingBottom:'6%',
       
          },
          textContent:{flexDirection:'row'},
  billingnameandIcon:{
    flexDirection:'row',
    justifyContent:'space-between',alignItems:'center',paddingTop:'5%',paddingBottom:'3%'
  },
  userText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 13,
    color:'#686868',
    lineHeight:21,
    paddingLeft:'1%'
  },
  del:{
    flexDirection:'row',
    justifyContent:'space-between',
},
deliveryText:{
  fontFamily: I18nManager.isRTL
  ? globals.FONTS.notokufiArabic
  : globals.FONTS.poppinssemiBold,
  fontSize:14,
},
billingnameandIcon:{
    flexDirection:'row',
    justifyContent:'space-between',alignItems:'center',paddingTop:'5%',paddingBottom:'3%'
  },
});

export { styles,images };
