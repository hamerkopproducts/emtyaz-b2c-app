import react from "react";
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text,I18nManager, TouchableOpacity,ScrollView} from 'react-native';
import appTexts from "../../lib/appTexts";
import { styles,images } from "./styles";
import DividerLine from '../DividerLine';

const Address = (props) => {
    const {
      titleText,
          
        } = props;

    return(    
        <View>
        <View style={styles.del}>
        <Text style={styles.deliveryText}>{titleText} </Text>
        <Image
                  source={images.edit}
                  resizeMode="contain"
                  style={styles.arrowicon}
                />  
        </View>
        <View style={styles.billingnameandIcon}>
          <Text style={styles.nameText}>John Mathew</Text>
           
        </View>
        <View style={styles.userDetails}>
              <View style={styles.textContent}>
          <View style={{ paddingLeft: "0%" }}>
            <View style={styles.textContent}>
              <Text style={styles.userText}>Al Naemia Street</Text>
            </View>
            <View style={styles.textContent}>
              <Text style={styles.userText}>PO Box: 2211</Text>
            </View>
            <View style={styles.textContent}>
              <Text style={styles.userText}>Al Nameem,Jaddah</Text>
            </View>
            <View style={styles.textContent}>
              <Text style={styles.userText}>+966 00000</Text>
            </View>
            <View style={styles.textContent}>
              <Text style={styles.userText}>Al Nameem,Jaddah</Text>
            </View>
            <View style={styles.textContent}>
              <Text style={styles.userText}>+966 00000</Text>
            </View>
          </View>
        </View>
      </View>
      </View>
      
        
     
      );
      };
      Address.propTypes = {
     titleText:PropTypes.string,  
};

export default Address;
