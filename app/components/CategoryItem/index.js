import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text} from 'react-native';
import { styles } from "./styles";

const CategoryItem = (props) => {
  const {
        itemImage,
        badgeLabel
      } = props;

  return (
    <View style={styles.categoryItemContainer}>
      <View style={styles.imageContainer}>
        <Image style={styles.itemImage} resizeMode="contain" source={itemImage}/>
      </View>
      {badgeLabel && badgeLabel !==''? <View style={styles.badgeContainer}>
        <Text style={styles.badgeLabel}>{badgeLabel}</Text>
      </View>: null}
    </View>
  );
};
CategoryItem.propTypes = {
  itemImage:PropTypes.number,
  badgeLabel:PropTypes.string
};

export default CategoryItem;