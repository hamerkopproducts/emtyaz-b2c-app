import { StyleSheet } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  categoryItemContainer: {
    width: (globals.INTEGER.screenWidthWithMargin / 4),
    height: (globals.INTEGER.screenWidthWithMargin / 4),
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageContainer:{
    justifyContent: 'center',
    alignItems: 'center'
  },
  itemImage: {
    width: 80,
    height: 80
  },
  badgeLabel:{
    color: globals.COLOR.white,
    fontFamily: globals.FONTS.poppinsMedium,
    fontSize: 10,
  },
  badgeContainer:{
    position:'absolute',
    top:0,
    height: 20,
    width: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:20,
    backgroundColor:'red'
  }
});

export { styles };
