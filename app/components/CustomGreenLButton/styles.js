import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  dividerStyle:{
    width:'100%',
    height: 1,
    backgroundColor:'#cccccc'
  }
});

export { styles  };
