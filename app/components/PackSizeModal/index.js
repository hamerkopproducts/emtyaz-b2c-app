import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text,TouchableOpacity} from 'react-native';
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import Modal from 'react-native-modal';
import CustomRadioButton from '../CustomRadioButton';


const PackSizeModal = (props) => {
  const {
        togglePackSizeModal,
        isPackSizeModalVisible
      } = props;
      


  return (
    <Modal animationIn="slideInUp" 
    animationOut="slideOutRight" 
    isVisible={isPackSizeModalVisible} 
    style={styles.packSizeModalView}
    >
      <View style={styles.packSizeModalContent}>
      <View style={styles.xButton}>
                <TouchableOpacity onPress={() => {togglePackSizeModal();}}>
                  <Text style={styles.xText}>X</Text>
                </TouchableOpacity>
              </View>
        <View style={styles.packSizeModalTopView}>
          <View style={styles.headingPackSelect}>
            <Text style={styles.headingStyle}>{appTexts.PACKSIZE.Heading}</Text>
          </View>
          
          <View style={styles.butWrapper}>
            <TouchableOpacity>
          <View style={styles.buttonStyle}>
     <Text style={styles.buttonText}>{appTexts.PACKSIZE.ButtonTitleOne}</Text>
    </View>
    </TouchableOpacity>
    <TouchableOpacity>
    <View style={styles.buttonTwoStyle}>
     <Text style={styles.buttonText}>{appTexts.PACKSIZE.ButtonTitleTwo}</Text>
    </View>
    </TouchableOpacity>
          </View>
          
          </View>
          
          <View style={styles.touchableopacityWrap}>
              
          <TouchableOpacity style={styles.boxWrap }>
                <View style={styles.boxInput}>
                  <View style={styles.textInputWrap}>
                    <View style={styles.textOne}>
                  <Text style={styles.boxTextInput}>{appTexts.PACKSIZE.SizeOne}</Text>
                  </View>
                  <View style={styles.textTwo}>
                  <Text style={styles.boxTextTwoInput}>SAR 354.00</Text>
                  </View>
                  <View style={styles.textThree}>
                  <Text style={styles.boxTextThreeInput}>SAR 250.00</Text>
                  </View>
                  <View style={styles.textFour}>
                  <Text style={styles.boxTextFourInput}>220% off</Text>
                  </View>
                  </View>
                  
                </View>
                <CustomRadioButton />
              </TouchableOpacity>
              <TouchableOpacity style={styles.boxWrap }>
                <View style={styles.boxInput}>
                  <View style={styles.textInputWrap}>
                    <View style={styles.textOne}>
                  <Text style={styles.boxTextInput}>{appTexts.PACKSIZE.SizeTwo}</Text>
                  </View>
                  <View style={styles.textTwo}>
                  <Text style={styles.boxTextTwoInput}>SAR 354.00</Text>
                  </View>
                  <View style={styles.textThree}>
                  <Text style={styles.boxTextThreeInput}>SAR 250.00</Text>
                  </View>
                  <View style={styles.textFour}>
                  <Text style={styles.boxTextFourInput}>220% off</Text>
                  </View>
                  </View>
                  
                </View>
                <CustomRadioButton />
              </TouchableOpacity>
              <TouchableOpacity style={styles.boxWrap }>
                <View style={styles.boxInput}>
                  <View style={styles.textInputWrap}>
                    <View style={styles.textOne}>
                  <Text style={styles.boxTextInput}>{appTexts.PACKSIZE.SizeThree}</Text>
                  </View>
                  <View style={styles.textTwo}>
                  <Text style={styles.boxTextTwoInput}>SAR 354.00</Text>
                  </View>
                  <View style={styles.textThree}>
                  <Text style={styles.boxTextThreeInput}>SAR 250.00</Text>
                  </View>
                  <View style={styles.textFour}>
                  <Text style={styles.boxTextFourInput}>220% off</Text>
                  </View>
                  </View>
                  
                </View>
                <CustomRadioButton />
              </TouchableOpacity>

              </View>

              
           
          
          
      </View>
    </Modal>
    );
};
PackSizeModal.propTypes = {
  
  
};

export default PackSizeModal;