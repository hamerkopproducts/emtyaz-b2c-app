import { StyleSheet, I18nManager,Platform } from "react-native";
import globals from "../../lib/globals";
let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  whatsappIcon: require("../../assets/images/myOrders/call_icon.png"),
  mailIcon: require("../../assets/images/myOrders/mail_icon.png"),
};
const styles = StyleSheet.create({
    modalMaincontentHelp:{
        justifyContent: "flex-end",
       margin: 20,
    },
    modalmainView: {
        backgroundColor: "white",
        //width: wp("90%"),
        padding: "4%",
        //borderRadius: 10,
        borderTopRightRadius: 0,
        borderTopLeftRadius: 0,
        borderColor: "rgba(0, 0, 0, 0.1)",
      },
      buttonWrappers:{
        // paddingLeft:'3%',
        // paddingRight:'3%',
        paddingBottom:'3%',
        paddingTop:'3%',
        alignSelf:'center'
      },
      closeButton:{
        alignSelf:'flex-end',
        paddingRight:'3%',
        paddingLeft:'3%',
        paddingTop:'2%',
        paddingBottom:'2%',
        color:'#707070',
        fontSize:16
      },
      helpText:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinssemiBold,
        fontSize:14, 
        paddingBottom:'3%'
      },
      atingBlock:{
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop:'4%',
        paddingBottom:'4%' ,
        alignItems:'center'
      },
      RatingText:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinssemiRegular,
        fontSize:13, 
        color:'#1C1C1C',
        paddingTop:'1.5%'
      },
      reviewText:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinssemiBold,
        fontSize:13, 
        
      },
      inputmessage: {
        height: 90,
        marginTop: "3%",
        borderColor: "#DEDEDE",
        //backgroundColor: "#E8E8E8",
        borderWidth: 1,
        borderRadius: 1,
        textAlignVertical: "top",
        textAlign: I18nManager.isRTL ? "right" : "left",
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinsRegular,
        //fontFamily: "Montserrat-Medium",
        // textAlign: isRTL ? "right" : "left",
        // fontFamily: isRTL ? "Noto Kufi Arabic" : "Montserrat-Medium",
        fontSize: 14,
        padding: 4
      },
      buttonWrappers:{
        // paddingLeft:'3%',
        // paddingRight:'3%',
        paddingBottom:'3%',
        paddingTop:'3%',
        alignSelf:'center'
      },
      submitButton: {
        backgroundColor:  globals.COLOR.themeGreen,
        width: 144,
        height: 48,
        justifyContent: "center",
        alignItems: "center",
      },
      sendbuttonText:{
        color: globals.COLOR.white,
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
        fontSize: 15, 
      },
      ratingBlock:{
        flexDirection: "row", 
        justifyContent: "space-between",
        paddingTop:'4%',
        paddingBottom:'4%' ,
        alignItems:'center'
      },
});
export { styles };