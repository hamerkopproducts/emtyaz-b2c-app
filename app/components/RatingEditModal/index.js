import React from "react";
import { View, Text, TouchableOpacity, TextInput } from "react-native";
import Stars from "react-native-stars";
import RatingComponent from "../../components/StarRating";
import { styles } from "./styles";
import Modal from "react-native-modal";
import appTexts from "../../lib/appTexts";
const RatingEditModal = ({ isRatingModalVisible, onColse }) => {
  return (
    <Modal isVisible={isRatingModalVisible} style={styles.modalMaincontentHelp}>
      <View style={{backgroundColor: 'white', padding: 20}}>
        <TouchableOpacity
          style={styles.buttonwrapper}
          onPress={() => {
              onColse(true)
            }}
        >
          <Text style={styles.closeButton}>X</Text>
          <Text style={styles.helpText}>{appTexts.RATING.HeadingEdit}</Text>
        </TouchableOpacity>

        <View style={styles.ratingBlock}>
          <View>
            <Text style={styles.RatingText}>{appTexts.RATING.Quality}</Text>
            <Text style={styles.RatingText}>
              {appTexts.RATING.Supportfromteam}
            </Text>
            <Text style={styles.RatingText}>
              {appTexts.RATING.DeliveryTime}
            </Text>
          </View>

          <View>
            <RatingComponent />
            <RatingComponent />
            <RatingComponent />
          </View>
        </View>

        <View style={{ paddingBottom: "5%", paddingTop: "2%" }}>
          <Text style={styles.reviewText}>{appTexts.RATING.Yourreview}</Text>
          <TextInput
            style={styles.inputmessage}
            underlineColorAndroid="transparent"
            placeholderTextColor="#C9C9C9"
            autoCapitalize="none"
          />
        </View>
        <View style={styles.buttonWrappers}>
          <TouchableOpacity onPress={() => console.log("click")}>
            <View style={styles.submitButton}>
              <Text style={styles.sendbuttonText}>{appTexts.SUPPORT.Send}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default RatingEditModal;
