import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text,Image } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import { TouchableOpacity } from 'react-native-gesture-handler';


const LocationArray = [
    {id: '1', value: 'Riyadh'},
    {id: '2', value: 'Jeddah'},
    {id: '3', value: 'Khobar'},
    {id: '4', value: 'Tabuk'},
    {id: '5', value: 'Makkah'},
    {id: '6', value: 'Dammam'},
    {id: '7', value: 'Taif'},
    
  ];
const ItemView =({item}) =>{
    return(

        <View style={styles.loCListView}> 
        {/* <View style={styles.iconView}>
            <Image style={styles.radioImage} source={images.radioOff}></Image>
            </View> */}
        <View style={styles.listView}>
            <Text style={styles.locationText}>{item.value}</Text>
        </View>
        </View>
        
        
    );
};
  const LocationList = () =>{
      return(
          <View style={styles.mainScreen}>
            <FlatList
                data={LocationArray}
                renderItem=
                   
                    {ItemView}
                    
                keyExtractor={(item, index) => index.toString()}
        />
          </View>
      );
  };
      LocationList.propTypes = {

      };
      
      
      export default LocationList;
  