import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  categoryItemContainer: {
    width: (globals.INTEGER.screenWidthWithMargin / 3),
    height: (globals.INTEGER.screenWidthWithMargin / 3),
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageContainer:{
    justifyContent: 'center',
    alignItems: 'center'
  },
  itemImage: {
    width: 80,
    height: 80
  },
  nameLabel:{
    top:5,
    color: globals.COLOR.addressLabelColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic: globals.FONTS.poppinssemiBold,
    fontSize: 11,
    height: 20
  },
});

export { styles };
