import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text} from 'react-native';
import { styles } from "./styles";

const ShopsByCategoryItem = (props) => {
  const {
        itemImage,
        nameLabel
      } = props;

  return (
    <View style={styles.categoryItemContainer}>
      <View style={styles.imageContainer}>
        <Image style={styles.itemImage} resizeMode="contain" source={itemImage}/>
      </View>
      <Text style={styles.nameLabel}>{nameLabel}</Text>
    </View>
  );
};
ShopsByCategoryItem.propTypes = {
  itemImage:PropTypes.number,
  nameLabel:PropTypes.string
};

export default ShopsByCategoryItem;