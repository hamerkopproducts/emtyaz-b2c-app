import PropTypes from 'prop-types';
import React from 'react';
import { View, Text, TouchableOpacity,Image,FlatList } from 'react-native';
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import Customradiobutton from '../CustomRadioButton';
import Modal from "react-native-modal";

const SortModal = (props) => {

  const {
      isSortModalVisible,
    closeModal,
      } = props;
  let listData = [{ id: 1, name: 'Top Discount', isSelected: true }, { id: 2, name: 'Price: Low to High', isSelected: false }, { id: 3, name: 'Price: High to Low', isSelected: false }]
  const renderItem = ({ item, index }) => <View style={styles.listItem}>
    
    <View style={styles.labelView}>
    
      <View style={styles.discountTextContainer}>
        <Text style={styles.discountText}>{item.name}</Text>
      </View>
      <TouchableOpacity style={styles.favIconContainer}>
        <Customradiobutton />
        {/*<Image source={item.isSelected ? require('../../assets/images/footerTabItem/heart-active.png') : require('../../assets/images/footerTabItem/heart.png')} />*/}
      </TouchableOpacity>
    </View>
  </View>;
  return (
    <Modal
            //animationType="fade"         
            onRequestClose={() => null}
            style={styles.modalStyle}
            isVisible={isSortModalVisible}>
            <View style={styles.popupContainer}>
              {/* <TouchableOpacity style={styles.popupBackground} onPress={() => { closeModal()}}/> */}
              {/* <View style={styles.popupContentContainer}> */}
              {/* <View style={styles.popupContent}> */}
              <View style={styles.closeContainer}>
                <TouchableOpacity style={styles.popupClose} onPress={() => { closeModal() }}>
              <Text style={styles.popupCloseText}>{'X'}</Text>
                </TouchableOpacity>
                </View>
                <Text style={styles.sortByText}>{'SORT BY'}</Text>
                
            <FlatList
              style={styles.flatListStyle}
              data={listData}
              extraData={listData}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              renderItem={renderItem} />
              </View>
               {/* </View> */}
              {/* </View>  */}
            
  </Modal>
  );
};

SortModal.propTypes = {
 
};

export default SortModal;
