import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  searchIcon: require("../../assets/images/header/home-search.png")
};

const styles = StyleSheet.create({
          modalMaincontentHelp: {
    
            margin:0,
                     
  },
	popupContainer:{
   
    //justifyContent:'flex-end',
   // alignItems:'flex-end',
    backgroundColor: "white",
        //width: wp("90%"),
        //padding: "4%",
        //borderRadius: 10,
        borderTopRightRadius:15,
        borderTopLeftRadius:15,
        borderColor: "rgba(0, 0, 0, 0.1)",
        marginTop: 'auto',
        borderWidth:1,
      overflow:'hidden'
      
      
    },
  //   popupBackground:{
  //     width: '100%',
  //    height: '100%',
  //    // backgroundColor: globals.COLOR.white,
  //   	opacity:0.7,
  //   	zIndex: 1
  //   },
  // popupContentContainer: {
  //   top: 0,
  //   position: 'absolute',
  //   height: '80%',
  //   width: '100%',
  //   flexDirection: 'column',
  //   alignItems: 'center',
  //   justifyContent: "flex-end",
  //   zIndex: 3
  // },
  // popupContent: {
  //   height: '80%',
  //   width: '100%',
  //   backgroundColor: globals.COLOR.white,
  //   // paddingLeft:'5%',
  //   // paddingRight:'5%'
  //  // alignItems: 'center',
  // },
  closeContainer:{
    // width: '100%',
    // height: 50,
    // alignItems: 'flex-end',
    alignSelf:'flex-end',
    paddingRight:'3%',
     paddingTop:'4%',
    // paddingRight:'3%',
      paddingLeft:'3%',
    //   //paddingTop:'2%',
      paddingBottom:'2%',
  },
  popupClose: {
    height: 50,
    width: 40,
    marginTop: 10,
    marginRight:5,
    marginLeft:5
  },
  popupCloseText: {
    textAlign: 'center',
    color: "#8B8B8B",
    fontSize: 18
  },
  sortByText: {
    marginLeft: 0,
    width: '100%',
    textAlign: 'left',
    color: globals.COLOR.lightTextColor,
    //fontSize: 20,
    fontSize:I18nManager.isRTL ? 16 :18,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
  },
  brandText:{
    marginTop:15,
    marginLeft: '5%',
    width: '95%',
    textAlign: 'left',
    color: globals.COLOR.lightTextColor,
    fontSize:I18nManager.isRTL ? 14 :18,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  priceText:{
   // marginTop:5,
    marginLeft: '5%',
    marginRight:'5%',
    width: '95%',
    textAlign: 'left',
    color: globals.COLOR.lightTextColor,
    fontSize: 13
  },
  quantityText:{
   // marginTop: 20,
    //marginLeft: '5%',
    width: '95%',
    textAlign: 'left',
    color: globals.COLOR.lightTextColor,
    fontSize: 13,
    paddingLeft:'5%',
    paddingRight:'5%',
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
  },
  flatListStyle: {
    marginTop: 8,
    width: '100%',
    marginBottom: globals.INTEGER.bottomSpace,
  },
  listItem: {
    width: '100%',
    height: 51,
    justifyContent: 'center',
    alignItems: 'center',
  },
  labelView: {
    width: '90%',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  discountTextContainer: {
    
    height: 40,
    justifyContent: 'center',
    
  },
  discountText:{
    fontSize:I18nManager.isRTL ? 12 :12,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  textContainer: {
    marginLeft: '5%',
    marginRight: '5%',
    width: '90%',
    flexDirection: 'row'
  },
  favIconContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    width: '10%',
    height: 40
  },
  slider: {
    marginLeft: '5%',
    marginRight:'5%',
   // width: '100%'
  },
  horizontalContainer: {
    marginLeft: '5%',
    marginRight:'5%',
    //width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
   // marginTop: 17,
   marginBottom: '5%',
  },
  text: {
    color: 'white',
    fontSize: 20,
  },
  valueText: {
    color: globals.COLOR.textColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 14,
  },
  container: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: 48,
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft:'5%',
    paddingRight:'5%',
    marginBottom:'5%'
    //backgroundColor:'green'
  },
  leftHeadingContainer: {
    width: '72%',
    justifyContent: 'center',
    alignItems: 'flex-start',
    //marginBottom:'3%',
  },
  filterLabel: {
    marginLeft: 5,
    marginRight: 5,
    color: globals.COLOR.white,
    // fontFamily: globals.FONTS.poppinsMedium,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 13
  },
  applyContainer: {
    height: 40,
    width: '20%',
    marginRight: '1%',
    backgroundColor: globals.COLOR.themeGreen,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  resetContainer: {
    backgroundColor: globals.COLOR.themeOrange,
    height: 40,
    width: '20%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  dotedItemContainer:{
    marginTop:8,
    paddingLeft:'5%',
    paddingRight:'5%',
    flexDirection:'row',
    width: '90%'
  },
  categoryText:{
    justifyContent:'space-between',
    flexDirection:'row',
    paddingRight:'5%',
    paddingLeft:'5%',
    
   // marginTop:'5%'
    //marginBottom:'5%'
    //alignSelf:'flex-start'
  },
  categoryTexts:{
    justifyContent:'space-between',
    flexDirection:'row',
  },
  freshStyle:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize:10,
    color:'red',
  },
  line:{
    marginTop:'5%',
    marginBottom:'5%',
    borderWidth:0.2,
    borderColor:globals.COLOR.lightGray,
    

  },
  textCategoryView:{
    flexDirection:'column',
    alignSelf:'flex-start',
   // marginTop:'2%'
  },
  categoryTextStyle:{
    textAlign:'left',
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
  },
  arrowView:{
    alignSelf:'center',
    
  }
});

export { images, styles  };