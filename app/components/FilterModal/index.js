import PropTypes from 'prop-types';
import React, { useCallback, useState} from 'react';
import { View, StatusBar, Text, TouchableOpacity,Image,FlatList,TextInput,I18nManager } from 'react-native';
import Modal from 'react-native-modal';
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import Slider from 'rn-range-slider';
import Thumb from './Slider/Thumb';
import Rail from './Slider/Rail';
import RailSelected from './Slider/RailSelected';
import Notch from './Slider/Notch';
import Label from './Slider/Label';
import DividerLine from "../DividerLine";
import DotedContainerItem from "./DotedContainerItem";
import Category from '../Category';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import CategoryModal from '../../components/CategoryModal';
const FilterModal = (props) => {

  const {
    isFilterModalVisible,
    closeModal,
    setIscateogoryFilter,
    setIsbrandFilter,
    } = props;
      
      const openCategoryModal = () => {
        closeModal();
        setTimeout(() => {
          setIscateogoryFilter() ;
        }, 400);
      };
      const openBrandModal = () => {
        closeModal();
        setTimeout(() => {
          setIsbrandFilter() ;
        }, 400);
      };

  const [rangeDisabled, setRangeDisabled] = useState(false);
  const [low, setLow] = useState(0);
  const [high, setHigh] = useState(1000);
  const [min, setMin] = useState(0);
  const [max, setMax] = useState(1000);
  const [floatingLabel, setFloatingLabel] = useState(false);

  const renderThumb = useCallback(() => <Thumb />, []);
  const renderRail = useCallback(() => <Rail />, []);
  const renderRailSelected = useCallback(() => <RailSelected />, []);
  const renderLabel = useCallback(value => <Label text={value} />, []);
  const renderNotch = useCallback(() => <Notch />, []);
  const handleValueChange = useCallback((low, high) => {
    setLow(low);
    setHigh(high);
  }, []);
  
  let listData = [{ id: 1, name:  (I18nManager.isRTL ? 'طلباتك(25)' : 'Activa(25)'), isSelected: true }, 
    { id: 2, name:  (I18nManager.isRTL ? 'طلباتك(25)' :'Al ain(65)'), isSelected: false }, 
    { id: 3, name:  (I18nManager.isRTL ? 'طلباتك(25)' :'Al jazira(10)'), isSelected: false },
    { id: 4, name: (I18nManager.isRTL ? 'طلباتك(25)' : 'Al jazira(10)'), isSelected: false },
    { id: 5, name:  (I18nManager.isRTL ? 'طلباتك(25)' :'Al Khazna(24)'), isSelected: false },
    { id: 6, name:  (I18nManager.isRTL ? 'طلباتك(25)' :'Almaraialyoum(19)'), isSelected: false }];

  const renderItem = ({ item, index }) => <View style={styles.listItem}>
    <View style={styles.labelView}>
      <View style={styles.discountTextContainer}>
        <Text style={styles.discountText}>{item.name}</Text>
      </View>
      <TouchableOpacity style={styles.favIconContainer}>
        <Image source={item.isSelected ? require('../../assets/images/modal/checked.png') : require('../../assets/images/modal/uncheck.png')} />
      </TouchableOpacity>
    </View>
    <DividerLine />
  </View>;

  return (
    
    <Modal
    
            
            // animationIn="slideInUp" 
            //  animationOut="slideOutRight"
          style={styles.modalMaincontentHelp}
            onRequestClose={() => null}
            isVisible={isFilterModalVisible}>
            <View style={styles.popupContainer}>
              {/* <TouchableOpacity style={styles.popupBackground} onPress={() => { closeModal()}}/> */}
        {/* <View style={styles.popupContentContainer}> */}
          {/* <View style={styles.popupContent}> */}
            <View style={styles.closeContainer}>
            <TouchableOpacity style={styles.popupClose} onPress={() => { closeModal() }}>
              <Text style={styles.popupCloseText}>{'X'}</Text>
            </TouchableOpacity>
            </View>
            <View style={styles.container}>
              <View style={styles.leftHeadingContainer}>
                <Text style={styles.sortByText}>{appTexts.MY_ORDERS.Filter}</Text>
              </View>
              <TouchableOpacity style={styles.applyContainer} onPress={() => {  }}>
                <Text style={styles.filterLabel}>{appTexts.CATEGORY_CONTENT.Button1}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.resetContainer} onPress={() => {  }}>
                <Text style={styles.filterLabel}>{appTexts.CATEGORY_CONTENT.Button2}</Text>
              </TouchableOpacity>
            </View>
            
            <TouchableOpacity onPress={() => { openCategoryModal()}}>
            <View style={styles.categoryText}>
            
                <View style={styles.textCategoryView}>
                <Text style={styles.categoryTextStyle}>{appTexts.CATEGORY_CONTENT.Category}</Text>
                <Text style={styles.freshStyle}>{appTexts.CATEGORY_CONTENT.SubHeading}</Text>
                </View>    
                <View style={styles.arrowView}>
                  <Image source={require('../../assets/images/profileItem/arrow_profile.png')} style={styles.arrowStyle}></Image>
                </View>
                 
            </View>
            </TouchableOpacity>
            <View style={styles.line}></View>
            <TouchableOpacity onPress={() => { openBrandModal()}}>
            <View style={styles.categoryText}>
                <View style={styles.textCategoryView}>
                <Text style={styles.categoryTextStyle}>{appTexts.CATEGORY_CONTENT.Brands}</Text>
                </View>    
                <View style={styles.arrowView}>
                  <Image source={require('../../assets/images/profileItem/arrow_profile.png')} style={styles.arrowStyle}></Image>
                </View>
             </View>
             <View style={styles.line}></View>
            </TouchableOpacity> 
            
            
            <Text style={styles.quantityText}>{appTexts.PRODUCT_FLITER.Quantity}</Text>
            
            <View style={styles.dotedItemContainer}>
              <DotedContainerItem text={'1'} unit={'Kg'}/>
              <DotedContainerItem text={'2'} unit={'Kg'}/>
              <DotedContainerItem text={'3'} unit={'Kg'}/>
              <DotedContainerItem text={'5'} unit={'Kg'}/>
            </View>
            <View style={styles.line}></View>
            <Text style={styles.quantityText}>{appTexts.PRODUCT_FLITER.packSize}</Text>
            <View style={styles.dotedItemContainer}>
              <DotedContainerItem text={'SMALL'} />
              <DotedContainerItem text={'MEDIUM'} />
              <DotedContainerItem text={'LARGE'} />
              
            </View>
            <View style={styles.line}></View>
            <Text style={styles.quantityText}>{appTexts.PRODUCT_FLITER.price}</Text>
            {/* <Text style={styles.priceText}>{''}</Text> */}
            <Slider
              style={styles.slider}
              min={min}
              max={max}
              step={1}
              disableRange={rangeDisabled}
              floatingLabel={floatingLabel}
              renderThumb={renderThumb}
              renderRail={renderRail}
              renderRailSelected={renderRailSelected}
              //renderLabel={renderLabel}
              renderNotch={renderNotch}
              onValueChanged={handleValueChange}
            />
            <View style={styles.horizontalContainer}>
              <Text style={styles.valueText}>{'AR ' + low}</Text>
              <Text style={styles.valueText}>{'AR ' + high}</Text>
            </View>
            {/* <Text style={styles.brandText}>{appTexts.PRODUCT_FLITER.Brands}</Text>
            <FlatList
              style={styles.flatListStyle}
              data={listData}
              extraData={listData}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={true}
              renderItem={renderItem} /> */}
          </View>
        {/* </View> */}
      {/* </View> */}
  </Modal>
  );
};

FilterModal.propTypes = {

};

export default FilterModal;