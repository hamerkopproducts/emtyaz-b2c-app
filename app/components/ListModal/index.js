import PropTypes from 'prop-types';
import React from 'react';
import { Modal, View, StatusBar, Text, TouchableOpacity,Image,FlatList,TextInput } from 'react-native';
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import PromotionalProductCard from "../PromotionalProductCard";

const ListModal = (props) => {

  const {
    listModalIndex,
    closeModal,
   
      } = props;
  let listData = [1, 2, 3, 4, 5, 6]
  const renderItem = ({ item, index }) => <PromotionalProductCard itemImage={require('../../assets/images/home/product_01.png')} />;
  return (
    <Modal
            animationType="fade"
            transparent={true}
            onRequestClose={() => null}
              visible={listModalIndex !==-1?true:false}>
            <View style={styles.popupContainer}>
        <TouchableOpacity style={styles.popupBackground} onPress={() => { closeModal()}}/>
              <View style={styles.popupContentContainer}>
          <Text style={styles.headingText}>{listModalIndex===0? 'Favourites': 'Save Later'}</Text>
            <View style={styles.listContainer}>
            {listData.length === 0 ? <View style={styles.noDataContainer}><Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text></View> :

              <FlatList
                style={styles.flatListStyle}
                data={listData}
                extraData={listData}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                renderItem={renderItem} />}
            </View>
              </View>
            </View>
  </Modal>
  );
};

ListModal.propTypes = {
 
};

export default ListModal;
