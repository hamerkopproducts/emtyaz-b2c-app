import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity} from 'react-native';
import { styles } from "./styles";
import DotedContainerItem from "./DotedContainerItem";
import MoreItem from "./MoreItem";
import PlusButton from "./PlusButton";
import DividerLine from "../DividerLine";
import Modal from 'react-native-modal';
import CustomRadioButton from '../../components/CustomRadioButton';
import appTexts from '../../lib/appTexts';
import BannerModal from '../../components/BannerModal';
import QuantityModal from '../../components/QuantityModal';
import PackModal from '../../components/PackModal';
import PackSizeModal from '../../components/PackSizeModal';

const PromotionalProductCard = (props) => {
  const {
        item,
        itemClick,
        
      } = props;

      const [isPackModalVisible, setIsPackModalVisible] = useState(false);
      
      const [isPackSizeModalVisible, setIsPackSizeModalVisible] = useState(false);
      const [isBannerModalVisible, setIsBannerModalVisible] = useState(false);
      const [isQuantityModalVisible, setIsQuantityModalVisible] = useState(false);

      const togglePackSizeModal = () => {
        setIsPackSizeModalVisible(false)
     };
     const openPackSizeModal = () => {
       setIsPackSizeModalVisible(true)
     };
	

      const togglePackModal = () => {
        setIsPackModalVisible(false)
     };
     const openPackModal = () => {
       setIsPackModalVisible(true)
     };
	

 const toggleQuantityModal = () => {
     setIsQuantityModalVisible(false)
  };
  const openQuantityModal = () => {
    setIsQuantityModalVisible(true)
  };

const closeBannerModal = () => {
    setIsBannerModalVisible(false)
  };
  const openBannerModal = () => {
    setIsBannerModalVisible(true)
  };
  return (
    <View style={styles.fullWidthRowContainer}>
    <View style={styles.rowContainer}>
      <View style={styles.firstContainer}>
      <TouchableOpacity  onPress={()=>{itemClick()}}>
        <View style={styles.imageContainer}>
          <Image resizeMode="contain" source={item.image} style={styles.image}/>
        </View>
        </TouchableOpacity>
        <View style={styles.labelContainer}>
          <View style={styles.labelView}>
            <View style={styles.textContainer}>
                <Text style={styles.itemNameText}>{item.name}</Text>
            </View>
            
            <View style={styles.favIconContainer}>
                <Image source={item.isFav ? require('../../assets/images/products/heart-active.png') : require('../../assets/images/products/favourite.png')} />
            </View>
            
          </View>
          <TouchableOpacity onPress={() => { openQuantityModal()}}>
          <View style={styles.sizeSelectContainer}>
           
            <View style={styles.sizeSelectView}>
              <Text style={styles.sizeText}>{'Small'}</Text>
                <Image style={styles.dropDownArrow} source={require('../../assets/images/cartScreenIcons/downArrow.png')} />
            </View>
           
          </View>
          </TouchableOpacity>
          <QuantityModal isQuantityModalVisible={isQuantityModalVisible}toggleQuantityModal={toggleQuantityModal} />
          
        </View>
      </View>
      
      
    <PackModal isPackModalVisible={isPackModalVisible} togglePackModal={togglePackModal} />
    <PackSizeModal isPackSizeModalVisible={isPackSizeModalVisible} togglePackSizeModal={togglePackSizeModal} />
    <BannerModal isBannerModalVisible={isBannerModalVisible} closeModal={closeBannerModal} bannerText={appTexts.BANNER.WishList}/>
      <View style={styles.secondContainer}>
        <DotedContainerItem/>
        <DotedContainerItem onEventClick={openPackSizeModal}/>
        <DotedContainerItem />
        <MoreItem onItemClick={openPackModal}/>
      </View>
      <View style={styles.addButtonContainer}>
        <PlusButton onButtonClick={openBannerModal}/>
      </View>
      </View>
      <DividerLine />
    </View>
  );
};
PromotionalProductCard.propTypes = {
  item:PropTypes.object,
  
};

export default PromotionalProductCard;