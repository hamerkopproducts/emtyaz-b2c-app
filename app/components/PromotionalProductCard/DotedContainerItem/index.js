import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text} from 'react-native';
import { styles } from "./styles";
import { TouchableOpacity } from 'react-native-gesture-handler';

const DotedContainerItem = (props) => {
  const {
        itemImage,
        nameLabel,
        onEventClick,
      } = props;

  return (
    <TouchableOpacity onPress={() => {onEventClick();}}>
  <View style={styles.sizeSelectView}>
    <Text style={styles.symbolText}>{'X'}</Text>
    <Text style={styles.countText}>{'200'}</Text>
  </View>
  </TouchableOpacity>
  );
};
DotedContainerItem.propTypes = {
  itemImage:PropTypes.number,
  nameLabel:PropTypes.string
};

export default DotedContainerItem;