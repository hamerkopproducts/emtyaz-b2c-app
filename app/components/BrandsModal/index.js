import PropTypes from 'prop-types';
import React from 'react';
import { View, StatusBar, Text, TouchableOpacity } from 'react-native';
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import Category from '../Category';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Modal from 'react-native-modal';


const BrandsModal = (props) => {

  const {
   
   isBrandModalVisible,
   //closeCategoryModal,
   toggleBrandModal,
   categoryHeading,
   categorySubHeading,

      } = props;

   isBrandModalVisible,
      console.debug(isBrandModalVisible, '<<--isBrandModalVisible')
  
  return (
    <Modal
    isVisible={isBrandModalVisible} 
    //transparent={true}
    onRequestClose={() => null}
    style={styles.modalMainContent}
  >
    <View style={styles.modalmainView}>
                    <KeyboardAwareScrollView
                        scrollEnabled={true}
                        //contentContainerStyle={{ justifyContent: "flex-end" }}
                    >
        <View>
            <TouchableOpacity
              style={styles.buttonwrapper}
              onPress={() => {toggleBrandModal();}}>
            
              <Text style={styles.closeButton}>X</Text>
               </TouchableOpacity>
        </View>
            <View style={styles.categoryHeading}>
                    <View style={styles.textWrapper}>
                        <Text style={styles.categoryTextHeading}>{categoryHeading}</Text>
                        <Text style={styles.lowerText}>{categorySubHeading}</Text>
                    </View>
                <View style={styles.buttonCover}>
                    <View style={styles.applyButton}>
                        <TouchableOpacity style={styles.applyButton}>
                            <Text style={styles.applyButtonText}>{appTexts.CATEGORY_CONTENT.Button1}</Text>
                            </TouchableOpacity>
                    </View>
                    <View style={styles.resetButton}>
                    <TouchableOpacity style={styles.resetButton}>
                            <Text style={styles.resetButtonText}>{appTexts.CATEGORY_CONTENT.Button2}</Text>
                            </TouchableOpacity>
                    </View>
                </View>
            </View>	
            <View style={styles.bordersStyle}></View>	
            <View style={styles.childComponent}>
            <Category itemText={appTexts.CATEGORY_CONTENT.BrandOne} />	
            <Category itemText={appTexts.CATEGORY_CONTENT.BrandTwo}/>	
            <Category itemText={appTexts.CATEGORY_CONTENT.BrandThree}/>	
            <Category itemText={appTexts.CATEGORY_CONTENT.Content}/>	
            <Category itemText={appTexts.CATEGORY_CONTENT.BrandOne}/>	
            <Category itemText={appTexts.CATEGORY_CONTENT.BrandTwo}/>	
            <Category itemText={appTexts.CATEGORY_CONTENT.BrandThree}/>	
            <Category itemText={appTexts.CATEGORY_CONTENT.BrandOne}/>	
                </View>	
          
          
            
        
        </KeyboardAwareScrollView>
        </View>
            </Modal>
  );
};

BrandsModal.propTypes = {
 
};

export default BrandsModal;