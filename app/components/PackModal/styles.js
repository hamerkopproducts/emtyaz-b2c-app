import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";

const styles = StyleSheet.create({

  closeButton:{
    //justifyContent:'flex-end',
    //alignItems:'flex-end',
    alignSelf:'flex-end',
    paddingRight:'3%',
   paddingTop:'2%',
  // paddingRight:'3%',
    paddingLeft:'3%',
  //   //paddingTop:'2%',
    paddingBottom:'2%',
  },
  closeText:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
    fontSize: 17,
    paddingTop:'1%',
    color:'grey',
  },
  packHeadingText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
    fontSize: 17, 
  },
  packModalView:{
margin:0,
  },
  packModalContent:{
    //height: '55%',
    marginTop: 'auto',
    backgroundColor:'white',
    paddingBottom:'5%',
    
  },
  packModalContentView:{
    paddingLeft:'5%',
    paddingRight:'8%',
  },
  touchableopacityWrapper:{
   
   paddingTop:'4%',
   paddingBottom:'4%',
 
  
  },
  boxWrapper:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    
    
  },
  boxText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic: globals.FONTS.poppinsMedium,
    fontSize: 14,   
    paddingLeft:'1%',
  },
  packModalheading:{
    flexDirection: 'row'
  },
  inputBox:{
    borderWidth:0.5,
    width:'90%',
    padding:'2%',
    flexDirection: 'row',
   
    borderColor:globals.COLOR.lightGray,
      },

});

export { styles };