import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text,TouchableOpacity} from 'react-native';
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import Modal from 'react-native-modal';
import CustomRadioButton from '../CustomRadioButton';


const PackModal = (props) => {
  const {
        togglePackModal,
        isPackModalVisible
      } = props;
      


  return (

<Modal animationIn="slideInUp" 
    animationOut="slideOutRight" 
    isVisible={isPackModalVisible} 
    style={styles.packModalView}
    >
      <View style={styles.packModalContent}>
          <View style={styles.closeButton}>
            <TouchableOpacity onPress={() => {togglePackModal();}}>
              <Text style={styles.closeText}>X</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.packModalContentView}>
            <View style={styles.packModalheading}>
              <Text style={styles.packHeadingText}>{appTexts.QUANTITY.Heading}</Text>
            </View>
            <View style={styles.touchableopacityWrapper}>
              <TouchableOpacity style={styles.boxWrapper }>
                <View style={styles.inputBox}>
                  <Text style={styles.boxText}>{appTexts.QUANTITY.BodyTextOne}</Text>
                </View>
                <CustomRadioButton />
              </TouchableOpacity>
            </View>

            <View style={styles.touchableopacityWrapper}>
              <TouchableOpacity style={styles.boxWrapper }>
                <View style={styles.inputBox}>
                  <Text style={styles.boxText}>{appTexts.QUANTITY.BodyTextTwo}</Text>
                </View>
                <CustomRadioButton />
              </TouchableOpacity>
            </View>

            <View style={styles.touchableopacityWrapper}>
              <TouchableOpacity style={styles.boxWrapper }>
                <View style={styles.inputBox}>
                  <Text style={styles.boxText}>{appTexts.QUANTITY.BodyTextThree}</Text>
                </View>
                <CustomRadioButton />
              </TouchableOpacity>
            </View>
            <View style={styles.touchableopacityWrapper}>
              <TouchableOpacity style={styles.boxWrapper }>
                <View style={styles.inputBox}>
                  <Text style={styles.boxText}>{appTexts.QUANTITY.BodyTextFour}</Text>
                </View>
                <CustomRadioButton />
              </TouchableOpacity>
            </View>
          </View>

          
      </View>
    </Modal>
  );
};
PackModal.propTypes = {
  
  
};

export default PackModal;  