import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text,I18nManager, TouchableOpacity,ScrollView} from 'react-native';
import appTexts from "../../lib/appTexts";
import { styles } from "./styles";
import DividerLine from '../DividerLine';



const OrderSummary = (props) => {
    const {
      
          
        } = props;

    return(    
        
        <View style={styles.fullWidthRowContainer}>
            <View style={styles.rowOne}>
                <View style={styles.summaryHeading}>
                    <Text style={styles.summaryText}>{appTexts.SUMMARY.heading}</Text>
                </View>
                <View>
                <Image
                  source={require("../../assets/images/profileItem/edit.png")}
                  resizeMode="contain"
                  style={styles.arrowicon}
                />  
                </View>
            </View>
            <View style={styles.rowTwo}>
                <Text style={styles.textTwo}>{appTexts.SUMMARY.item}</Text>
            </View>
            <View style={styles.rowThree}>
                <View>
                <Text style={styles.textThree}>{appTexts.SUMMARY.subtotal}</Text>
                </View>
                <View>
                    <Text style={styles.sarText}>{appTexts.SUMMARY.sar}</Text>
                </View>
            </View>
            <View style={styles.rowThree}>
                <View>
                <Text style={styles.textThree}>{appTexts.SUMMARY.discount}</Text>
                </View>
                <View>
                    <Text style={styles.sarText}>{appTexts.SUMMARY.sars}</Text>
                </View>
            </View>
            <View style={styles.rowThree}>
                <View>
                <Text style={styles.textThree}>{appTexts.SUMMARY.delivery}</Text>
                </View>
                <View>
                    <Text style={styles.sarText}>{appTexts.SUMMARY.sarss}</Text>
                </View>
            </View>
            <View style={styles.line}>
            <DividerLine />
            </View>
            <View style={styles.rowThree}>
                <View>
                <Text style={styles.sarText}>{appTexts.SUMMARY.tot}</Text>
                </View>
                <View>
                    <Text style={styles.sarText}>{appTexts.SUMMARY.sartot}</Text>
                </View>
            </View>

        </View>
        
     
      );
      };
      OrderSummary.propTypes = {
        itemImage:PropTypes.object,
  
};

export default OrderSummary;
