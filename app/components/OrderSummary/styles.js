import { StyleSheet,I18nManager  } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  fullWidthRowContainer:{
    borderWidth:0.5,
    width:'100%',
    borderRadius:10,
    height:'25%',
    paddingLeft:'3%',
    paddingRight:'3%',
    marginBottom:'5%',
  },
  rowOne:{
      flexDirection:'row',
      justifyContent:'space-between',
      paddingTop:'5%',
      alignItems:'center'
  },
  summaryText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic: globals.FONTS.poppinssemiBold,
  },
  arrowicon:{
      alignSelf:'center',
  },
  rowTwo:{
      marginTop:'2%'
  },
  rowThree:{
      flexDirection:'row',
      justifyContent:'space-between',
      marginTop:'5%'
  },
  textThree:{
    color: globals.COLOR.lightTextColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic: globals.FONTS.poppinsRegular,
    fontSize:12,
  },
  sarText:{
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic: globals.FONTS.poppinssemiBold,
      fontSize:12,
  },
  line:{
      marginTop:'3%',
  },
  
});

export { styles };