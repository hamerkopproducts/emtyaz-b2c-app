import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";

const styles = StyleSheet.create({
    modalMaincontentHelp: {
        //justifyContent: "center",
        justifyContent:'flex-end',
        margin: 0,
      },
      
      modalmainviewHelp: {
        backgroundColor: "white",
        //width: wp("90%"),
        padding: "4%",
        //borderRadius: 10,
        borderTopRightRadius:15,
        borderTopLeftRadius:15,
        borderColor: "rgba(0, 0, 0, 0.1)",
      },

closeButton:{
    //justifyContent:'flex-end',
    //alignItems:'flex-end',
    alignSelf:'flex-end',
    paddingRight:'3%',
     paddingTop:'4%',
    // paddingRight:'3%',
      paddingLeft:'3%',
    //   //paddingTop:'2%',
      paddingBottom:'2%',
  },
  closeText:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
      fontSize: 20,
      color:'grey',
  },
  popHeading:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
    fontSize: 14,
  },
});

export { styles };