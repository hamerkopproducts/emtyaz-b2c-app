
import React from 'react';
import { images, styles } from "./styles";
import {  View, Text, TouchableOpacity,Image,TextInput,FlatList } from 'react-native';
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import PropTypes from 'prop-types';
import PromoProductCard from '../../components/PromoProductCard';


import Modal from 'react-native-modal';

const SaveForLaterModal = (props) => {

  const {
        isSaveForLaterModalVisible,
        toggleSaveForLaterModal,
      } = props;
      
	let listData = [
		{
			id: 1,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_01.png')

		},
		{
			id: 2,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_02.png')

		},
		{
			id: 3,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_03.png')

		},
		
		]

      const renderItem1 = ({ item, index }) => <PromoProductCard item={item} />; 
  return (
    <Modal
    isVisible={isSaveForLaterModalVisible}
    style={styles.modalMaincontentHelp}
    animationIn="slideInUp" 
    animationOut="slideOutRight" 
    onSwipeComplete={toggleSaveForLaterModal}
    swipeDirection={["left", "right", "up", "down"]}
  >
    <View style={styles.modalmainviewHelp}>
    <View style={styles.closeButton}>
            <TouchableOpacity onPress={() => {toggleSaveForLaterModal();}}>
              <Text style={styles.closeText}>X</Text>
            </TouchableOpacity>
          </View> 
     <View style={styles.heading}  >
         <Text style={styles.popHeading}>{appTexts.SAVE.heading}</Text>
         </View>  
         <FlatList
							style={styles.flatListStyle}
							data={listData}
							extraData={listData}
							keyExtractor={(item, index) => index.toString()}
							/*onEndReachedThreshold={0.5}
							onEndReached={({ distanceFromEnd }) => {
								if (listData.length >= (currentPage * pageLimit)) {
									loadMoreItems();
								}
							}}*/
							//onRefresh={() => { onRefresh() }}
							//refreshing={isRefreshing}
							// numColumns={columnCount}
							// key={columnCount}
							showsVerticalScrollIndicator={false}
							renderItem={renderItem1} />
    </View>
  </Modal>
  );
};

SaveForLaterModal.propTypes = {
isSaveForLaterModalVisible:PropTypes.bool,
toggleSaveForLaterModal:PropTypes.func,
};

export default SaveForLaterModal;