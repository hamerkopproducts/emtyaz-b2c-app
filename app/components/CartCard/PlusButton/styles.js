import { StyleSheet } from "react-native";
import globals from "../../../lib/globals"

const styles = StyleSheet.create({
  buttonView: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    //alignSelf: 'center',
    backgroundColor:'red'
  },
  plusText:{
    //alignSelf: 'center',
    color:'white',
    fontSize:30
  }
});

export { styles };
