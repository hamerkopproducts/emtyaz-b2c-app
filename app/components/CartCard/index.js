import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity} from 'react-native';
import { styles } from "./styles";
import DotedContainerItem from "./DotedContainerItem";
import MoreItem from "./MoreItem";
import PlusButton from "./PlusButton";
import DividerLine from "../DividerLine";
import appTexts from '../../lib/appTexts';
const CartCard = (props) => {
  const {
        itemImage,
        nameLabel
      } = props;

  return (
    <View style={styles.fullWidthRowContainer}>
    <View style={styles.rowContainer}>
      <View style={styles.firstContainer}>
        <View style={styles.imageContainer}>
          <Image resizeMode="contain" source={itemImage} style={styles.image}/>
        </View>
        
        <View style={styles.labelContainer}>
          <View style={styles.labelView}>
            <View style={styles.textContainer}>
                <Text style={styles.itemNameText}>{'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder'}</Text>
            </View>
            <View style={styles.deleteIconContainer}>
                <Image source={require('../../assets/images/cartScreenIcons/delete.png')} style={styles.binStyle}/>
            </View>
            <View style={styles.favIconContainer}>
                <Image source={require('../../assets/images/cartScreenIcons/heart-active.png')} style={styles.favIconStyle}/>
            </View>
          </View>
          <View style={styles.sizeSelectContainer}>

              <View style={styles.labelView}>
                <View style={styles.sizeSelectView}>
                  <Text style={styles.totalCountText}>{appTexts.CART.Nos}</Text>
                </View>
                <View style={styles.priceContainer}>
                  <Text style={styles.oldpriceStyle}>SAR 1567</Text>
                  <Text style={styles.priceStyle}>SAR 620</Text>
                </View>
                {/* <View style={styles.priceContainer}>
                  <Text>sar 1567</Text>
                  <Text>sar 6765</Text> */}
                 {/*<View style={styles.countSelectView}>
                  <TouchableOpacity>
                    <Text style={styles.symbolText}>{'-'}</Text>
                  </TouchableOpacity>
                    <View style={styles.verticalDivider}/>
                  <Text style={styles.countText}>{'2'}</Text>
                    <View style={styles.verticalDivider}/>
                  <TouchableOpacity>
                      <Text style={styles.symbolText}>{'+'}</Text>
                  </TouchableOpacity>
  </View> */}
 
              </View>
            
             
           
              
          </View>
        </View>
      </View>
      </View>
      <View style={styles.countContainer}>
                <View style={styles.countSelectView}>
                  <TouchableOpacity>
                    <Text style={styles.symbolText}>{'-'}</Text>
                  </TouchableOpacity>
                    <View style={styles.verticalDivider}/>
                  <Text style={styles.countText}>{'2'}</Text>
                    <View style={styles.verticalDivider}/>
                  <TouchableOpacity>
                      <Text style={styles.symbolText}>{'+'}</Text>
                  </TouchableOpacity>
                </View>
                </View>
      <DividerLine />
    </View>
  );
};
CartCard.propTypes = {
  itemImage:PropTypes.number,
  nameLabel:PropTypes.string
};

export default CartCard;