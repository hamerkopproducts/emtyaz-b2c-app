import { StyleSheet } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  fullWidthRowContainer:{
    width: '100%',
    height: 175,
    //justifyContent: 'center',
    //alignItems: 'center',
   
  },
  rowContainer: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: 130,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft:'5%',
  },
  firstContainer:{
    width: '100%',
    height: 120,
    paddingTop:10,
    //paddingBottom:,
    flexDirection:'row'
  },
  secondContainer: {
    width: '100%',
    height: 40,
    flexDirection: 'row'
  },
  image: {
    height: 90,
    width: 90,
  },
  imageContainer:{
    height: 100,
    width:90,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.lightGray,
    borderWidth:1
  },
  labelContainer:{
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 100,
  },
  labelView:{
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 80,
    flexDirection:'row'
  },
  sizeSelectContainer: {
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 40,
    flexDirection:'row'
  },
  sizeSelectView:{
    marginLeft:20,
    width: 80,
    height: 25,
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  countContainer:{
    flex:1,
    height: 30,
    justifyContent:'flex-start',
    alignItems:'flex-start',
    //alignItems: 'flex-start'
  },
  countSelectView: {
    marginLeft: 20,
    width: 92,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: globals.COLOR.themeGreen
  },
  priceContainer:{
    width: globals.INTEGER.screenWidthWithMargin,
    paddingLeft:'40%',
    
    
  },
  oldpriceStyle:{
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinssemiBold,
    fontSize: 12,
    textDecorationLine:'line-through',
  },
  priceStyle:{
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinssemiBold,
    fontSize: 12,
    
  },
  itemNameText:{
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsMedium,
    fontSize: 12,
  },
  totalCountText:{
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 12
  },
 countText: {
    width: 30,
    textAlign:'center',
    color: globals.COLOR.white,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 16
  },
  symbolText: {
    width: 30,
    textAlign: 'center',
    color: globals.COLOR.white,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 20
  },
  dropDownArrow: {
    
  },
  textContainer:{
    marginLeft: 20,
    width: globals.INTEGER.screenWidthWithMargin - 200,
    height: 60
  },
  favIconContainer: {
    alignItems: 'flex-end',
    width:40,
    height: 50
  },
  favIconStyle:{
    width:20,
    height:19,
  },
  deleteIconContainer:{
    alignItems: 'flex-end',
    width: 40,
    height: 50
  },
  binStyle:{
    width:15,
    height:19,
  },
  nameLabel:{
    top:0,
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 11,
    height: 20
  },
  addButtonContainer:{
    position:'absolute',
    right:0,
    bottom:20
  },
  verticalDivider:{
    width:0.5,
    height:'80%',
    backgroundColor: globals.COLOR.white,
  }
});

export { styles };
