import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
    varrow:require('../../assets/images/profileicon/arrow.png'),
    search:require('../../assets/images/listCard/Search.png'),
};

const styles = StyleSheet.create({
modalMaincontentHelp: {
    //justifyContent: "center",
    justifyContent:'flex-end',
    margin: 0,
  },
  
  modalmainviewHelp: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius:15,
    borderTopLeftRadius:15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  helptextWarpper:{
    flexDirection:'row',
    justifyContent:'center',
    paddingTop:hp('2%'),
    paddingBottom:hp('1.5%'),
    paddingLeft:'10%',
    paddingRight:'10%'
    //paddingLeft:'19%'
    },
    helpheadText:{
      fontSize: hp('2.6%'),
      fontFamily: globals.FONTS.avenirHeavy,
      textAlign:'center'
    },
    boxVies:{
        height:50,
        width:'95%',
        borderWidth:0.5,
        borderColor:'#707070',
        flexDirection:'row',
        justifyContent:'space-between',
              marginTop:'8%',
        borderRadius:5,
        
      marginLeft:'3%'
         },
         hinput: {
            //marginTop:-4
            // margin: 15,
            // height: 40,
            // borderColor: '#7a42f4',
            // borderWidth: 1
            fontFamily: globals.FONTS.openSansLight,
            fontSize: hp('2%'),
            textAlign: I18nManager.isRTL ? "right" : "left",
            color:"#707070",
            marginLeft:'2%'
            
          },
          arrowv:{
            marginRight:'4%',
            alignItems:'center',
            justifyContent:'center'
              },
              search:{
                width:20,
                height:20,
                alignSelf:'center'
              },
              listV:{
                marginTop:"5%"
                },
            });

            export { images, styles };