import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity,Image, Text} from 'react-native';
import { styles } from "./styles";

const HeadingWithRightLink = (props) => {
  const {
        leftIcon,
        nameLabel,
        rightLinkText,
        color,
        locationnameLabel,
        onItemClick,
      } = props;
  return (
    <View style={styles.container}>
      <View style={styles.leftHeadingContainer}>
        <View style={styles.leftHeadingView}>
          {leftIcon && <Image style={styles.leftIcon} resizeMode="contain" source={leftIcon} />}
          <View style={styles.leftHeadingLabelView}>
            <Text style={styles.locationnameLabel}>{locationnameLabel}</Text>
          </View>
          <View style={styles.leftHeadingLabelView}>
            <Text style={styles.nameLabel}>{nameLabel}</Text>
          </View>
        </View>
      </View>
      <TouchableOpacity style={styles.rightLinkContainer} onPress={onItemClick}>
        <Text style={color === 'green' ? styles.linkLabel : styles.linkLabelYellow}>{rightLinkText}</Text>
      </TouchableOpacity>
    </View>
  );
};
HeadingWithRightLink.propTypes = {
  leftIcon: PropTypes.number,
  nameLabel:PropTypes.string,
  onItemClick:PropTypes.func,
};

export default HeadingWithRightLink;