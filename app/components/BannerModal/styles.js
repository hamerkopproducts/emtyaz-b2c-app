import { StyleSheet,I18nManager,Platform } from "react-native";
import globals from "../../lib/globals"

const images = {
  searchIcon: require("../../assets/images/header/home-search.png")
};

const styles = StyleSheet.create({
	popupContainer:{
      width: '100%',
    	height: '100%',
      flexDirection: 'column',
      backgroundColor: globals.COLOR.transparent
    },
    popupBackground:{
      width: '100%',
      height: '100%',
      backgroundColor: '#000',
      opacity:0.4,
    	zIndex: 1
    },
    popupContentContainer:{
      top:0,
      width: '100%',
      height: '100%',
      position:'absolute',
      backgroundColor: globals.COLOR.transparent,
    	flexDirection: 'column',
      alignItems: 'center',
      justifyContent: "center",
      zIndex: 3
    },
  bannerView:{
    alignItems: "center",
    width: globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen * 2),
    height:80,
    flexDirection:'row',
    backgroundColor: globals.COLOR.themeGreen
  },
  bannerText:{
    width: '80%',
    marginLeft:'5%',
    textAlign: "left",
    color: globals.COLOR.white,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 15
  },
  viewText:{
   // textAlign: "left",
   marginRight:25,
    textDecorationLine: 'underline',
    color: globals.COLOR.white,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 13
  },
  closeText: {
    textAlign: "left",
    color: globals.COLOR.white,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
    fontSize: 15
  },
  viewTextContainer:{

  },
  closeView:{
    alignItems: 'center',
    justifyContent: "center",
    width: '15%',
    height: 80,
    marginRight:'5%'
  },
  closeContainer:{
    position:'absolute',
    top:5,
    right:10
  }
});

export { images, styles  };
