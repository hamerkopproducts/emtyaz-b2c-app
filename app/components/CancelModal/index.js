
import React from "react";
import { View, Text, TouchableOpacity, I18nManager ,Image,TextInput} from "react-native";
import appTexts from "../../lib/appTexts";
import globals from "../../lib/globals";
import { styles } from "./styles";
import Modal from "react-native-modal";
import DropDownPicker from "react-native-dropdown-picker";

  const CancelModal = ({ isCancelModalVisible, onColse }) => {
  return (
    <Modal
            isVisible={isCancelModalVisible}
            style={styles.modalMaincontentHelp}
          >
            <View style={{backgroundColor: 'white', padding: 20}}>
              <TouchableOpacity
                style={styles.buttonwrapper}
                onPress={() => {
                  onColse(true);
                }}
              >
                <Text style={styles.closeButton}>X</Text>
                <View style={{flexDirection: 'row',marginBottom:'8%'}}>
                <Text style={styles.helpText}>{appTexts.CANCELMODAL.Heading}</Text>
                </View>
                
              </TouchableOpacity>
              <View style={{ paddingBottom: "5%", paddingTop: "2%" }}>
                <DropDownPicker
                  items={[{ label: "Not Interested", value: "sold out" }, { label: "out of", value: "Differnet" }]}
                  dropDownStyle={{placeholderTextColor: "red"}}
                  placeholder={appTexts.CANCELMODAL.Reason}
                  labelStyle={{
                    fontSize: 13,
                    fontFamily: I18nManager.isRTL
                      ? globals.FONTS.notokufiArabic
                      : globals.FONTS.poppinsRegular,
                    
                    textAlign: "left",
                    color: '#242424',
                    
                
                  }}
                  containerStyle={{ height: 30,margin:0}}
                  style={styles.dropDownStyle}
                  itemStyle={{
                    justifyContent: "flex-start",
                  }}
                  dropDownStyle={{ backgroundColor: "white" }}
                />
                <TextInput
                  style={styles.inputmessage}
                  //underlineColorAndroid="transparent"
                  placeholder={appTexts.CANCELMODAL.Typereason}
                  placeholderTextColor="#C9C9C9"
                  autoCapitalize="none"
                />
              </View>
              <View style={styles.buttonWrappers}>
                <TouchableOpacity onPress={() => console.log("click")}>
                  <View style={styles.submitButton}>
                    <Text style={styles.sendbuttonText}>
                      {appTexts.CANCELMODAL.Done}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
  );
};


export default CancelModal;
