import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: globals.COLOR.screenBackground,
  },

  screenContainer: {
    flex: 1,
    marginTop: globals.INTEGER.heightTen,
    flexDirection: "column",
    backgroundColor: globals.COLOR.white,
    // alignItems: 'center',
    // justifyContent:'center'
  },
  headerButtonContianer: {
    flexDirection: "row",
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: "center",
  },
  headerButton: {
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty,
  },
  headerButtonMargin: {
    marginLeft: globals.MARGIN.marginTen,
  },
  profileScreen: {
    //backgroundColor:'red',
    paddingLeft: "5%",
    paddingRight: "5%",
  },
  editArrow: {
    alignSelf: "flex-end",
    paddingTop: "4%",
  },
  profileEdit: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: "1%",
    paddingBottom: "5%",
    alignItems: "center",
    //backgroundColor:'red'
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1,
  },
  profileContent: {
    flexDirection: "row",
  },
  profileInformation: {
    
    justifyContent: "center",
    paddingLeft: "6%",
  },
  name: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.lightBlack,
    fontSize: 16,
  },
  address: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.drakGray,
    fontSize: 12,
  },
  place: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.drakGray,
    fontSize: 12,
  },
  // arrowicons:{
  //   transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]},
  // },
  arrowicons:{resizeMode: "contain",  
  transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]},
  profileItem: {
    paddingTop: "2%",
    // paddingLeft:'5%',
    // paddingRight:'5%'
  },
  modalMainContent: {
    //justifyContent: "center",
    justifyContent: "flex-end",
    margin: 0,
  },
  modalMaincontentHelp:{
  
    justifyContent: "flex-end",
    margin: 20,
  },
  modalmainView: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  closeButton:{
    alignSelf:'flex-end',
    paddingRight:'3%',
    paddingLeft:'3%',
    paddingTop:'2%',
    paddingBottom:'2%',
    color:'#707070',
    fontSize:16
  },
  restText:{
   alignItems:'flex-start' ,
   fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsMedium,
      fontSize:16,
   paddingRight:'3%',
    paddingLeft:'3%',
    paddingTop:'1%',
    paddingBottom:'2%'
  },resetPassword:{
    paddingRight:'3%',
    paddingLeft:'3%',
    paddingTop:'2%',
    paddingBottom:'5%'
  },
  passwordText:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
    fontSize:13,
    //paddingTop:'2%'
  },
  buttonWrapper:{
    paddingLeft:'3%',
    paddingRight:'3%',
    paddingBottom:'6%',
    paddingTop:'3%',
  },
  buttonWrappers:{
    // paddingLeft:'3%',
    // paddingRight:'3%',
    paddingBottom:'3%',
    paddingTop:'3%',
    alignSelf:'center'
  },
  logoutWrappers:{
     paddingLeft:'9%',
     paddingRight:'9%',
    paddingBottom:'15%',
    paddingTop:'12%',
    //alignSelf:'center',
    flexDirection:'row',
    justifyContent:'space-around'
  },
  cPassword:{
marginTop:-10,
flexDirection:'row',
borderBottomColor: "#EEEEEE",
borderBottomWidth: 1,
justifyContent: 'center',
    alignItems: 'center',

  },
  cdPassword:{
    // marginTop:-10,
    // flexDirection:'row',
    // borderBottomColor: "red",
    // borderBottomWidth: 1,
    
    
  },
  numberEnter:{
    fontSize: 16,
    color: "#242424",
    marginLeft:-4,
   // lineHeight: 9,
    alignItems: "center",
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
      flex:1,
      textAlign: I18nManager.isRTL ? "right" : "left",
   
  },imageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  pwdOne:{
    paddingTop:'5%'
  },
  inputsubject: {
    height: 40,
    marginTop: "5%",
    borderColor: "#DEDEDE",
    //backgroundColor: "#E8E8E8",
    borderWidth: 1,
    borderRadius: 1,
    fontSize: 14,
    textAlignVertical: "top",
    textAlign: I18nManager.isRTL ? "right" : "left",
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
    // fontFamily: isRTL ? "Noto Kufi Arabic" : "Montserrat-Medium",
    //fontFamily: "Montserrat-Medium",
  },
  dropDownStyle:{
      
      
     
  },
  inputmessage: {
    height: 140,
    marginTop: "5%",
    borderColor: "#DEDEDE",
    //backgroundColor: "#E8E8E8",
    borderWidth: 1,
    borderRadius: 1,
    textAlignVertical: "top",
    textAlign: I18nManager.isRTL ? "right" : "left",
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
    //fontFamily: "Montserrat-Medium",
    // textAlign: isRTL ? "right" : "left",
    // fontFamily: isRTL ? "Noto Kufi Arabic" : "Montserrat-Medium",
    fontSize: 14,
    padding: 10
  },
  helpText:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinssemiBold,
    fontSize:16, 
    //paddingLeft:'4%',
  },
  logoutText:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
    fontSize:15, 
    alignSelf:'center',
    paddingTop:'6%'
  },
  somePlaceholderStyle:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinssemiBold,
    fontSize:20, 
  },greenButton: {
    backgroundColor: globals.COLOR.themeGreen,
    width: 144,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
  },submitButton: {
    backgroundColor:  globals.COLOR.themeGreen,
    width: 144,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
  },
  yesButton:{
    backgroundColor:  globals.COLOR.themeGreen,
    width: 110,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
  },
  noButton:{
    backgroundColor:  globals.COLOR.themeOrange,
    width: 110,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
  },
  contactdetails:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    marginLeft: 5
  },
  contactTitle:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    marginLeft: 15, 
    marginRight: 5,
    flexDirection: 'row',
     alignItems: 'center'
  },
  cont:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
     alignItems: 'center'
  },
  contacts:{
    marginTop: 10,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
     flexDirection: 'row',
     alignItems: 'center',
     paddingLeft:'4%',
  },
  sendbuttonText:{
    color: globals.COLOR.white,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 15, 
  },
  flagsectionWrapper: {
    //flexBasis: "30%",
    width:'30%',
    //backgroundColor: "pink",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1,
  },
  resendWrapper: {
    flexDirection: "row",
    justifyContent: "center",
    paddingLeft: "3%",
    paddingRight: "3%",
     paddingTop: hp("3%"),
     paddingBottom: hp("2%"),
    //padding:'10%'
  },
  numbersectionWrapper: {
    //flexBasis: "70%",
    width:'75%',
    //backgroundColor: "pink",
    marginLeft: "2%",
  },
  numberEnters: {
    fontSize: 14,
    color: "#242424",
    lineHeight: 9,
    alignItems: "center",
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1,
    textAlign: I18nManager.isRTL ? "right" : "left",
  },
});

export { images, styles };