import PropTypes from 'prop-types';
import React from 'react';
import {  View, StatusBar, Text, TouchableOpacity } from 'react-native';
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import Category from '../Category';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Modal from 'react-native-modal';


const CategoryModal = (props) => {

  const {
   
   isCategoryModalVisible,
   //closeCategoryModal,
   toggleCategoryModal,
   categoryHeading,
   categorySubHeading,

      } = props;

   isCategoryModalVisible,
      console.debug(isCategoryModalVisible, '<<--isCategoryModalVisible')
  
  return (
    <Modal
    isVisible={isCategoryModalVisible} 
    //transparent={true}
    onRequestClose={() => null}
    style={styles.modalMainContent}
  >
    <View style={styles.modalmainView}>
                    <KeyboardAwareScrollView
                        scrollEnabled={true}
                        //contentContainerStyle={{ justifyContent: "flex-end" }}
                    >
        <View>
            <TouchableOpacity
              style={styles.buttonwrapper}
              onPress={() => {toggleCategoryModal();}}>
            
              <Text style={styles.closeButton}>X</Text>
               </TouchableOpacity>
        </View>
            <View style={styles.categoryHeading}>
                    <View style={styles.textWrapper}>
                        <Text style={styles.categoryTextHeading}>{categoryHeading}</Text>
                        <Text style={styles.lowerText}>{categorySubHeading}</Text>
                    </View>
                <View style={styles.buttonCover}>
                    <View style={styles.applyButton}>
                        <TouchableOpacity style={styles.applyButton}>
                            <Text style={styles.applyButtonText}>{appTexts.CATEGORY_CONTENT.Button1}</Text>
                            </TouchableOpacity>
                    </View>
                    <View style={styles.resetButton}>
                    <TouchableOpacity style={styles.resetButton}>
                            <Text style={styles.resetButtonText}>{appTexts.CATEGORY_CONTENT.Button2}</Text>
                            </TouchableOpacity>
                    </View>
                </View>
            </View>	
            <View style={styles.bordersStyle}></View>	
            <View style={styles.childComponent}>
            <Category itemText={appTexts.CATEGORY_CONTENT.Title} />	
            <Category itemText={appTexts.CATEGORY_CONTENT.Title2}/>	
            <Category itemText={appTexts.CATEGORY_CONTENT.Title3}/>	
            <Category itemText={appTexts.CATEGORY_CONTENT.Content}/>	
            <Category itemText={appTexts.CATEGORY_CONTENT.Title}/>	
            <Category itemText={appTexts.CATEGORY_CONTENT.Title}/>	
            <Category itemText={appTexts.CATEGORY_CONTENT.Title}/>	
            <Category itemText={appTexts.CATEGORY_CONTENT.Title}/>	
                </View>	
          
          
            
        
        </KeyboardAwareScrollView>
        </View>
            </Modal>
  );
};

CategoryModal.propTypes = {
 
};

export default CategoryModal;