import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";

const styles = StyleSheet.create({
    modalMainContent:{
      
        margin:0,
        //justifyContent: "flex-end",
        //position:'absolute',
          
    },
    modalmainView:{
      //height: '85%',
       marginTop: 'auto',
       backgroundColor:'white',
       paddingLeft:'5%',
       paddingRight:'4%',
       borderTopRightRadius:15,
       borderTopLeftRadius:15,
       borderColor: "rgba(0, 0, 0, 0.1)",
       borderWidth:1,
       overflow:'hidden'
    },
    buttonWrapper:{
      justifyContent:'flex-end',
    },
    closeButton:{
      alignSelf:'flex-end',
      //paddingRight:'3%',
      paddingLeft:'1%',
      paddingTop:'2%',
      paddingBottom:'2%',
      color:'#707070',
      fontSize:16
    },
    
    categoryHeading:{
      flexDirection:'row',
      //justifyContent:'space-around',
      paddingBottom:'6%',
      flex:1,
      //backgroundColor:'red',
      
      
    },
    categoryTextHeading:{
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
      fontSize:16,
      color:'black',
    },
    lowerText:{
      color:'green',
      fontSize:10,
    },
    buttonCover:{
      flexDirection:'row',
     //justifyContent:'space-between',
     //paddingTop:'2%',
     marginLeft:'27%',
     
    },
    applyButton:{
      
      width:'40%',
      height:'90%',
      marginRight:'3%',
      backgroundColor:'green',
      justifyContent:'center',
      alignItems:'center',
      
    
    },
    applyButtonText:{
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
      textAlign:'center',
      color:'white',
      fontSize:10,
      
    },
    resetButton:{
      
      width:'38%',
      height:'90%',
      backgroundColor:'orange',
      justifyContent:'center',
      alignItems:'center',
    
    },
    resetButtonText:{
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,    
      textAlign:'center',
      color:'white',
      fontSize:10,
    },
    
    
    bordersStyle:{
      borderWidth:0.5,
      borderColor:globals.COLOR.lightGray,
      
        
    },
    childComponent:{
      //backgroundColor:'blue',
      flex:0.5,
      paddingTop:'1%',
    },
});

export { styles };