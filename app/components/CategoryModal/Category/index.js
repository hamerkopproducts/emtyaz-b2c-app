import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, Text,TouchableOpacity,Image} from 'react-native';
import { styles } from "./styles";
import Icon from 'react-native-vector-icons/Entypo';

const Category = (props) => {
    const {
          itemText,
          
          
        } = props;
        const [isIconClicked,setIsIconClicked] = useState(false);
        

         const setIconClicked= () => {
             setIsIconClicked(!isIconClicked)
           };
          
          
return(
    <View style={styles.mainView}>
         <TouchableOpacity onPress={() => setIconClicked()}
         >
        <View style={styles.contentView}>
         <Text style={styles.categoryText}>{itemText}</Text>
         
         <Image source={isIconClicked ? require('../../assets/images/modal/checked.png') : require('../../assets/images/modal/uncheck.png')} />
        {/* <Icon name = {isIconClicked ?  "eye" : "eye-with-line"}
        style={{fontSize:22,color:'#ACACAC'}}
        onPress={() => setIconClicked(!isIconClicked)}/> */}
            </View>
            
            </TouchableOpacity>
            <View style={styles.bordersStyle}></View>
    </View>

);
    };
        Category.propTypes = {
            onItemClick:PropTypes.func,
            itemText:PropTypes.string
          };
          
          export default Category;        