import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
const images = {
  backIcon: require("../../assets/images/chooseLanguage/backarrow.png"),
  logoImage: require("../../assets/images/header/Emtyaz_logo_color.png"),
  arrowImage: require("../../assets/images/walkThrough/right.png"),
};
const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      backgroundColor: globals.COLOR.screenBackground
  },
  arrowWrapper: {
    paddingLeft: "5%",
    paddingTop: hp("2%"),
  },
  arrowicon: {
    width: 20,
    height: 20,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
  skipIcon: {
    width: 10,
    height: 10,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
  logoImage: {
    alignSelf: "center",
  },
  logoWrapper: {
    paddingTop: hp("3%"),
  },
  contentWrapper:{
    justifyContent:'center',
    paddingTop:hp('2%')
  },
  contentHeader:{
    textAlign:'center',
    paddingTop:hp('1%'),
    paddingBottom:hp('1%')
  },
  contentDescription:{
    textAlign:'center',
    paddingLeft:'4%',
    paddingRight:'4%',
    lineHeight:24
  },
  otpwrapper:{
    paddingLeft:'4%',
    paddingRight:'4%'
  },
  // buttonWrapper:{
  //   alignItems:'center',
  //   justifyContent:'center'
  // },
   image: {
    width: wp('83%'),
    height: hp('42%'),
    alignSelf: "center",
    resizeMode:'contain',
    marginTop:hp('4%')
    //marginTop:50
    //height:200,
    //flex:1,
    //flex:.5,
    //width:null
  },

  WalkthroughContents:{
    marginTop:hp('14%'),
    paddingLeft:'5%',
    paddingRight:'4%'
  },
  contentHeading:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
    color:globals.COLOR.drakBlack,
    //fontSize:18
    fontSize:13,
    marginBottom:hp('2%')
  },
  contentDescription:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    color:globals.COLOR.drakBlack,
    //lineHeight:20,
    //fontSize:18
    fontSize:13
  },
  buttonWrapper:{
    height:hp('10%'), 
    flexDirection:'row',
    //alignItems:'flex-end',
    justifyContent:'space-between',
    alignItems:'center',
    //backgroundColor:'red',
    //paddingBottom:'2%',
    paddingLeft:'5%',
    paddingRight:'5%',
    paddingBottom:hp('5%')
  },
  leftButton:{
    flexDirection:'row',
    alignItems:'center',
    
  },
  RightButton:{
    flexDirection:'row',
    //alignItems:'center'
  },
  skipText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
    color:globals.COLOR.drakBlack,
    fontSize:14,
    marginRight:3
  },
  nextText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
    color:globals.COLOR.drakBlack,
    fontSize:14,
    marginRight:3
  }
});

export { images, styles };
