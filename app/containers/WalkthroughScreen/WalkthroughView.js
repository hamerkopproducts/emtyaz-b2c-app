import React from "react";
import PropTypes from "prop-types";
import { View, StatusBar, Text, TouchableOpacity,Image,TextInput ,SafeAreaView} from "react-native";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import { images, styles } from "./styles";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import AppIntroSlider from "react-native-app-intro-slider";

const data = [
  {
    heading:appTexts.WALKTHROUGH_SLIDER.Heading,
    description:appTexts.WALKTHROUGH_SLIDER.Descritpion,
    image: require("../../assets/images/walkThrough/slide.png"),
  },
  {
    heading:appTexts.WALKTHROUGH_SLIDER.Heading,
    description:appTexts.WALKTHROUGH_SLIDER.Descritpion,
    image: require("../../assets/images/walkThrough/slide.png"),
  },
  {
    heading:appTexts.WALKTHROUGH_SLIDER.Heading,
    description:appTexts.WALKTHROUGH_SLIDER.Descritpion,
    image: require("../../assets/images/walkThrough/slide.png"),
  },
];


const WalkthroughView = (props) => {
  const {
    backArrowPress,
    signInPress,
    onFinish,
    currentIntroSlider,
    updateCurrentIntroSlider
  }= props;


  _renderItem = ({ item }) => {
    return (
      <View
        style={[
          styles.slide,
          {
            backgroundColor: item.bg,
          },
        ]}
      >
        <SafeAreaView>
          <Image source={item.image} style={styles.image} />
          <View style={styles.WalkthroughContents}>
          <Text style={styles.contentHeading}>{item.heading}</Text>
          <Text style={styles.contentDescription}>{item.description}</Text> 
          </View>

        </SafeAreaView>
      </View>
    );
  };

  _keyExtractor = (item, index) => index.toString();

  const { loginButtonPress } = props;
  return (
    <View style={styles.screenMain}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={globals.COLOR.screenBackground}
      />
      <View style={styles.arrowWrapper}>
      <TouchableOpacity onPress={backArrowPress}>
        <Image
          source={images.backIcon}
          resizeMode="contain"
          style={styles.arrowicon}
        />
         </TouchableOpacity>
      </View>
      {/* <View style={styles.logoWrapper}>
        <Image
          source={images.logoImage}
          resizeMode="contain"
          style={styles.logoImage}
        />
      </View> */}
      <AppIntroSlider
            ref={component => intro_slider = component}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            showDoneButton={false}
            showNextButton={false}
            dotStyle={{ backgroundColor: "green", marginBottom: hp('45%'), width: 26, height: 3 }}
            activeDotStyle={{ backgroundColor: "#8FBC8F", marginBottom: hp('45%'), width: 26, height: 3 }}
            data={data}
            onSlideChange={(a, b) => {
              updateCurrentIntroSlider(a);}}
          />
          
          <View style={styles.buttonWrapper}>
          <TouchableOpacity onPress={signInPress}>
              <View style={styles.leftButton}>
              <Text style={styles.skipText}>{appTexts.WALKTHROUGH_SLIDER.Skip}</Text>
              {/* <Image
          source={images.arrowImage}
          resizeMode="contain"
          style={styles.skipIcon} 
              />*/}
         
              </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => {
          const _nextIndex = currentIntroSlider + 1;
          updateCurrentIntroSlider(_nextIndex);
          if(data.length == _nextIndex) {
            onFinish();
          } else {
            intro_slider.goToSlide(_nextIndex);
          }
         }}>
              <View style={styles.leftButton}>
              <Text style={styles.skipText}>{appTexts.WALKTHROUGH_SLIDER.Next}</Text>
              <Image
          source={images.arrowImage}
          resizeMode="contain"
          style={styles.skipIcon}
        />
              </View>
              </TouchableOpacity>
              {/* <View style={styles.RightButton}>
              <Text style={styles.nextText}>{appTexts.WALKTHROUGH_SLIDER.Next}</Text>
              <Image
          source={images.arrowImage}
          resizeMode="contain"
          style={styles.skipIcon}
        />
              </View> */}
          </View> 
    
      
    </View>
  );
};

WalkthroughView.propTypes = {
  loginButtonPress: PropTypes.func,
  backArrowPress: PropTypes.func,
  signInPress: PropTypes.func,
};

export default WalkthroughView;
