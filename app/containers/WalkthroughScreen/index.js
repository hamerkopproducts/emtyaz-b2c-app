import React, { useState, useEffect } from 'react';
import { BackHandler } from 'react-native';
import { connect } from 'react-redux';
import WalkthroughView from './WalkthroughView';
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";
import ChooseLanguageScreen from '../../containers/ChooseLanguageScreen';
import SigninScreen from '../../containers/SigninScreen';

const WalkthroughScreen = (props) => {
  //mounted
  useEffect(() => handleComponentMounted(), []);
  const [currentIntroSlider, updateCurrentIntroSlider] = useState(0);

  const handleComponentMounted = () => {
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      BackHandler.exitApp();
      return true;
    });
  };

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
  };

  const loginButtonPress = () => {
    props.doLogin({});
  };

  const backArrowPress = () => {
    props.navigation.navigate('ChooseLanguageScreen')
  }

  const signInPress = () => {
    props.navigation.navigate('SigninScreen')
  }
  const nextButtonPressed =() => {
    console.log('next')
   }
 
   const introFinished = () => {
     //props.introFinished();
     props.navigation.navigate('SigninScreen');
   }

  return (
    <WalkthroughView loginButtonPress={loginButtonPress}
    backArrowPress={backArrowPress}
    signInPress={signInPress}
    onFinish={introFinished}
    nextButtonPressed={nextButtonPressed}
    currentIntroSlider={currentIntroSlider}
    updateCurrentIntroSlider={(index) => updateCurrentIntroSlider(index)}/>
  );

};


const mapStateToProps = (state, props) => {
  return {
  
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    introFinished: LoginActions.introFinished
  }, dispatch)
};

const WalkthroughWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(WalkthroughView);

WalkthroughWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default WalkthroughScreen;
