import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,I18nManager
} from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";

import Header from "../../components/Header";
import PropTypes from "prop-types";
import appTexts from "../../lib/appTexts";
import { ScrollView } from "react-native-gesture-handler";


const MapView = (props) => {
  const {
    backHomeClick,
  } = props;
  return(


<View style={styles.mapScreenView}>
<Header
				navigation={props.navigation}
				// isLogoRequired={true}
				// isLanguageButtonRequired={true}
				headerTitle={appTexts.STRING.Location}
        isRightButtonRequired={false}
        isBackButtonRequired={true} onBackButtonPress={backHomeClick}
				isSearchButtonRequired={false}
				//onCartButtonPress={onCartButtonPress}
				//onSearchPress={onSearchPress}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>
      <TouchableOpacity style={styles.floating} onPress={backHomeClick}>
    <View style={styles.bottonBoxView}>
    <View style={styles.leftWrapper}>
            <Image style={styles.mapImage} source={require('../../assets/images/map/map_white2.png')}></Image>
            <Text style={styles.locationText}>Al Nameem</Text>
      </View> 
      <View style={styles.iconStyle}>
            <Image style={styles.mapIcon} source={require('../../assets/images/map/gps.png')}></Image>
      </View>
        
    </View>
    </TouchableOpacity>
</View>
  );
};
MapView.propTypes = {
    backHomeClick:PropTypes.func,
  };
  
export default MapView;