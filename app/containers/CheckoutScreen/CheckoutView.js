import React,{useState} from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,Image,
  TouchableOpacity,
  CheckBox,
  ScrollView,
} from "react-native";
import globals from "../../lib/globals";
import { styles,images } from "./styles";
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";
import CustomGreenLargeButton from "../../components/CustomGreenLargeButton";
import Customradiobutton from "../../components/CustomRadioButton";
import OrderSummary from '../../components/OrderSummary';
import Address from '../../components/Address';

const CheckoutView = (props) => {

  const { logoutButtonPress,
onBackButtonPress } = props;

   return (
    <View style={styles.screenMain}>
        
      <Header
        navigation={props.navigation}
        isLogoRequired={false}
        isBackButtonRequired={true}
        onBackButtonPress={onBackButtonPress}
        // isRightButtonRequired={true}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />

      <View style={styles.contentWrapper}>
      <ScrollView>
        <OrderSummary />  
        <View style={styles.box}>
        
        <Address titleText={appTexts.SUMMARY.delAd}/>
        <View style={{marginTop:'5%'}}>
        <Address  titleText={appTexts.SUMMARY.billAd}/>
        </View>
        </View>
        <View style={styles.calView}>
            <View style={{marginTop:'5%'}}>
                 <Text style={styles.locText}>{appTexts.SUMMARY.locCal}</Text> 
                 <View style={styles.blueText}>
                 <Text style={styles.date}>{appTexts.SUMMARY.date}</Text>
                 <Text style={styles.blue}>{appTexts.SUMMARY.time}</Text>
                 </View>
                 </View>
        </View>
       

        {/* <View style={styles.buttonWrapper}>
          <TouchableOpacity onPress={() => console.log("click")}>
            <CustomGreenLargeButton
              buttonText={appTexts.BILLINGDELIVERY.AddNewAddress}
            />
          </TouchableOpacity>
        </View> */}
       
      </ScrollView>
      </View>
      <View style={styles.tabContainer}>
            <View style={styles.tabOne}>
                <Text style={styles.textArea}>{appTexts.SUMMARY.Amount}</Text>
                <View style={styles.second}>
                <Text style={styles.butText}>{appTexts.SUMMARY.amount}</Text>
                </View>
            </View>
            <View style={styles.tabTwo}>
                <View style={styles.tabTwoText}>
                    <Text style={styles.tabToText}>PAY NOW</Text>
                </View>
            </View>
        </View>
    </View>
  );
};

CheckoutView.propTypes = {
  logoutButtonPress: PropTypes.func,
};

export default CheckoutView;
