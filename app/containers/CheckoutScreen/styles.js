import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  edit:require("../../assets/images/profileItem/edit.png")
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      //flexDirection:'column',
      backgroundColor: globals.COLOR.screenBackground
  },
  screenContainer:{
    flex:1,
    marginTop:  globals.INTEGER.heightTen,
    flexDirection: 'column',
    backgroundColor: globals.COLOR.white,
    alignItems: 'center',
    justifyContent:'center'
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  contentWrapper:{
//backgroundColor:'red',
paddingLeft:'4%',
paddingRight:'4%',
paddingTop:'7%',
flex:1
  },
  box:{
      borderWidth:0.5,
      borderRadius:10,
      paddingLeft:'3%',
      paddingRight:'3%',
      marginBottom:'5%',
      paddingTop:'3%',
      //marginTop:'3%',

  },
  del:{
      flexDirection:'row',
      justifyContent:'space-between',
  },
  deliveryText:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinssemiBold,
    fontSize:14,
  },
  addressWrapper:{
    flexDirection:'row',
    justifyContent:'space-between',
    paddingTop:'5%'
,
  },
  nameText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 13,
    color:'#383838'
  },
  userText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 13,
    color:'#686868',
    lineHeight:21,
    paddingLeft:'1%'
  },
  textContent:{flexDirection:'row'},
  billingnameandIcon:{
    flexDirection:'row',
    justifyContent:'space-between',alignItems:'center',paddingTop:'5%',paddingBottom:'3%'
  },
  userDetails:{
    paddingTop:'1%',
    borderBottomColor:'#CFCFCF',
    borderBottomWidth:1,
    paddingBottom:'6%',
   
      },
      auserDetails:{
        paddingTop:'4%',
       
        paddingBottom:'6%'
      },
      buttonWrapper:{
    //      position: 'absolute',
    // bottom:10,
    justifyContent:'center',
    alignItems: 'flex-end',
    flex: 1,
   
    //backgroundColor:'red'
   
      },
      calView:{
          borderWidth:0.5,
          borderRadius:10,
          height:150,
          marginBottom:'10%',
          paddingLeft:'5%',
          //marginTop:'5%',
      },
      blueText:{
          flexDirection:'row',

      },
      date:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinsRegular,
        fontSize:14,
        color:globals.COLOR.lightTextColor,
      },
      blue:{
          color:'blue',
          fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
    fontSize:12,
    alignSelf:'center'
    //paddingTop:'1%'
      },
      locText:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinsRegular,
        fontSize:14,
        color:globals.COLOR.lightTextColor, 
      },
      tabContainer:{
          flexDirection:'row',
          borderWidth:0.5,
          width:'100%',
          height:70,
         // flex:1,
      },
      tabOne:{
        flex:0.5,
       // backgroundColor:'red',
        alignItems:'center',
        justifyContent:'center'
      },
      tabTwo:{
        flex:0.5,
        backgroundColor:globals.COLOR.themeGreen,
        alignItems:'center',
        justifyContent:'center'
      },
      second:{
          alignSelf:'flex-start',
          paddingLeft:'14%'
      },
      textArea:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinsMedium,
        fontSize:12,
      },
      butText:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinsMedium,
        fontSize:14,
        color:globals.COLOR.textColor,
      },
      tabToText:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinsMedium,
        fontSize:14,
        color:'white'
      }
});

export { images, styles };
