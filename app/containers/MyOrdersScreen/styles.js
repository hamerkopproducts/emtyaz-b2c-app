import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      flexDirection:'column',
      backgroundColor: globals.COLOR.screenBackground
  },
  screenContainer:{
    flex:1,
    marginTop:  globals.INTEGER.heightTen,
    flexDirection: 'column',
    backgroundColor: globals.COLOR.white,
    alignItems: 'center'
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  container: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: 60,
    alignItems: 'center',
    flexDirection: 'row'
  },
  leftHeadingContainer: {
    width: '38%',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  itemImage:{
    marginLeft: 5,
  },
  filterLabel: {
    marginLeft: 5,
    marginRight: 5,
    color: globals.COLOR.greenTexeColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
    fontSize: 11
  },
  orderLabel:{
    marginLeft: 5,
    marginRight: 5,
    color: globals.COLOR.white,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
    fontSize: 11
  },
  myOrdersContainer: {
    marginLeft:'10%',
    height: 32,
    width: '40%',
    marginRight: '1%',
    backgroundColor: globals.COLOR.themeOrange,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  myQuotationContainer: {
    height: 32,
    width: '40%',
    marginRight: '1%',
    backgroundColor:globals.COLOR.themeGreen,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  filterContainer: {
    borderWidth: 1,
    borderColor: '#37B34A',
    height: 32,
    width: '20%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewChangeContainer: {
    borderWidth: 1,
    borderColor: 'green',
    height: 30,
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  listContainer: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  flatListStyle: {
    width: '100%'
  },
  noDataContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  noDataText: {
    textAlign: 'center',
    color: globals.COLOR.lightTextColor,
    fontSize: 12
  },
  modalmainView: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  modalMaincontentHelp:{
  
    justifyContent: "center",
    //justifyContent: "flex-end",
   margin: 0,
},
buttonWrapper:{
  paddingLeft:'3%',
  paddingRight:'3%',
  paddingBottom:'6%',
  paddingTop:'3%',
},
});

export { images, styles };