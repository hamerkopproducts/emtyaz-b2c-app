import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,I18nManager
} from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";

import Header from "../../components/Header";
import PropTypes from "prop-types";
import appTexts from "../../lib/appTexts";
import MyOrdersCard from "../../components/MyOrdersCard";
import OrderFilterModal from "../../components/OrderFilterModal";
const MyOrdersView = (props) => {
  const {
    isMyOrderFilterModalVisible,
    onOrderFilterPress,
    onOrderStatusPress,
    closeFiltermodal,
    onCartButtonPress,
    itemClick,
    listData,
    changeTab,
    tabIndex,
    onBackButtonPress
  } = props;
  const renderItem = ({ item, index }) => 
  <MyOrdersCard item={item} itemClick={itemClick} tabIndex={tabIndex} />;
  return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        isLogoRequired={false}
         //headerTitle={appTexts.MY_ORDERS.MyOrders}
        // headerTitle={tabIndex === 0 ? (I18nManager.isRTL ? 'طلباتك' : 'My Orders'):(I18nManager.isRTL ? 'My Quotations':'My Quotations')}
        headerTitle={tabIndex === 0 ? appTexts.MY_ORDERS.MyOrders :appTexts.MY_ORDERS.MyQuotations}
        headerTitle={appTexts.MY_ORDERS.heading}
        isBackButtonRequired={true}
        onBackButtonPress={onBackButtonPress}
        //isLanguageButtonRequired={true}
        isRightButtonRequired={true}
        onCartButtonPress={onCartButtonPress}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      <View style={styles.screenContainer}>
        <View style={styles.container}>
          <TouchableOpacity style={styles.myOrdersContainer} onPress={() => { changeTab(0)}}>
            <Image style={styles.itemImage} source={require('../../assets/images/myOrders/my-orders-white.png')} />
            <Text style={styles.orderLabel}>{appTexts.MY_ORDERS.MyOrders}</Text>

          </TouchableOpacity>
          <TouchableOpacity style={styles.myQuotationContainer} onPress={() => { changeTab(1) }}>
            <Image style={styles.itemImage} source={require('../../assets/images/myOrders/my_quotations.png')} />
            <Text style={styles.orderLabel}>{appTexts.MY_ORDERS.MyQuotations}</Text>
          </TouchableOpacity>

          {/* <TouchableOpacity style={styles.filterContainer} onPress={() => { onOrderFilterPress() }}>
            <Image style={styles.itemImage} source={require('../../assets/images/products/filter.png')} />
            <Text style={styles.filterLabel}>{appTexts.MY_ORDERS.Filter}</Text>
            </TouchableOpacity> */}
        </View>
        <View style={styles.listContainer}>
          {listData.length === 0 ? 
          <View style={styles.noDataContainer}>
            <Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text>
            </View> :
            <FlatList
              style={styles.flatListStyle}
              data={listData}
              extraData={listData}
              keyExtractor={(item, index) => index.toString()}
              /*onEndReachedThreshold={0.5}
              onEndReached={({ distanceFromEnd }) => {
                if (listData.length >= (currentPage * pageLimit)) {
                  loadMoreItems();
                }
              }}*/
              //onRefresh={() => { onRefresh() }}
              //refreshing={isRefreshing}
              
              showsVerticalScrollIndicator={false}
              renderItem={renderItem} />}
        </View>
      </View>
    {/* <OrderFilterModal 
    isMyOrderFilterModalVisible={isMyOrderFilterModalVisible} 
    closeFiltermodal={closeFiltermodal} 
    onOrderStatusPress={onOrderStatusPress}
    statusList={listData}
    navigation={props.navigation}
    /> */}
    </View>
  );
};

MyOrdersView.propTypes = {
onCartButtonPress: PropTypes.func,
  changeTab: PropTypes.func,
  tabIndex: PropTypes.number,
};

export default MyOrdersView;