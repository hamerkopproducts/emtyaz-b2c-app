import { StyleSheet,I18nManager } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import globals from "../../../lib/globals";

const styles = StyleSheet.create({
  itemConatiner: {
    paddingLeft:'5%',
    paddingRight:'5%'
    //width: '100%',
    //height: 60,
    //backgroundColor:'red'
  },
  listWrapper:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
   paddingTop:hp('2%'),
   paddingBottom:'3%',
  // paddingLeft:'5%',
   borderBottomColor:'#EEEEEE',
   borderBottomWidth:1
   //paddingRight:'5%'
  },
  imageMain:{
    resizeMode: "contain", width: 27, height: 27
  },
  contentHeading:{
    //fontFamily: globals.FONTS.avenirMedium,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize:14,
    paddingLeft:'5%',
    paddingRight:'4%',
    color:globals.COLOR.lightBlack,
    textAlign:"left"
    //color:'#707070'
  },
  contentsWrapper:{
     
  },
  mainHead:{
    paddingTop:'8%'

  },
  subText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize:10,
    paddingLeft:'4%',
    paddingRight:'4%',
    color:globals.COLOR.drakGray,
    textAlign:"left"
  },
  arrowWrapper:{
    flexDirection:'row',
    paddingRight:'1%'
  },
  arrowImage:{resizeMode: "contain", width:12,height:12, 
transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]},


});

export { styles  };
