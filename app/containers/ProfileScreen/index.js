import React, { useEffect, useState } from "react";

import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import ProfileView from "./ProfileView";
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";
import globals from "../../lib/globals";
import functions from "../../lib/functions";
import { add } from "react-native-reanimated";

const ProfileScreen = (props) => {
   //Initialising states
   const [isModalVisible, setIsModalVisible] = useState(false);
   const [isHelpModalVisible, setIsHelpModalVisible] = useState(false);
   const [isLogoutpModalVisible, setIsLogoutModalVisible] = useState(false);
   const [isUpdateModalVisible, setIsUpdateModalVisible] = useState(false);
   const [isRequestModalVisible, setIsRequestModalVisible] = useState(false);
   const [ishidePass, setIsHidePass] = useState(true);
   const [ishideNewPass, setIsHideNewPass] = useState(true);
   const [isconfirmHidePass, setIsConfirmHidePass] = useState(true);
   
  //will focus
  useEffect(() => {
    return props.navigation.addListener("focus", () => {
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  };

  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);

  const handleComponentUnmount = () => {};

  //updated
  useEffect(() => handleComponentUpdated(
    
  ));

  const handleComponentUpdated = () => {};

  const logoutButtonPress = () => {
    props.doLogout();
  };

  const toggleModal = () => {
    setIsModalVisible(!isModalVisible);
  };

  const toggleHelpModal = () => {
    setIsHelpModalVisible(!isHelpModalVisible)
  };
  const toggleLogoutModal = () => {
    setIsLogoutModalVisible(!isLogoutpModalVisible)
  };
  const toggleUpdateModal = () => {
    setIsUpdateModalVisible(!isUpdateModalVisible)
  };
  const toggleRequestModal = () => {
    setIsRequestModalVisible(!isRequestModalVisible)
  };

  
  const setHidePass =() => {
    setIsHidePass(!ishidePass)
  }
  const setHideNewPass =() => {
    setIsHideNewPass(!ishideNewPass)
  }

  const setHideConfirmPass =() => {
    setIsConfirmHidePass(!isconfirmHidePass)
  }
  const favouritemsClicked = () => {
    props.navigation.navigate("FavouritesScreen");
  };
  const faqPress = () => {
    props.navigation.navigate("FaqScreen");
  };
  const privacyPress = () => {
    props.navigation.navigate("PrivacyScreen");
  };
  const termsPress = () => {
    props.navigation.navigate("TermsScreen");
  };
  const supportPress = () => {
    props.navigation.navigate("");
  };
  const sharePress = () => {
    props.navigation.navigate("");
  };
  const logoutPress = () => {
    props.navigation.navigate("");
  };
  const addressClicked = () => {
    props.navigation.navigate('AddressdetailsScreen')
  };
  const myOrderClicked =() => {
    props.navigation.navigate('MyOrdersScreen')
  }

  return (
    <ProfileView
      logoutButtonPress={logoutButtonPress}
      faqPress={faqPress}
      privacyPress={privacyPress}
      termsPress={termsPress}
      supportPress={supportPress}
      sharePress={sharePress}
      logoutPress={logoutPress}
      isModalVisible={isModalVisible}
      toggleModal={toggleModal}
      toggleHelpModal={toggleHelpModal}
      isHelpModalVisible={isHelpModalVisible} 
      toggleLogoutModal={toggleLogoutModal}
      isLogoutpModalVisible={isLogoutpModalVisible}
      toggleUpdateModal={toggleUpdateModal}
      isUpdateModalVisible={isUpdateModalVisible}
      toggleRequestModal={toggleRequestModal}
      favouritemsClicked={favouritemsClicked}
      isRequestModalVisible={isRequestModalVisible}
      ishidePass={ishidePass}
      setHidePass={setHidePass}
      ishideNewPass={ishideNewPass}
      setHideNewPass={setHideNewPass}
      setHideConfirmPass={setHideConfirmPass}
      isconfirmHidePass={isconfirmHidePass}
      addressClicked={addressClicked}
      myOrderClicked={myOrderClicked}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      doLogout: LoginActions.doLogout,
    },
    dispatch
  );
};

const profileScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileScreen);

profileScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default profileScreenWithRedux;
