import React from "react";
import PropTypes from "prop-types";
import {
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  Image,
  TextInput,ScrollView,CheckBox
} from "react-native";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import { images, styles } from "./styles";
import CustomGreenLargeButton from "../../components/CustomGreenLargeButton";
import Modal from "react-native-modal";

const SignupView = (props) => {
  const {
    signInClick,
    termsClick,
    backPress,
    isIconClicked,
    checkIconClicked,
  } = props;
  return (
     <ScrollView>  
    <View style={styles.screenMain}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={globals.COLOR.screenBackground}
      />

      <View style={styles.arrowWrapper}>
        <TouchableOpacity
          onPress={backPress}
          
        >
          <Image
            source={images.backIcon}
            resizeMode="contain"
            style={styles.arrowicon}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.logoWrapper}>
        <Image
          source={images.logoImage}
          resizeMode="contain"
          style={styles.logoImage}
        />
      </View>
      <View style={styles.contentWrapper}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <Text style={styles.contentHeader}>{appTexts.SIGNIN.Heading}</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <Text style={styles.contentDescription}>
          {appTexts.SIGNUP.Description}
        </Text>
      </View>
      </View>

      <View style={styles.fullName}>
      <View style={{ flexDirection: 'row' }}>
        <Text style={styles.formcontentDescription}>
          {appTexts.SIGNUP.Fullname}
        </Text>
        </View>
        <View style={{marginTop:-12}}>
        <TextInput
          style={styles.formInput}
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
        />
        </View>
      </View>
      <View style={styles.emailAddress}>
      <View style={{ flexDirection: 'row' }}>
        <Text style={styles.formcontentDescription}>
          {appTexts.SIGNUP.Email}
        </Text>
        </View>
        <View style={{marginTop:-12}}>
        <TextInput
          style={styles.formInput}
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
        />
        </View>
      </View>

      <View style={styles.emailAddress}>
      <View style={{ flexDirection: 'row' }}> 
        <Text style={styles.formcontentDescription}>
          {appTexts.SIGNUP.PHONE}
        </Text>
        </View>
        <View style={styles.phonewrapper}>
       <View style={styles.nationalFlag}>
       <Image
            source={require("../../assets/images/Icons/flag.png")}
            style={{ height: 18, width: 18 }}
          />
          <View style={{ flexDirection: 'row', marginLeft: 2 }}></View>
          <TextInput
            editable={false}
            style={styles.disableText}
            placeholder={"+966"}
            placeholderTextColor="#282828"
          />
       </View>
       
       <View style={styles.nationalNumber}>
       <TextInput
                style={styles.enableText}
                placeholder={appTexts.SIGNIN.PHONE}
                placeholderTextColor="#242424"
                keyboardType="number-pad"
                autoFocus={true}
              // value={number}

              />
       </View>
        </View>

      </View>

      {/* <View style={styles.companyName}>
        <Text style={styles.formcontentDescription}>
          {appTexts.SIGNUP.compnayname}
        </Text>
        <View style={{marginTop:-12}}>
        <TextInput
          style={styles.formInput}
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
        />
        </View>
      </View>

      <View style={styles.companyName}>
        <Text style={styles.formcontentDescription}>
          {appTexts.SIGNUP.companynumber}
        </Text>
        <View style={{marginTop:-12}}>
        <TextInput
          style={styles.formInput}
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
        />
        </View>
      </View>

      <View style={styles.companyName}>
        <Text style={styles.formcontentDescription}>
          {appTexts.SIGNUP.VAT}
        </Text>
        <View style={{marginTop:-12}}>
        <TextInput
          style={styles.formInput}
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
        />
        </View>
      </View> */}

      <View style={styles.acceptSection}>
      <TouchableOpacity onPress={() => checkIconClicked()}>
          {/* isCheck ? 
          <Image style={styles.imageCheck} source={require('../../assets/images/signup/uncheck.png')}/> :
        <Image style={styles.imageCheck} source={require('../../assets/images/signup/checked_box.png')}/>  */}
        <Image  style={styles.imageCheck} source={isIconClicked ? require("../../assets/images/signup/checked_box.png")
									: require("../../assets/images/signup/uncheck.png")} />
        </TouchableOpacity>
<Text style={styles.conitionText}> {appTexts.SIGNUP.Terms}</Text>
<TouchableOpacity onPress= {termsClick}>
<Text style={styles.termsText}> {appTexts.SIGNUP.Condition}</Text>
</TouchableOpacity>
      </View>

           <View style={styles.buttonWrapper}>
        <TouchableOpacity onPress={signInClick}>
          <CustomGreenLargeButton buttonText={appTexts.SIGNUP.NEXT} />
        </TouchableOpacity>
      </View>
       <View style={styles.signtextWrapper}>
        <View
          style={{
            width: "15%",
            borderBottomWidth: 1,
            borderBottomColor: globals.COLOR.lightGray,
            borderRadius: 0.2,
            borderStyle: "dashed",
            marginRight: 10,
          }}
        />
        <Text style={styles.orText}>{appTexts.SIGNIN.ORWITH}</Text>
        <View
          style={{
            width: "15%",
            borderBottomWidth: 1,
            borderBottomColor: globals.COLOR.lightGray,
            borderRadius: 0.2,
            borderStyle: "dashed",
            marginLeft: 10,
          }}
        />
      </View> 
       <View style={styles.socialMedia}>
        <View style={styles.faceBook}>
        <Image
            source={images.facebookIcon}
            resizeMode="contain"
            style={styles.socialicon}
          />
        </View>
        <View style={styles.googlePlus}>
        <Image
            source={images.googleIcon}
            resizeMode="contain"
            style={styles.socialicon}
         
          />
        </View>
      </View> 
    </View>
    </ScrollView>  
  );
};

SignupView.propTypes = {
  signInClick :PropTypes.func,
  termsClick:PropTypes.func,
  backPress:PropTypes.func,
};

export default SignupView;
