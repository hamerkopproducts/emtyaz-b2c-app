import React, { useState, useEffect } from "react";
import { BackHandler } from "react-native";
import { connect } from "react-redux";
import SignupView from "./SignupView";
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";

const SignupScreen = (props) => {
  //Initialising states
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isIconClicked,setIsIconClicked] = useState(false);


  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    BackHandler.addEventListener(
      "hardwareBackPress",
      (backPressed = () => {
        BackHandler.exitApp();
        return true;
      })
    );
  };

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {};

  const loginButtonPress = () => {
    props.doLogin({});
  };
  const toggleModal = () => {
    setIsModalVisible(!isModalVisible);
  };
  const signInClick = () => {
    props.navigation.navigate('SigninScreen');
  };

  const termsClick = () => {
    props.navigation.navigate('TermsScreen');
  };

  const backPress = () => {
    props.navigation.navigate('SigninScreen');
  };

  const checkIconClicked = () => {
    setIsIconClicked(!isIconClicked)
  };

  return (
    <SignupView signInClick={signInClick}
    termsClick={termsClick}
    backPress={backPress}
    checkIconClicked={checkIconClicked}
    isIconClicked={isIconClicked}
     />
  );
};

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      doLogin: LoginActions.doLogin,
    },
    dispatch
  );
};

const SignUpWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupScreen);

SignUpWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default SignUpWithRedux;
