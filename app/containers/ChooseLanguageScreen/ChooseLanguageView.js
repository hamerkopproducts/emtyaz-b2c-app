import React from "react";
import PropTypes from "prop-types";
import { View, StatusBar, Text, TouchableOpacity, Image } from "react-native";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import { images, styles } from "./styles";

const ChooseLanguageView = (props) => {
  const { selectLanguage } = props;
  return (
    <View style={styles.screenMain}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={globals.COLOR.screenBackground}
      />

      {/* <View style={styles.arrowWrapper}>
      <TouchableOpacity onPress={() => {
                console.log("backButton");
              }}>
        <Image
          source={images.backIcon}
          resizeMode="contain"
          style={styles.arrowicon}
        />
         </TouchableOpacity>
      </View> */}
      <View style={styles.logoWrapper}>
        <Image
          source={images.logoImage}
          resizeMode="contain"
          style={styles.logoImage}
        />
      </View>

      <View style={styles.contentWrapper}>
        <Text style={styles.langText}>{appTexts.SELECT_LANGUAGE.Chooseyourlanguage}</Text>
      </View>
      <View style={styles.buttonWrapper}>
        {/*<View style={styles.lowerPadding}>*/}
           <TouchableOpacity onPress={() => {
                selectLanguage("EN");
              }}>
          <View style={styles.yellowButton}>
            <Text style={styles.leftbuttonText}>{appTexts.SELECT_LANGUAGE.En}</Text>
          </View>
        </TouchableOpacity>
        
        <View>
        <TouchableOpacity   onPress={() => {
                selectLanguage("AR");
              }}>
          <View style={styles.greenButton}>
            <Text style={styles.RightbuttonText}>{appTexts.SELECT_LANGUAGE.AR}</Text>
          </View>
        </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

ChooseLanguageView.propTypes = {
  selectLanguage: PropTypes.func,
};

export default ChooseLanguageView;
