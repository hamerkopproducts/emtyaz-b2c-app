import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
const images = {
  backIcon: require("../../assets/images/chooseLanguage/backarrow.png"),
  logoImage: require("../../assets/images/header/Emtyaz_logo_color.png"),
};

const styles = StyleSheet.create({
  screenMain: {
    flex: 1,
    backgroundColor: globals.COLOR.screenBackground,
  },
  arrowWrapper: {
    paddingLeft: "5%",
    paddingTop: hp("2%"),
  },
  arrowicon: {
    width: 20,
    height: 20,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
  logoImage: {
    alignSelf: "center",
  },
  logoWrapper: {
    paddingTop: hp("3%"),
  },
  contentWrapper: {
    //alignSelf:'center',
    //flex:1,
    backgroundColor: "white",
    alignItems: "center",
    paddingTop: hp("27%"),
    //justifyContent:'center'
  },
  buttonWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems:"center",
    paddingLeft: "8%",
    paddingRight: "8%",
    paddingTop:hp('3%'),
    
  },
  lowerPadding:{
    paddingBottom:'3%',
  },
  greenButton: {
    backgroundColor: globals.COLOR.themeGreen,
    width: 150,
    height: 48,
    borderRadius:10,
    justifyContent: "center",
    alignItems: "center",
  },
  yellowButton: {
    backgroundColor: globals.COLOR.themeOrange,
    width: 150,
    height: 48,
    borderRadius:10,
    justifyContent: "center",
    alignItems: "center",
  },

  leftbuttonText: {
    color:globals.COLOR.white,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 15,
  },
  RightbuttonText: {
    color: globals.COLOR.white,
    fontFamily: globals.FONTS.notokufiArabic,
    fontSize: 15,
  },
  langText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.poppinssemiBold : globals.FONTS.poppinssemiBold,
    color:globals.COLOR.drakBlack,
    fontSize:16,
  }
});

export { images, styles };
