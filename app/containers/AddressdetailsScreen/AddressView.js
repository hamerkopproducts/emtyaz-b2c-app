import React from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  CheckBox,
  I18nManager,
} from "react-native";
import globals from "../../lib/globals";
import { styles, images } from "./styles";
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";
import Modal from "react-native-modal";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import DropDownPicker from "react-native-dropdown-picker";
import Icon from "react-native-vector-icons/FontAwesome";
const AddressView = (props) => {
  const { toggleModal, isModalVisible,onBackButtonPress,editAdClicked } = props;

  return (
   
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        isLogoRequired={false}
        headerTitle={appTexts.ADDRESSDETAILS.Heading}
        isBackButtonRequired={true}
        onBackButtonPress={onBackButtonPress}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      <View style={styles.screenContainer}>
        <ScrollView>
          <View
            style={{
              paddingLeft: "5%",
              paddingRight: "5%",
              paddingTop: "5%",
              paddingBottom: "5%",
            }}
          >
            <View style={[styles.shadowContainerStyle, { width: "100%" }]}>
              <View style={styles.delivreyAddress}>
                <Text style={styles.deliveryText}>
                  {appTexts.ADDRESSDETAILS.DeliveryAddress}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    toggleModal();
                  }}
                >
                  <View style={styles.addnewButton}>
                    <Text style={styles.buttonText}>
                      {appTexts.ADDRESSDETAILS.AddNew}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.delivreyaddressDetail}>
                <View style={styles.nameandIcon}>
                  <Text style={styles.nameText}>John Mathew</Text>
                  <View style={styles.iconList}>
                    <TouchableOpacity onPress={editAdClicked}>
                    <View style={{ paddingHorizontal: "3%" }}>
                    <Image
                  source={require("../../assets/images/profileItem/edit.png")}
                  resizeMode="contain"
                  style={styles.arrowicon}
                />
                    </View>
                    </TouchableOpacity>
                   
                    <Icon
                      style={{ fontSize: 16, color: "#ACACAC" }}
                      name="trash-o"
                    />
                  </View>
                </View>
               
                  {/* <View style={styles.iconheart}>
                    <Icon
                      style={{ fontSize: 16, color: "#f7aa1a" }}
                      name="heart"
                    />
                  </View> */}
                <View style={styles.userDetails}>
                   <View style={styles.textContent}>
                    <View style={{ paddingLeft: "0%" }}>
                    <View style={styles.textContent}>
                       <Text style={styles.placeText}>Al Bassam 2</Text>
                        <Text style={styles.placeText}>Al Naemia Street</Text>
                        <Text style={styles.placeText}>PO Box: 2211</Text>
                        <Text style={styles.placeText}>Al Nameem,Jaddah</Text>
                        <Text style={styles.placeText}>+966 00000</Text>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={styles.nameandiconSecond}>
                  <Text style={styles.nameText}>John Mathew</Text>
                  <View style={styles.iconList}>
                    <TouchableOpacity onPress={editAdClicked}>
                    <View style={{ paddingHorizontal: "3%" }}>
                    <Image
                  source={require("../../assets/images/profileItem/edit.png")}
                  resizeMode="contain"
                  style={styles.arrowicon}
                />
                    </View>
                    </TouchableOpacity>
                    <Icon
                      style={{ fontSize: 16, color: "#ACACAC" }}
                      name="trash-o"
                    />
                  </View>
                </View>
                <View style={styles.userDetails}>
                <View style={styles.textContent}>
                    <View style={{ paddingLeft: "0%" }}>
                    <View style={styles.textContent}>
                        <Text style={styles.placeText}>Al Naemia Street</Text>
                      </View>
                      <View style={styles.textContent}>
                        <Text style={styles.placeText}>PO Box: 2211</Text>
                      </View>
                      <View style={styles.textContent}>
                        <Text style={styles.placeText}>Al Nameem,Jaddah</Text>
                      </View>
                      <View style={styles.textContent}>
                        <Text style={styles.placeText}>+966 00000</Text>
                      </View>
                    
                    </View>
                  </View>
                </View>

                <View style={styles.billingAddress}>
                  <Text style={styles.deliveryText}>
                    {appTexts.ADDRESSDETAILS.BillingAddress}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      toggleModal();
                    }}
                  >
                    <View style={styles.addnewButton}>
                      <Text style={styles.buttonText}>
                        {appTexts.ADDRESSDETAILS.AddNew}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>

                <View style={styles.billingnameandIcon}>
                  <Text style={styles.nameText}>John Mathew</Text>
                  <View style={styles.iconList}>
                  <TouchableOpacity onPress={editAdClicked}>
                    <View style={{paddingHorizontal: "3%"  }}>
                    <Image
                  source={require("../../assets/images/profileItem/edit.png")}
                  resizeMode="contain"
                  style={styles.arrowicon}
                />
                    </View>
                    </TouchableOpacity>
                    <Icon
                      style={{ fontSize: 16, color: "#ACACAC" }}
                      name="trash-o"
                    />
                  </View>
                  
                </View>
                <View style={styles.nameandIcon}>
                  <Text style={styles.placeText}>Al Bassam lll2</Text>
                </View>

                <View style={styles.userDetails}>
                  {/* <Text style={styles.placeText}>Al Naemia Street</Text>
                  <Text style={styles.placeText}>PO Box: 2211</Text>
                  <Text style={styles.placeText}>Al Nameem,Jaddah</Text>
                  <Text style={styles.placeText}>+966 123330</Text> */}
                   <View style={styles.textContent}>
                    <View style={{ paddingLeft: "0%" }}>
                    <View style={styles.textContent}>
                        <Text style={styles.placeText}>Al Naemia Street</Text>
                      </View>
                      <View style={styles.textContent}>
                        <Text style={styles.placeText}>PO Box: 2211</Text>
                      </View>
                      <View style={styles.textContent}>
                        <Text style={styles.placeText}>Al Nameem,Jaddah</Text>
                      </View>
                      <View style={styles.textContent}>
                        <Text style={styles.placeText}>+966 00000</Text>
                      </View>
                    
                    </View>
                  </View>
                </View>

                <View style={styles.billinguserDetails}>
                <View style={styles.textContent}>
                    <View style={{ paddingLeft: "0%" }}>
                    <View style={styles.textContent}>
                        <Text style={styles.placeText}>Al Naemia Street</Text>
                      </View>
                      <View style={styles.textContent}>
                        <Text style={styles.placeText}>PO Box: 2211</Text>
                      </View>
                      <View style={styles.textContent}>
                        <Text style={styles.placeText}>Al Nameem,Jaddah</Text>
                      </View>
                      <View style={styles.textContent}>
                        <Text style={styles.placeText}>+966 00000</Text>
                      </View>
                      <View style={styles.textContent}>
                        <Text style={styles.placeText}>Al Nameem,Jaddah</Text>
                      </View>
                      <View style={styles.textContent}>
                        <Text style={styles.placeText}>+966 00000</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
        {/* modalpopup starting */}
        <Modal isVisible={isModalVisible} style={styles.modalMainContent}>
          <KeyboardAwareScrollView scrollEnabled={true}>
            <View style={styles.modalmainView}>
              <TouchableOpacity
                style={styles.buttonwrapper}
                onPress={() => {
                  toggleModal();
                }}
              >
                <Text style={styles.closeButton}>X</Text>
              </TouchableOpacity>
              <Text style={styles.restText}>
                {appTexts.ADDRESSDETAILS.AddNewAddress}
              </Text>
              <View style={styles.resetPassword}>
                <View style={styles.pwdOne}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.Name}
                  </Text>
                  <View style={styles.cPassword}>
                    <TextInput
                      style={styles.numberEnter}
                      underlineColorAndroid="transparent"
                    />
                  </View>
                </View>
                <View style={{ paddingTop: "2%" }}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.Apartment}
                  </Text>
                  <View style={styles.cPassword}>
                    <TextInput
                      style={styles.numberEnter}
                      underlineColorAndroid="transparent"
                    />
                  </View>
                </View>
                <View style={{ paddingTop: "2%" }}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.Street}
                  </Text>
                  <View style={styles.cPassword}>
                    <TextInput
                      style={styles.numberEnter}
                      underlineColorAndroid="transparent"
                    />
                  </View>
                </View>
                <View style={{ paddingTop: "2%" }}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.POBox}
                  </Text>
                  <View style={styles.cPassword}>
                    <TextInput
                      style={styles.numberEnter}
                      underlineColorAndroid="transparent"
                    />
                  </View>
                </View>
                <View style={{ paddingTop: "2%" }}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.Landmark}
                  </Text>
                  <View style={styles.cPassword}>
                    <TextInput
                      style={styles.numberEnter}
                      underlineColorAndroid="transparent"
                    />
                  </View>
                </View>
                <View style={{ paddingTop: "2%" }}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.Region}
                  </Text>
                  <View style={styles.cdPassword}>
                    <DropDownPicker
                      items={[
                        { label: "Thrissur", value: "Thrissur" },
                        { label: "Palakkad", value: "Palakkad" },
                      ]}
                      //placeholder="Select your city"
                      placeholder={[]}
                      labelStyle={{
                        fontSize: 13,
                        fontFamily: I18nManager.isRTL
                          ? globals.FONTS.notokufiArabic
                          : globals.FONTS.poppinsRegular,
                        textAlign: "left",
                        color: "#242424",
                      }}
                      containerStyle={{ height: 30 }}
                      style={{
                        backgroundColor: "white",
                        borderBottomColor: "#EEEEEE",
                        borderTopWidth: 0,
                        borderBottomWidth: 1,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                        borderLeftColor: "red",
                        borderTopLeftRadius: 0,
                        borderTopRightRadius: 0,
                        borderBottomLeftRadius: 0,
                        borderBottomRightRadius: 0,
                      }}
                      itemStyle={{
                        justifyContent: "flex-start",
                      }}
                      dropDownStyle={{ backgroundColor: "white" }}
                    />
                  </View>
                </View>

                <View style={styles.resendWrapper}>
                  <View style={styles.flagsectionWrapper}>
                    <Image
                      source={require("../../assets/images/Icons/flag.png")}
                      style={{ height: 20, width: 20 }}
                    />
                    <TextInput
                      editable={false}
                      style={{
                        //height: 40,
                        fontSize: 14,
                        alignItems: "center",
                        //fontFamily: "Montserrat-Regular",
                        //fontFamily:globals.FONTS.poppinsRegular,
                        color: "#7D7B7D",
                      }}
                      placeholder={"+966"}
                      placeholderTextColor="#7D7B7D"
                    />
                  </View>

                  <View style={styles.numbersectionWrapper}>
                    <TextInput
                      //editable={false}
                      style={styles.numberEnters}
                      placeholder={appTexts.OTP.Enter}
                      placeholder={appTexts.EDIT_PROFILE.PHONE}
                      placeholderTextColor="#242424"
                    />
                  </View>
                </View>

                <View style={styles.acceptSection}>
                  <CheckBox />
                  <Text style={styles.setasText}>
                    {appTexts.ADDRESSDETAILS.setas}
                  </Text>
                </View>
              </View>
              <View style={styles.buttonWrapper}>
                <View style={styles.cancelButton}>
                  <Text style={styles.cancelText}>
                    {appTexts.ADDRESSDETAILS.Cancel}
                  </Text>
                </View>
                <View style={styles.submitButton}>
                  <Text style={styles.saveText}>
                    {appTexts.ADDRESSDETAILS.Save}
                  </Text>
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </Modal>
        {/* modalpopup ending */}
      </View>
    </View>

    // </ScrollView>
  );
};

AddressView.propTypes = {
  toggleModal: PropTypes.func,
  isModalVisible: PropTypes.bool,
};

export default AddressView;
