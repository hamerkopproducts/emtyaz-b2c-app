import React, { useState, useEffect } from "react";
import { BackHandler } from "react-native";
import { connect } from "react-redux";
import SigninView from "./SigninView";
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-toast-message';
import appTexts from "../../lib/appTexts";

const SigninScreen = (props) => {
  //Initialising states
  
  const [number, setNumber] = useState('');
  const [butClick,setButClick] = useState(true);
  const [upButClick,setUpButClick] = useState(false);
  


  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    BackHandler.addEventListener(
      "hardwareBackPress",
      (backPressed = () => {
        BackHandler.exitApp();
        return true;
      })
    );
  };

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {};
  
  const loginButtonPress = () => {
    if (number.trim() === '') {
      Toast.show({
        type: 'error',
        position: 'top',
        text1:  appTexts.VALIDATION_MESSAGE.Type,
        text2:  appTexts.VALIDATION_MESSAGE.Mobile_error_msg,
        visibilityTime: 2000,
        autoHide: true,
        topOffset: 30,
        bottomOffset: 40
      });
    }
  };
  const signUpPress = () => {
    props.navigation.navigate('SignupScreen');
  };
  const inClick = () =>{
    setButClick(true)
  };

  const upClick = () =>{
    setUpButClick(true);
    setButClick(false);
    
    props.navigation.navigate('SignupScreen');
    //setTimeout(1000);
    setUpButClick(false);
    setButClick(true);
  };

  const signInButtonPress = () => {
    props.navigation.navigate('OTPScreen');
  };

  const backButtonpress = () => {
    props.navigation.navigate('ChooseLanguageScreen')
  };
 

  

  return (
    <SigninView
    loginButtonPress={loginButtonPress} 
    setNumber={setNumber} 
    backButtonpress={backButtonpress}
    number={number}
    signUpPress={signUpPress}
    signInButtonPress={signInButtonPress}
    inClick={inClick}
    setButClick={setButClick}
    butClick={butClick}
    upClick={upClick}
    setUpButClick={setUpButClick}
    upButClick={upButClick}
    
    
    />
    
     
  );
};

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      doLogin: LoginActions.doLogin,
    },
    dispatch
  );
};

const SignUpWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(SigninScreen);

SignUpWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default SignUpWithRedux;
