import React,{ useRef ,useState} from "react";
import PropTypes from "prop-types";
import {
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  
} from "react-native";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import { images, styles } from "./styles";
import CustomGreenLargeButton from "../../components/CustomGreenLargeButton";
import Modal from "react-native-modal";

const SigninView = (props) => {
  const numberInput = useRef(null);
  const { loginButtonPress,
    number,
    setNumber,
    signUpPress,
    signInButtonPress,
    backButtonpress,
    inClick,
    butClick,
    upClick,
    upButClick,
    
    isLoading,} = props;

    const signInPress = () => {
      signInPress(!signInPress);
    };

      
  return (
    <ScrollView style={styles.screenMain}>
      <View style={styles.screenMain}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor={globals.COLOR.screenBackground}
        />
        <View></View>
        <View style={styles.arrowWrapper}>
        <TouchableOpacity
            onPress={() => {
              backButtonpress();
            }}
          >
            <Image
              source={images.backIcon}
              resizeMode="contain"
              style={styles.arrowicon}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.logoWrapper}>
          <Image
            source={images.logoImage}
            resizeMode="contain"
            style={styles.logoImage}
          />
        </View>
        <View style={styles.signButtons}>
          <TouchableOpacity onPress={() => {inClick();}}>
          {butClick ?
          <View style={styles.clickBut}>
            <Text style={styles.clickText}>SIGN IN</Text>
         </View> : <View style={styles.buttonStyle}>
           <Text style={styles.buttonText}>SIGN IN</Text>
         </View>}
         </TouchableOpacity >      
         <TouchableOpacity onPress={() => {upClick(); }}>
           {upButClick ?  <View style={styles.clickBut}>
            <Text style={styles.clickText}>SIGN UP</Text>
         </View> :
         <View style={styles.buttonStyle}>
         <Text style={styles.buttonText}>SIGN UP</Text>
         </View> }
         </TouchableOpacity>
     
    
        </View>
        <View style={styles.contentWrapper}>
          <Text style={styles.contentHeader}>{appTexts.SIGNIN.Heading}</Text>
          <Text style={styles.contentDescription}>
            {appTexts.SIGNIN.Description}
          </Text>
        </View>

        <View style={styles.phoneScetion}>
          <View style={styles.firstSection}>
            <Image
              source={require("../../assets/images/Icons/flag.png")}
              style={{ height: 18, width: 18 }}
            />
            <View style={{flexDirection: 'row',marginLeft:2}}> 
            <TextInput
              editable={false}
              style={styles.disableText}
              placeholder={"+966"}
              placeholderTextColor="#282828"
            
            />
          </View>
          </View>
          <View style={styles.secondSection}>
            <TextInput
              style={styles.enableText}
              placeholder={appTexts.SIGNIN.PHONE}
              placeholderTextColor="#242424"
              keyboardType = "number-pad"
              autoFocus={true}
              // value={number}
              ref={numberInput}
              onChangeText={val => {
                setNumber(val);
                 }}

            />
          </View>
        </View>
        {/* </View> */}

        <View style={styles.buttonWrapper}>
        <TouchableOpacity onPress={signInButtonPress}
            // onPress={() => {
            //   loginButtonPress();
            // }}
          >
            <CustomGreenLargeButton buttonText={appTexts.SIGNIN.SIGNIN} />
          </TouchableOpacity>
        </View>

        <View style={styles.coGust}>
          <Text style={styles.goText}>{appTexts.SIGNIN.GUEST}</Text>
</View> 
         <View style={styles.signtextWrapper}>
          <View
            style={{
              width: "15%",
              borderBottomWidth: 1,
              borderBottomColor: globals.COLOR.lightGray,
              borderRadius: 0.2,
              borderStyle: "dashed",
              marginRight: 10,
            }}
          />
          <Text style={styles.orText}>{appTexts.SIGNIN.ORWITH}</Text>
          <View
            style={{
              width: "15%",
              borderBottomWidth: 1,
              borderBottomColor: globals.COLOR.lightGray,
              borderRadius: 0.2,
              borderStyle: "dashed",
              marginLeft: 10,
            }}
          />
        </View> 
         <View style={styles.socialMedia}>
          <View style={styles.faceBook}>
            <Image
              source={images.facebookIcon}
              resizeMode="contain"
              style={styles.socialicon}
            />
          </View>
          <View style={styles.googlePlus}>
            <Image
              source={images.googleIcon}
              resizeMode="contain"
              style={styles.socialicon}
            />
          </View>
        </View>
        {/* <View style={styles.socialMedia}>
          <View style={styles.appleID}>
            <Image
              source={images.googleIcon}
              resizeMode="contain"
              style={styles.socialicon}
            />
          </View>
        </View>  */}
 
         {/* <View style={styles.SignupSection}>
          <Text style={styles.signText}>{appTexts.SIGNIN.New}</Text>
          <Text style={styles.gustText}>{appTexts.SIGNIN.SIGN}</Text>
        </View> */}
        

      </View>
    </ScrollView>
  );
};

SigninView.propTypes = {
  loginButtonPress: PropTypes.func,
  number: PropTypes.string,
  setNumber: PropTypes.func,
  isLoading:PropTypes.bool,
  signUpPress:PropTypes.func,
  signInButtonPress:PropTypes.func,
  backPress:PropTypes.func,
  inClick:PropTypes.func,
  
  
};

export default SigninView;
