import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
const images = {
   backIcon: require("../../assets/images/chooseLanguage/backarrow.png"),
  logoImage: require("../../assets/images/header/Emtyaz_logo_color.png"),
  facebookIcon: require("../../assets/images/signup/facebook.png"),
  googleIcon: require("../../assets/images/signup/google.png"),
};
const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      backgroundColor: globals.COLOR.screenBackground,
      height:"100%"
  },
  arrowWrapper: {
    paddingLeft: "5%",
    paddingTop: hp("2%"),
  },
  arrowicon: {
    width: 20,
    height: 20,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
  logoImage: {
    alignSelf: "center",
  },
  signButtons:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    paddingLeft:'6%',
    paddingRight:'6%',
    paddingTop:'15%',
  },
  buttonStyle: {
    backgroundColor: globals.COLOR.lightGray,
    width: 170,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
  },
  clickBut:{
    backgroundColor:globals.COLOR.themeGreen,
    width:170,
    height:48,
    justifyContent: "center",
    alignItems: "center",
  },
  
  buttonText: {
    color: globals.COLOR.white,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 15,
  },
  clickText:{
    color: globals.COLOR.white,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 15,
  },
  buttonTexts: {
    color: globals.COLOR.white,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 15,
  },
  logoWrapper: {
    paddingTop: hp("3%"),
  },
  contentWrapper:{
    justifyContent:'center',
    paddingTop:hp('15%'),
    paddingLeft:'7%'
  },
  contentHeader:{
    //textAlign:'center',
    paddingTop:hp('1%'),
    paddingBottom:hp('.1%'),
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    color:globals.COLOR.drakBlack,
    fontSize:18,
  },
  contentDescription:{
    //textAlign:'center',
    // paddingLeft:'6%',
    // paddingRight:'6%',
    lineHeight:24,
    fontSize:13,
    color:globals.COLOR.drakGray,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  buttonWrapper:{
    alignItems:'center',
    justifyContent:'center',
    paddingTop:hp('5%'),
    //paddingLeft:'7%'
  },

  buttonbottomWrapper:{
    flexDirection:'row',
    justifyContent:'center',
    paddingTop:hp('5%'),
    alignItems:'center'
  },
  DdntreceiveText:{
    fontSize:13,
    color:globals.COLOR.lightBlack,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  resendText:{
    fontSize:14,
    //textDecorationLine: 'underline',
    color:globals.COLOR.lightBlack,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
  },
  modalMainContent: {
    //justifyContent: "center",
    justifyContent:'flex-end',
    margin: 0
  },
  modalmainView: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius:15,
    borderTopLeftRadius:15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  formWrapper:{
    flexDirection:'row',
    paddingLeft:'7%',
    paddingRight:'7%'
  },
  disableSection:{
width:'20%',
//backgroundColor:'red',
flexDirection:'row',
alignItems:'center',
borderBottomColor:'red',
borderBottomWidth:1,
//marginTop:8
//justifyContent:'center'
  },
  enableSection:{
width:'74%',
marginLeft:5,
//backgroundColor:'blue'
  },
  fixedInput:{
    marginLeft:5,
    //marginTop:5
  },
  forminWrapper:{  flexDirection:'row',paddingLeft:'7%',
  paddingRight:'7%'},
  phoneScetion:{
    flexDirection:'row',
    paddingTop:hp('2%'),
    paddingLeft:'7.5%',
    paddingRight:'7.5%'
  },
  firstSection:{
    //flexBasis: "25%",
    width:'28%',
              //backgroundColor: "lightgray",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomColor: globals.COLOR.lightGray,
              borderBottomWidth: 1,
              //paddingRight:'12%',
  },
  disableText:{
                fontSize: 14,
                color:'#7D7D7D',
                width:"80%",

              //  paddingLeft:I18nManager.isRTL ?'12%':0,
                fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  secondSection:{
    width:'70%',
    //flexBasis: "72.5%",
              //backgroundColor: "gray",
              marginLeft: "2%",
  },
  enableText:{
    fontSize: 14,
    lineHeight: 9,
    alignItems: "center",
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    borderBottomColor: globals.COLOR.lightGray,
    borderBottomWidth: 1,
    color:'#242424',
    textAlign: I18nManager.isRTL ? "right" : "left",
  },
  signtextWrapper:{
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    paddingTop:hp('4%')
  },
  socialMedia:{
    flexDirection:'row',
    justifyContent:'space-around',
    alignItems:'center',
    paddingLeft:'23%',
    paddingRight:'23%',
    paddingTop:hp('3%'),
    paddingBottom:hp('3%'),

  },
  faceBook:{
    backgroundColor: globals.COLOR.fbButton,
    width:90,
    height: 43,
    justifyContent: "center",
    alignItems: "center",
  },
  googlePlus:{
    backgroundColor: globals.COLOR.googleButton,
    width: 90,
    height: 43,
    justifyContent: "center",
    alignItems: "center",
  },
  appleID:{
    backgroundColor: 'black',
    width: 210,
    height: 43,
    justifyContent: "center",
    alignItems: "center",
  },
  
  orText:{
    color:globals.COLOR.lightBlack,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  socialicon:{
    width: 60,
    height: 72,
  },
  SignupSection:{
    flexDirection:'row',
    justifyContent:'center',
    paddingTop:hp('3%'),

  },signText:{
    fontSize:12,
    color:globals.COLOR.lightBlack,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  gustText:{
    color:globals.COLOR.lightBlack,
    fontSize:11,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  coGust:{
    justifyContent:'center',
    flexDirection:'row',
    paddingTop:hp('4%'),
    paddingBottom:hp('3%')
  },
  goText:{
    fontSize:11,
    color:globals.COLOR.lightBlack,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsLight,
  }

  
});

export { images, styles };
