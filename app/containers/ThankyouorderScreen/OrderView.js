import React from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity, Image,ScrollView } from "react-native";
import globals from "../../lib/globals";
import { styles, images } from "./styles";
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";

const OrderView = (props) => {
  const { logoutButtonPress,viewOrderClicked,shoppingClicked } = props;

  return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        isLogoRequired={false}
        //headerTitle={'Privacy Policy'}
        isBackButtonRequired={false}
        // isRightButtonRequired={true}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      /><ScrollView>
      <View style={styles.contentWrapper}>
        <View style={styles.tickImage}>
          <Image source={images.tickImage} style={styles.tickImages} />
          <Text style={styles.thankText}>
            {appTexts.THANKUORDER_VIEW.Thanku}
          </Text>
          <Text style={styles.desText}>
            {appTexts.THANKUORDER_VIEW.Orderdecritpion}
          </Text>
          <Text style={styles.desText}>
            {appTexts.THANKUORDER_VIEW.Orderdecritpionemail}
          </Text>
        </View>
        <View style={styles.orderDeatils}>
          <View style={styles.orderStatus}>
            <Text style={styles.orderText}>
              {appTexts.THANKUORDER_VIEW.Orderstatus}
            </Text>
            <Text style={styles.orderText}>Pending</Text>
          </View>
          <View style={styles.orderStatus}>
            <Text style={styles.orderText}>
              {appTexts.THANKUORDER_VIEW.Orderid}
            </Text>
            <Text style={styles.orderText}>ORD12345</Text>
          </View>
          <View style={styles.orderStatus}>
            <Text style={styles.orderText}>
              {appTexts.THANKUORDER_VIEW.Deliverytime}
            </Text>
            <Text style={styles.orderText}>20 June 2020, spm - 5pm</Text>
          </View>
          <View style={styles.orderStatus}>
            <Text style={styles.orderText}>
              {appTexts.THANKUORDER_VIEW.Paymenttime}
            </Text>
            <Text style={styles.orderText}>Cash on Delivery</Text>
          </View>
        </View>
        <View style={styles.billDeatils}>
          <View style={styles.billdetails}>
            <View>
              <Text style={styles.billText}>
                {appTexts.THANKUORDER_VIEW.BIlldeatils}
              </Text>
            </View>

            <View
              style={styles.billwrapper}
            >
              <View style={styles.bookdetailscontentsright}>
                <Text style={styles.sarsText}>
                  {appTexts.THANKUORDER_VIEW.Subtotal}
                </Text>
                <Text style={styles.sarsText}>
                  {appTexts.THANKUORDER_VIEW.DeliveryCharge}
                </Text>
                <Text style={styles.sarsText}>
                  {appTexts.THANKUORDER_VIEW.CODFee}
                </Text>
              </View>

              <View style={{ alignItems: "flex-end" }}>
                <Text style={styles.sarText}>SAR 200.00</Text>
                <Text style={styles.sarText}>SAR 10.00</Text>
                <Text style={styles.sarText}>SAR 210.00</Text>
              </View>
            </View>
          </View>
        </View>
        <View
          style={
            
			styles.bottomBorder          
          }
       />
		
		<View style={styles.vatSection}>
					  <Text style={styles.sarText}>{appTexts.THANKUORDER_VIEW.TotalInclusivefVAT}</Text>
                <Text style={styles.sarText}>SAR 130.00</Text>
				</View>

				
				<View style={styles.logoutWrappers}>
            <TouchableOpacity onPress={viewOrderClicked}>
            <View style={styles.yesButton}>
            <Text style={styles.sendbuttonText}>{appTexts.THANKUORDER_VIEW.VIEWORDER}</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={shoppingClicked}>
            <View style={styles.noButton}>
            <Text style={styles.sendbuttonText}>{appTexts.THANKUORDER_VIEW.CONTINUESHOPPING}</Text>
          </View>
          </TouchableOpacity>
            </View>
			
				

      </View>
	  </ScrollView>
    </View>
  );
};

OrderView.propTypes = {
  logoutButtonPress: PropTypes.func,
};

export default OrderView;
