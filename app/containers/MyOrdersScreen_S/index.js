import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import MyOrdersView from './MyOrdersView';
import { bindActionCreators } from "redux";

import globals from "../../lib/globals";
import functions from "../../lib/functions"


const MyOrdersScreen = (props) => {

  const [isHelpModalVisible, setIsHelpModalVisible] = useState(false);
  const [isRatingModalVisible, setIsRatingModalVisible] = useState(false);

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  const toggleHelpModal = () => {
    setIsHelpModalVisible(!isHelpModalVisible)
  };
  const toggleRatingModal = () => {
    setIsRatingModalVisible(!isRatingModalVisible)
  };


  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };
  
  return (
    <MyOrdersView
    itemClick={()=>{}}
    navigation={props.navigation}
    toggleHelpModal={toggleHelpModal}
    isHelpModalVisible={isHelpModalVisible}
    toggleRatingModal={toggleRatingModal} 
    isRatingModalVisible={isRatingModalVisible}/>
  );

};


const mapStateToProps = (state, props) => {
  return {
    
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
   
  }, dispatch)
};

const myOrdersScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(MyOrdersScreen);

myOrdersScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default myOrdersScreenWithRedux;
