import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import QuotationViewView from './QuotationView';
import { bindActionCreators } from "redux";

import globals from "../../lib/globals";
import functions from "../../lib/functions"


const QuotationScreen = (props) => {
  const [isHelpModalVisible, setIsHelpModalVisible] = useState(false);

  

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  const toggleHelpModal = () => {
    setIsHelpModalVisible(!isHelpModalVisible)
  };
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };
  const onCartButtonPress = () => {
props.navigation.navigate('CartScreen')
  };
  const showDetails = () => {
    props.navigation.navigate('DetailsScreen')
  };

  const qutotationSummery = () =>{
    props.navigation.navigate('QuotationsummeryScreen')
  }
  const onBackButtonPress = () => {
    props.navigation.goBack()
   };
  
  return (
    <QuotationViewView onCartButtonPress={onCartButtonPress} 
    itemClick={showDetails}
    qutotationSummery={qutotationSummery}
    toggleHelpModal={toggleHelpModal}
    isHelpModalVisible={isHelpModalVisible}
    onBackButtonPress={onBackButtonPress}/>
  );

};


const mapStateToProps = (state, props) => {
  return {
    
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
   
  }, dispatch)
};

const homeScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(QuotationScreen);

homeScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default homeScreenWithRedux;
