import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      //flexDirection:'column',
      backgroundColor: globals.COLOR.screenBackground
  },
  screenContainer:{
    flex:1,
    marginTop:  globals.INTEGER.heightTen,
    flexDirection: 'column',
    backgroundColor: globals.COLOR.white,
    alignItems: 'center',
    justifyContent:'center'
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  contentWrapper:{
//backgroundColor:'red',
paddingLeft:'7%',
paddingRight:'7%',
paddingTop:'7%',
flex:1
  },
  deliveryText:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinssemiBold,
    fontSize:16,
  },
  addressWrapper:{
    flexDirection:'row',
    justifyContent:'space-between',
    paddingTop:'5%'
,
  },
  nameText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 13,
    color:'#383838'
  },
  userText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 13,
    color:'#686868',
    lineHeight:21,
    paddingLeft:'1%'
  },
  textContent:{flexDirection:'row'},
  billingnameandIcon:{
    flexDirection:'row',
    justifyContent:'space-between',alignItems:'center',paddingTop:'5%',paddingBottom:'3%'
  },
  userDetails:{
    paddingTop:'1%',
    borderBottomColor:'#CFCFCF',
    borderBottomWidth:1,
    paddingBottom:'6%',
   
      },
      auserDetails:{
        paddingTop:'4%',
       
        paddingBottom:'6%'
      },
      buttonWrapper:{
    //      position: 'absolute',
    // bottom:10,
    justifyContent:'center',
    alignItems: 'flex-end',
    flex: 1,
   
    //backgroundColor:'red'
   
      },
      modalMainContent: {
        //justifyContent: "center",
        //justifyContent: "flex-end",
       // margin: 0,
       //bottom:0,
       
      },
      modalmainView: {
        backgroundColor: "white",
        // width: ("90%"),
        //height:750,
        padding: "5%",
        borderRadius: 10,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        borderBottomRightRadius:10,
        borderBottomLeftRadius:10,
        borderColor: "rgba(0, 0, 0, 0.1)",
      },
      closeButton:{
        alignSelf:'flex-end',
        paddingRight:'3%',
        paddingLeft:'3%',
        // paddingTop:'2%',
        // paddingBottom:'1%',
        color:'#707070',
        fontSize:16
      },
      restText:{
       alignItems:'flex-start' ,
       fontFamily: I18nManager.isRTL
          ? globals.FONTS.notokufiArabic
          : globals.FONTS.poppinsMedium,
          fontSize:16,
       paddingRight:'3%',
        paddingLeft:'3%',
        //paddingTop:'1%',
        paddingBottom:'1.3%'
      },resetPassword:{
        paddingRight:'3%',
        paddingLeft:'3%',
       // paddingTop:'1%',
        paddingBottom:'1.5%',
        //backgroundColor:'red'
      },
      passwordText:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinsRegular,
        fontSize:13,
        color:'#8D8D8D'
        //paddingTop:'2%'
      },
      buttonWrapper:{
        flexDirection:'row',
        justifyContent:'space-between',
         paddingLeft:'3%',
         paddingRight:'3%',
         // backgroundColor:'red',
          //paddingTop:'5%',
          //paddingBottom:'10%'
          //marginBottom:10,
        // paddingBottom:'14%',
         //paddingTop:'1%'
      },
      cPassword:{
    marginTop:-12,
    flexDirection:'row',
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1,
    justifyContent: 'center',
        alignItems: 'center',
    
      },
      numberEnter:{
        fontSize: 14,
        color: "#242424",
        // marginLeft:-4,
       // lineHeight: 9,
        //alignItems: "center",
        textAlign: I18nManager.isRTL ? "right" : "left",
        fontFamily: I18nManager.isRTL
          ? globals.FONTS.notokufiArabic
          : globals.FONTS.poppinsRegular,
          flex:1
       
      },imageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center',
      },
      passwordText:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinsRegular,
        fontSize:13,
        color:'#8D8D8D'
        //paddingTop:'2%'
      },
      buttonWrapper:{
        flexDirection:'row',
        justifyContent:'space-between',
         paddingLeft:'3%',
         paddingRight:'3%',
         // backgroundColor:'red',
          //paddingTop:'5%',
          //paddingBottom:'10%'
          //marginBottom:10,
        // paddingBottom:'14%',
         //paddingTop:'1%'
      },
      cPassword:{
    marginTop:-12,
    flexDirection:'row',
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1,
    justifyContent: 'center',
        alignItems: 'center',
    
      },
      numberEnter:{
        fontSize: 14,
        color: "#242424",
        // marginLeft:-4,
       // lineHeight: 9,
        //alignItems: "center",
        textAlign: I18nManager.isRTL ? "right" : "left",
        fontFamily: I18nManager.isRTL
          ? globals.FONTS.notokufiArabic
          : globals.FONTS.poppinsRegular,
          flex:1
       
      },imageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center',
      },
      pwdOne:{
        paddingTop:'2%'
      },
      shadowContainerStyle: {   //<--- Style with elevation
        borderWidth: 0,
        shadowOffset: {
          width: 1,
          height: 1,
        },
        shadowOpacity: 0.2,
        elevation: Platform.OS === 'ios' ? 1: 3,
        backgroundColor:  '#ffffff',
        paddingLeft:'5%',
        paddingRight:'5%'
      },
      resendWrapper: {
        flexDirection: "row",
        justifyContent: "center",
        paddingLeft: "3%",
        paddingRight: "3%",
        paddingTop:'4%',
        paddingBottom:'4%'
        //  paddingTop: hp("3%"),
        //  paddingBottom: hp("2%"),
        //padding:'10%'
      },
      flagsectionWrapper: {
        //flexBasis: "30%",
        width:'30%',
        //backgroundColor: "pink",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        borderBottomColor: "#EEEEEE",
        borderBottomWidth: 1,
      },
      numbersectionWrapper: {
        //flexBasis: "70%",
        width:'75%',
        //backgroundColor: "pink",
        marginLeft: "2%",
      },
      numberEnters: {
        fontSize: 14,
        color: "#242424",
        lineHeight: 9,
        alignItems: "center",
        fontFamily: I18nManager.isRTL
          ? globals.FONTS.notokufiArabic
          : globals.FONTS.poppinsRegular,
        borderBottomColor: "#EEEEEE",
        borderBottomWidth: 1,
        textAlign: I18nManager.isRTL ? "right" : "left",
      },
      acceptSection:{
        flexDirection:'row',
        alignItems:'center',
        // paddingLeft:'2%',
        // paddingTop:hp('1.3%')
      
      },
      setasText:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinsRegular,
        fontSize:12,
        color:'#242424',
        paddingLeft:'2%',
        paddingRight:'2%'
      },
      cancelButton: {
        backgroundColor: '#FBFBFB',
        borderColor:'#CFCFCF',
    borderWidth:1,
        width: 134,
        height: 45,
        justifyContent: "center",
        alignItems: "center",},submitButton: {
          backgroundColor: globals.COLOR.themeGreen,
          width: 134,
          height: 45,
          justifyContent: "center",
          alignItems: "center",},
          saveText:{
            fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
            fontSize: 13,
            color:'white'
          },cancelText:{
            fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
            fontSize: 13,
            color:'#242424'
          },
});

export { images, styles };
