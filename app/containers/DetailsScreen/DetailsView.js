import React,{useState,useRef} from 'react';
import { View, ScrollView, Text, Image, TouchableOpacity,TouchableHighlight, FlatList } from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";
import PropTypes from 'prop-types';
import Header from "../../components/Header";
import ImageItem from "./ImageItem";
import BannerModal from "../../components/BannerModal";
import ItemCard from "./ItemCard";
import DividerLine from "../../components/DividerLine";
import { SliderBox } from 'react-native-image-slider-box';
import appTexts from "../../lib/appTexts";
import ProductCard from "../../components/ProductCard";
import HeadingWithRightLink from '../../components/HeadingWithRightLink';
const DetailsView = (props) => {
	const {
		itemClick,
		onCartButtonPress,
		onBackButtonPress,
		isBannerModalVisible,
		isBanModalVisible,
		closeBannerModal,
		closeBanModal,

		openBannerModal,
		opBannerModal,
		showDescription,
		openHideDescription,
		showSpecification,
		openHideSpecification,
	} = props;
	const [scrollViewWidth, setScrollViewWidth] = useState(0);
	const [currentXOffset, setCurrentXOffset] = useState(0);
	let images = [
		"https://source.unsplash.com/1024x768/?nature",
		"https://source.unsplash.com/1024x768/?water",
		"https://source.unsplash.com/1024x768/?tree", // Network image
	];
	let listData = [
		{
			id: 1,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_01.png')

		},
		{
			id: 2,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_02.png')

		},
		{
			id: 3,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_03.png')

		},
		{
			id: 4,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_04.png')

		},
		{
			id: 5,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_05.png')

		}]
		const inputEl = useRef(null);
	// function renderItem({ item, index }) {
	// 	return <ProductCard item={item} itemClick={itemClick} />;
	// }

	//const renderItem1 = ({ item, index }) => <PromotionalProductCard item={item} itemClick={itemClick}/>;
	
	const handleScroll = (event) => {
		console.log('currentXOffset =', event.nativeEvent.contentOffset.x);
		let newXOffset = event.nativeEvent.contentOffset.x
		setCurrentXOffset(newXOffset);
	}

	const leftArrow = () => {
		let eachItemOffset = scrollViewWidth / 6; // Divide by 10 because I have 10 <View> items
		let _currentXOffset = currentXOffset - eachItemOffset;
		inputEl.current.scrollTo({ x: _currentXOffset, y: 0, animated: true })
	}

	const rightArrow = () => {
		let eachItemOffset = scrollViewWidth / 6; // Divide by 10 because I have 10 <View> items 
		let _currentXOffset = currentXOffset + eachItemOffset;
		inputEl.current.scrollTo({ x: _currentXOffset, y: 0, animated: true })
	}
	// const renderItem1 = ({ item, index }) => <ProductCard item={item} itemClick={itemClick} />;
	return (

		<View style={styles.screenMain}>
			<Header
				navigation={props.navigation}
				isLogoRequired={true}
				isBackButtonRequired={true}
				onBackButtonPress={onBackButtonPress}
				isRightButtonRequired={true}
				onCartButtonPress={onCartButtonPress}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>

			<ScrollView style={styles.screenContainerScrollView} showsVerticalScrollIndicator={false}>
				<View style={styles.imageScroller}>
					<SliderBox images={images} sliderBoxHeight={200} dotColor={'#3fb851'} inactiveDotColor={'#96D8AA'} ImageComponentStyle={{ borderRadius: 2, width: 200, marginTop: 20, marginBottom: 40 }} />
				</View>
				<View style={styles.screenDesignContainer}>
					{/*<BannerImage bannerImage={require('../../assets/images/temp/banner.png')} />*/}
					<View style={styles.screenContainerWithMargin}>
						<View style={styles.categoryItemRow}>
							<ImageItem itemImage={{ uri: 'https://source.unsplash.com/1024x768/?nature' }} />
							<ImageItem itemImage={{ uri: 'https://source.unsplash.com/1024x768/?water' }} />
							<ImageItem itemImage={{ uri: 'https://source.unsplash.com/1024x768/?tree' }} />
						</View>
					</View>
					<View style={styles.promotionalContainer}>
						<ItemCard itemImage={require('../../assets/images/home/product_01.png')} />
					</View>
					<View style={styles.deliveryDetailsContainer}>
						<View style={{flexDirection: 'row'}}>
						<Text style={styles.itemNameText}>{appTexts.DETAILSSCREEN.Location}<Text style={styles.itemNameText1}>{'Jedda, Al,Nameem'}</Text></Text>
						</View>
						<View style={{flexDirection: 'row'}}>
						<Text style={styles.itemNameText}>{appTexts.DETAILSSCREEN.Charge}<Text style={styles.itemNameText1}>{'SAR 5'}</Text></Text>
						</View>
						<View style={{flexDirection: 'row'}}>
						<Text style={styles.itemNameText}>{appTexts.DETAILSSCREEN.Time}<Text style={styles.itemNameText1}>{'Today25 August, 5pm-7pm'}</Text></Text>
						</View>
					</View>
					<DividerLine />
					<View style={styles.descriptionContainer}>
						<View style={styles.descriptionView}>
							<View style={{flexDirection: 'row'}}>
							<Text style={styles.itemNameText2}>{appTexts.DETAILSSCREEN.Specification}</Text>
							</View>
							<TouchableOpacity style={styles.arrowContainer} onPress={() => { openHideDescription() }}>
								<Image source={showDescription ? require("../../assets/images/home/down-arrown.png")
									: require("../../assets/images/home/up-arrown.png")} />
							</TouchableOpacity>
						</View>
						{showDescription &&
							<Text style={styles.itemNameText3}>{'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'}</Text>}
					</View>
					<DividerLine />
					<View style={styles.descriptionContainer}>
						<View style={styles.descriptionView}>
							<Text style={styles.itemNameText2}>{appTexts.DETAILSSCREEN.Review}</Text>
							<TouchableOpacity style={styles.arrowContainer} onPress={() => { openHideSpecification() }}>
								<Image source={showSpecification ? require("../../assets/images/home/down-arrown.png")
									: require("../../assets/images/home/up-arrown.png")} />
							</TouchableOpacity>
						</View>
						{showSpecification &&
							<Text style={styles.itemNameText3}>{'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'}</Text>}
					</View>
					<DividerLine />
					<View style={styles.similarProductContainer}>
						<View style={styles.similarProductView}>
							<View style={styles.headCont}>
							<HeadingWithRightLink nameLabel={appTexts.DETAILSSCREEN.similar} rightLinkText={appTexts.DETAILSSCREEN.see}/>
							</View>
							{/* <Text style={styles.itemNameText4}>
								{appTexts.MOREITEM.SimilarProducts}
								
							</Text> */}
							
							{/* <View style={styles.similarProductList}>
								{listData.length === 0 ?
									<View style={styles.noDataContainer}>
										<Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text>
									</View> : */}
									<View style={styles.listContainer}>
						
						<View style={styles.scrollableView}>
							<TouchableHighlight
								style={styles.leftArrowStyle}
								onPress={() => { leftArrow() }}>
								<Image style={styles.itemImage} source={require('../../assets/images/products/arrow-dropleft.png')} />
							</TouchableHighlight>
											<ScrollView
												style={styles.scrollViewStyle}
												contentContainerStyle={{alignItems: 'center' }}
												horizontal={true}
												pagingEnabled={true}
												ref={inputEl}
												showsHorizontalScrollIndicator={false}
												onContentSizeChange={(w, h) => setScrollViewWidth(w)}
												scrollEventThrottle={16}
												//scrollEnabled={false} // remove if you want user to swipe
												onScroll={(event)=>{handleScroll(event)}}
											>
	 <View style={styles.cardView}> 
							<ProductCard item={listData[1]} itemClick={itemClick}/>
							<ProductCard item={listData[2]} itemClick={itemClick}/>
							<ProductCard item={listData[3]} itemClick={itemClick}/>
							<ProductCard item={listData[4]} itemClick={itemClick}/>
							</View> 
						
												
											</ScrollView>
								
						<TouchableHighlight
							style={styles.rightArrowStyle}
							onPress={()=>{rightArrow()}}>
							<Image style={styles.itemImage} source={require('../../assets/images/products/arrow-dropright.png')} />
						</TouchableHighlight>
						
					</View>
					{/* </View> 
							 <FlatList 
										style={styles.flatListStyle}
								 	data={listData}
									 	extraData={listData}
									 	keyExtractor={(item, index) => index.toString()} */}
									 	{/* onEndReachedThreshold={0.5}
										onEndReached={({ distanceFromEnd }) => {
											if (listData.length >= (currentPage * pageLimit)) {
												loadMoreItems();
											}
										}}
										onRefresh={() => { onRefresh() }}
										refreshing={isRefreshing}

								 	 showsVerticalScrollIndicator={false}
									 	renderItem={renderItem1} /> */}
							</View>
						</View>
					</View>
					
					<View style={styles.similarProductContainer}>
						<View style={styles.similarProductView}>
							<View style={styles.headCont}>
							<HeadingWithRightLink nameLabel={appTexts.DETAILSSCREEN.last} rightLinkText={appTexts.DETAILSSCREEN.see}/>
							</View>
							{/* <Text style={styles.itemNameText4}>
								{appTexts.MOREITEM.SimilarProducts}
								
							</Text> */}
							
							{/* <View style={styles.similarProductList}>
								{listData.length === 0 ?
									<View style={styles.noDataContainer}>
										<Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text>
									</View> : */}
									<View style={styles.listContainer}>
						
						<View style={styles.scrollableView}>
							<TouchableHighlight
								style={styles.leftArrowStyle}
								onPress={() => { leftArrow() }}>
								<Image style={styles.itemImage} source={require('../../assets/images/products/arrow-dropleft.png')} />
							</TouchableHighlight>
											<ScrollView
												style={styles.scrollViewStyle}
												contentContainerStyle={{alignItems: 'center' }}
												horizontal={true}
												pagingEnabled={true}
												ref={inputEl}
												showsHorizontalScrollIndicator={false}
												onContentSizeChange={(w, h) => setScrollViewWidth(w)}
												scrollEventThrottle={16}
												//scrollEnabled={false} // remove if you want user to swipe
												onScroll={(event)=>{handleScroll(event)}}
											>
	 <View style={styles.cardView}> 
							<ProductCard item={listData[1]} itemClick={itemClick}/>
							<ProductCard item={listData[2]} itemClick={itemClick}/>
							<ProductCard item={listData[3]} itemClick={itemClick}/>
							<ProductCard item={listData[4]} itemClick={itemClick}/>
							</View> 
						
												
											</ScrollView>
								
						<TouchableHighlight
							style={styles.rightArrowStyle}
							onPress={()=>{rightArrow()}}>
							<Image style={styles.itemImage} source={require('../../assets/images/products/arrow-dropright.png')} />
						</TouchableHighlight>
						
					</View>
					{/* </View> 
							 <FlatList 
										style={styles.flatListStyle}
								 	data={listData}
									 	extraData={listData}
									 	keyExtractor={(item, index) => index.toString()} */}
									 	{/* onEndReachedThreshold={0.5}
										onEndReached={({ distanceFromEnd }) => {
											if (listData.length >= (currentPage * pageLimit)) {
												loadMoreItems();
											}
										}}
										onRefresh={() => { onRefresh() }}
										refreshing={isRefreshing}

								 	 showsVerticalScrollIndicator={false}
									 	renderItem={renderItem1} /> */}
							</View>
						</View>
					</View>
				</View>
			</ScrollView>
			<View style={styles.bottomButtonContainer}>
				<View style={styles.horizontalView}>
					{/* <TouchableOpacity style={styles.shareTextView}>
						<Text style={styles.shareText}>{appTexts.DETAILSSCREEN.Add}</Text>
					</TouchableOpacity> */}
					<TouchableOpacity style={styles.shareTextView}  onPress={() => { openBannerModal() }}>
						<Text style={styles.shareText}>{appTexts.DETAILSSCREEN.Add}</Text>
					</TouchableOpacity>
					<View style={styles.verticalDivider} />
					<TouchableOpacity style={styles.shareTextView}>
						<Text style={styles.shareText}>{appTexts.DETAILSSCREEN.Share}</Text>
					</TouchableOpacity>
				</View>
				<TouchableOpacity style={styles.buttonView} onPress={() => { opBannerModal() }}>
					<Text style={styles.buttonText}>{appTexts.DETAILSSCREEN.Cart}</Text>
				</TouchableOpacity>
			</View>
			<BannerModal isBannerModalVisible={isBanModalVisible} closeModal={closeBanModal} bannerText={appTexts.DETAILSSCREEN.wish} />
			<BannerModal isBannerModalVisible={isBannerModalVisible} closeModal={closeBannerModal} bannerText={appTexts.DETAILSSCREEN.Cart} />
		</View>


	);
};

DetailsView.propTypes = {
	onCartButtonPress: PropTypes.func
};

export default DetailsView;
