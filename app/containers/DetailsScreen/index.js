import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import DetailsView from './DetailsView';
import { bindActionCreators } from "redux";

import globals from "../../lib/globals";
import functions from "../../lib/functions"


const DetailsScreen = (props) => {

  const [isBannerModalVisible, setIsBannerModalVisible] = useState(false);
  const [showDescription, setShowDescription] = useState(false);
  const [showSpecification, setShowSpecification] = useState(false);
  const [isBanModalVisible,setIsBanModalVisible] = useState(false);

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };
  const showDetails = () => {
    props.navigation.navigate('DetailsScreen')
  };
  const onCartButtonPress = () => {
props.navigation.navigate('CartScreen')
  };
  const onBackButtonPress = () => {
    props.navigation.goBack()
  };
  const closeBannerModal = () => {
    setIsBannerModalVisible(false)
  };
  const openBannerModal = () => {
    setIsBannerModalVisible(true)
  };
  const opBannerModal = () => {
    setIsBanModalVisible(true)
  };
  const closeBanModal = () => {
    setIsBanModalVisible(false)
  };
  const openHideDescription = () => {
    setShowDescription(!showDescription)
  };
  const openHideSpecification = () => {
    setShowSpecification(!showSpecification)
  };
  return (
    <DetailsView onCartButtonPress={onCartButtonPress} 
    itemClick={showDetails} 
    onBackButtonPress={onBackButtonPress} 
      isBannerModalVisible={isBannerModalVisible} 
      closeBannerModal={closeBannerModal} 
      openBannerModal={openBannerModal} 
      openHideDescription={openHideDescription} 
      showDescription={showDescription}
      openHideSpecification={openHideSpecification} 
      showSpecification={showSpecification}
      opBannerModal={opBannerModal}
      closeBanModal={closeBanModal}
      isBanModalVisible={isBanModalVisible}/>
  );

};


const mapStateToProps = (state, props) => {
  return {
    
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
   
  }, dispatch)
};

const detailsScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailsScreen);

detailsScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default detailsScreenWithRedux;
