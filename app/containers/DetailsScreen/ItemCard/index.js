import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity,TextInput} from 'react-native';
import { styles } from "./styles";
import DotedContainerItem from "./DotedContainerItem";
import MoreItem from "./MoreItem";
import Modal from 'react-native-modal';
import DividerLine from "../../../components/DividerLine";
import CustomRadioButton from '../../../components/CustomRadioButton';
import appTexts from "../../../lib/appTexts";
import QuantityModal from '../../../components/QuantityModal';
import PackModal from '../../../components/PackModal';

const ItemCard = (props) => {
  const {
      } = props;
      
      const [isPackModalVisible, setIsPackModalVisible] = useState(false);
      const [isQuantityModalVisible, setIsQuantityModalVisible] = useState(false);

	

 const toggleQuantityModal = () => {
     setIsQuantityModalVisible(false)
  };
  const openQuantityModal = () => {
    setIsQuantityModalVisible(true)
  };

      
  const togglePackModal = () => {
    setIsPackModalVisible(!isPackModalVisible);
  };
  const openPackModal = () => {
    setIsPackModalVisible(true)
  };
    
  return (
    <View style={styles.fullWidthRowContainer}>
    <View style={styles.rowContainer}>
      <View style={styles.firstContainer}>
        <View style={styles.labelContainer}>
          <View style={styles.labelView}>
            <View style={styles.textContainer}>
                <Text style={styles.itemNameText}>{'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder'}</Text>
            </View>
            <TouchableOpacity onPress={() => { openQuantityModal()}}>
              <View style={styles.sizeSelectView}>
                <Text style={styles.sizeText}>{'Small'}</Text>
                <Image style={styles.dropDownArrow} source={require('../../../assets/images/cartScreenIcons/downArrow.png')} />
              </View>
            </TouchableOpacity>
            <QuantityModal isQuantityModalVisible={isQuantityModalVisible}toggleQuantityModal={toggleQuantityModal} />
          </View>
        </View>
      </View>
      
     
      <View style={styles.secondContainer}>
        
        <DotedContainerItem/>
        
        <DotedContainerItem />
        <DotedContainerItem />
        {/* <TouchableOpacity onPress={() => { openPackModal()}}>
        <MoreItem/>
        </TouchableOpacity> */}
        <PackModal isPackModalVisible={isPackModalVisible}togglePackModal={togglePackModal} />
      </View>
      </View>
      <DividerLine />
    </View>
    
  );
};
ItemCard.propTypes = {
 
};

export default ItemCard;