import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import HomeView from './HomeView';
import { bindActionCreators } from "redux";

import globals from "../../lib/globals";
import functions from "../../lib/functions"


const HomeScreen = (props) => {

  

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };
  const onCartButtonPress = () => {
props.navigation.navigate('CartScreen')
  };
  const showDetails = () => {
    props.navigation.navigate('DetailsScreen')
  };

  const categoryClick = () => {
    props.navigation.navigate('ChoosecategoryScreen')
  };

  const productsClick = () => {
    props.navigation.navigate('ProductsScreen')
  };

  const mapClick = () => {
    props.navigation.navigate('MapScreen')
  };
  
  return (
    <HomeView onCartButtonPress={onCartButtonPress} 
    itemClick={showDetails}
    categoryClick={categoryClick}
    productsClick={productsClick}
    mapClick={mapClick}/>
  );

};


const mapStateToProps = (state, props) => {
  return {
    
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
   
  }, dispatch)
};

const homeScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);

homeScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default homeScreenWithRedux;
