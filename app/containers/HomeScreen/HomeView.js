import React,{useState,useRef} from 'react';
import { View, ScrollView, Text,FlatList,TouchableHighlight,Image } from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";
import PropTypes from 'prop-types';
import Header from "../../components/Header";
import ShopsByCategoryItem from "../../components/ShopsByCategoryItem";
import BannerImage from "../../components/BannerImage";
import HeadingWithRightLink from "../../components/HeadingWithRightLink";
import ProductCard from "../../components/ProductCard";
import DividerLine from "../../components/DividerLine";
import { SliderBox } from "react-native-image-slider-box";
import appTexts from "../../lib/appTexts";
import { TouchableOpacity } from 'react-native-gesture-handler';
import QuantityModal from '../../components/QuantityModal';
import Modal from 'react-native-modal';

const HomeView = (props) => {
	const {
		onCartButtonPress,
		itemClick,
		categoryClick,
		productsClick,
		mapClick,
		
	} = props;
	const [scrollViewWidth, setScrollViewWidth] = useState(0);
	const [currentXOffset, setCurrentXOffset] = useState(0);
	
	let images = [
		"https://source.unsplash.com/1024x768/?nature",
		"https://source.unsplash.com/1024x768/?water",
		"https://source.unsplash.com/1024x768/?tree", // Network image
		require('../../assets/images/temp/banner.png'),          // Local image
	];
	let listData = [
		{
			id: 1,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_01.png')

		},
		{
			id: 2,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_02.png')

		},
		{
			id: 3,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_03.png')

		},
		{
			id: 4,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_04.png')

		},
		{
			id: 5,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_05.png')

		}]

		const inputEl = useRef(null);
	// function renderItem({ item, index }) {
	// 	return <ProductCard item={item} itemClick={itemClick} />;
	// }

	//const renderItem1 = ({ item, index }) => <PromotionalProductCard item={item} itemClick={itemClick}/>;
	
	const handleScroll = (event) => {
		console.log('currentXOffset =', event.nativeEvent.contentOffset.x);
		let newXOffset = event.nativeEvent.contentOffset.x
		setCurrentXOffset(newXOffset);
	}

	const leftArrow = () => {
		let eachItemOffset = scrollViewWidth / 6; // Divide by 10 because I have 10 <View> items
		let _currentXOffset = currentXOffset - eachItemOffset;
		inputEl.current.scrollTo({ x: _currentXOffset, y: 0, animated: true })
	}

	const rightArrow = () => {
		let eachItemOffset = scrollViewWidth / 6; // Divide by 10 because I have 10 <View> items 
		let _currentXOffset = currentXOffset + eachItemOffset;
		inputEl.current.scrollTo({ x: _currentXOffset, y: 0, animated: true })
	}
	
	return (

		<View style={styles.screenMain}>
			<Header
				navigation={props.navigation}
				isLogoRequired={true}
				isLanguageButtonRequired={true}
				isRightButtonRequired={true}
				onCartButtonPress={onCartButtonPress}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>
			
			<ScrollView style={styles.screenContainerScrollView} showsVerticalScrollIndicator={false}>
				<View style={styles.screenDesignContainer}>
					<View style={styles.screenContainerWithMargin}>
						<HeadingWithRightLink locationnameLabel={'Jedda, Al Nameem'} leftIcon={require('../../assets/images/home/maps.png')} rightLinkText={appTexts.HOMETEXT.change} color={'green'} onItemClick={mapClick}/>
					</View>	
			
						<SliderBox images={images} sliderBoxHeight={100} ImageComponentStyle={{ borderRadius: 2, width: globals.SCREEN_SIZE.width - 40  }} />
						{/*<BannerImage bannerImage={require('../../assets/images/temp/banner.png')} />*/}
						<View style={styles.screenContainerWithMargin}>	
						<HeadingWithRightLink nameLabel={appTexts.HOMETEXT.shopcategory} 
						rightLinkText={appTexts.HOMETEXT.seeall} color={'yellow'} onItemClick={categoryClick}/>
						<View style={styles.categoryItemRow}>
							<ShopsByCategoryItem itemImage={require('../../assets/images/home/cat_01.png')} nameLabel={'Bul Savings'} />
							<ShopsByCategoryItem itemImage={require('../../assets/images/home/cat_02.png')} nameLabel={'Deals'} />
							<ShopsByCategoryItem itemImage={require('../../assets/images/home/cat_03.png')} nameLabel={'Fresh Food'} />
							<ShopsByCategoryItem itemImage={require('../../assets/images/home/cat_04.png')} nameLabel={'Food Cupboard'} />
							<ShopsByCategoryItem itemImage={require('../../assets/images/home/cat_05.png')} nameLabel={'Cleaning & Household'} />
							<ShopsByCategoryItem itemImage={require('../../assets/images/home/cat_06.png')} nameLabel={'Baby Products'} />
						</View>
						<BannerImage bannerImage={require('../../assets/images/temp/banner-sec.png')} />
						
						<HeadingWithRightLink nameLabel={appTexts.HOMETEXT.promotionalitems} 
						rightLinkText={appTexts.HOMETEXT.seeall} color={'yellow'} onItemClick={productsClick}/>
						
					
					</View>
					{/* <DividerLine />*/}
					{/* <View style={styles.promotionalContainer}>
						<ProductCard item={listData[0]} itemClick={itemClick}/>
						<ProductCard item={listData[1]} itemClick={itemClick}/>
						
					</View>  */}
				<View style={styles.listContainer}>
	
					<View style={styles.scrollableView}>
						<TouchableHighlight
							style={styles.leftArrowStyle}
							onPress={() => { leftArrow() }}>
							<Image style={styles.itemImage} source={require('../../assets/images/products/arrow-dropleft.png')} />
						</TouchableHighlight>
										<ScrollView
											style={styles.scrollViewStyle}
											contentContainerStyle={{alignItems: 'center' }}
											horizontal={true}
											pagingEnabled={true}
											ref={inputEl}
											showsHorizontalScrollIndicator={false}
											onContentSizeChange={(w, h) => setScrollViewWidth(w)}
											scrollEventThrottle={16}
											//scrollEnabled={false} // remove if you want user to swipe
											onScroll={(event)=>{handleScroll(event)}}
										>
 <View style={styles.cardView}> 
						<ProductCard item={listData[1]} itemClick={itemClick}/>
						<ProductCard item={listData[2]} itemClick={itemClick}/>
						<ProductCard item={listData[3]} itemClick={itemClick}/>
						<ProductCard item={listData[4]} itemClick={itemClick}/>
						</View> 
					
											
										</ScrollView>
							
					<TouchableHighlight
						style={styles.rightArrowStyle}
						onPress={()=>{rightArrow()}}>
						<Image style={styles.itemImage} source={require('../../assets/images/products/arrow-dropright.png')} />
					</TouchableHighlight>
					
				</View>
				</View> 
				<View style={styles.screenContainerWithmargin}>
						<BannerImage bannerImage={require('../../assets/images/temp/banner.png')} />
					</View>
				
				</View>
			</ScrollView>
		</View>
		


	);
};

HomeView.propTypes = {
	onCartButtonPress: PropTypes.func,
	categoryClick:PropTypes.func,
	productsClick:PropTypes.func,
	mapClick:PropTypes.func,
};

export default HomeView;
