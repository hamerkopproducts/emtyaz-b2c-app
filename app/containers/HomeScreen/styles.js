import { StyleSheet } from "react-native";
import globals from "../../lib/globals"

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      flexDirection:'column',
      backgroundColor: globals.COLOR.screenBackground
  },
  screenContainerScrollView:{
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height
  },
  screenDesignContainer:{
    width: globals.SCREEN_SIZE.width,
   // paddingBottom: globals.INTEGER.screenPaddingFromFooter
  },
  screenContainerWithMargin:{
    width: globals.INTEGER.screenWidthWithMargin,
    marginLeft: globals.INTEGER.leftRightMargin,
  
  },
  screenContainerWithmargin:{
    width: globals.INTEGER.screenWidthWithMargin,
    marginLeft: globals.INTEGER.leftRightMargin,
    marginVertical:"5%",
  
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  promotionalContainer:{
    marginBottom:20,
    flexDirection:'row',
    // flex:1,
    // alignItems: 'center',
    // justifyContent: 'center'
  },
  categoryItemRow:{
    width:'100%',
    flexDirection:'row',
    flexWrap:'wrap'
  },
  listContainer: {
    width: '100%',
   // height:'100%',
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  flatListStyle: {
    width: '100%'
  },
  noDataContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  noDataText: {
    textAlign: 'center',
    color: globals.COLOR.lightTextColor,
    fontSize: 12
  },
  scrollViewStyle:{
    width: '100%',
    height: '100%',
    position: 'absolute',
    //marginVertical:'2%'
    //left:'5%'
    marginBottom:'2%'
  },
  scrollableView:{ 
    width: '100%',
    //height:320,
    minHeight:320,
  },
  leftArrowStyle:{
    position: 'absolute',
    top:100,
    left: 10,
    width:40,
    height:40,
    zIndex:2,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius:20
  },
  rightArrowStyle: {
    position: 'absolute',
    top: 100,
    right: 10,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf:'center',
    zIndex: 2,
    borderRadius: 20
  },
  cardView:{
     paddingTop:'3%',
   //  paddingBottom:'0.5%',
   // marginBottom:'0.5%',
    flexDirection:'row',
   /// height:'150%'
  },

});

export { images, styles };
