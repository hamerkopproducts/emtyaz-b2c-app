import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      flexDirection:'column',
      backgroundColor: globals.COLOR.screenBackground
  },
  screenContainer:{
    flex:1,
    marginTop:  globals.INTEGER.heightTen,
    flexDirection: 'column',
    backgroundColor: globals.COLOR.white,
    alignItems: 'center',
    justifyContent:'center'
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  formWrapper:{
    flex:1,
   paddingTop:hp('2%')
  },
  contentDescription:{
    paddingLeft:'5%',
    paddingRight:'5%',
    color:'#1c1c1c',
    fontSize:13,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS. poppinsRegular,
  },
  bodyDescritpion:{
    paddingLeft:'5%',
    paddingRight:'5%',
    paddingTop:hp('1%'),
    color:'#707070',
    fontSize:11,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS. poppinsRegular,
  },
  renderHeadings:{
    flexDirection: "row",
    //padding: 10,
    justifyContent: "space-between",
    alignItems: "center",
    //backgroundColor: "white",
     borderBottomColor:'lightgray',
     borderBottomWidth:.5,
    //width: "90%",
    paddingTop:'2%',
    //paddingRight:'2%',
    //marginTop:'3%',
    paddingBottom:'2%',
    marginLeft:'3.3%',
    marginRight:'3.3%'
    // paddingLeft:'2%'
  },
  rendercontentText:{
    //fontFamily: globals.FONTS.avenirMedium,
    color:globals.COLOR.lightBlack,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS. poppinsRegular,
     width: '88%',fontSize:14,paddingTop:hp('1%'),paddingBottom:hp('1.1%'),paddingLeft:'4%'
  
  },
  renderContents:{
    //backgroundColor: "#e3f1f1",
            //padding: 10,
            // borderBottomColor:'red',
            // borderBottomWidth:1
            //fontStyle: "italic",
            //fontFamily: globals.FONTS.avenirMedium,
            //fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
            paddingTop:'4%',
            paddingRight:'4%',
            paddingBottom:'4%',
            paddingLeft:'4%',
            color:globals.COLOR.lightBlack,
            fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS. poppinsRegular,
            fontSize:15,
            lineHeight:22
  },
  listItem:{
    //fontFamily: globals.FONTS.avenirMedium,
    color:globals.COLOR.lightBlack,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS. poppinsRegular,
    fontSize:14,paddingTop:hp('1%'),paddingBottom:hp('1.1%'),paddingLeft:'5%',paddingRight:'5%'
  
  },
  listWrapper:{
    width: '72%',
    flexDirection: "row",
    //padding: 10,
    justifyContent: "space-between",
    alignItems: "center",
    //backgroundColor: "pink",
     borderBottomColor:'lightgray',
     borderBottomWidth:.5,
    //width: "90%",
    paddingTop:'2%',
    //paddingRight:'2%',
    //marginTop:'3%',
    paddingBottom:'2%',
    marginLeft:'25%',
    marginRight:'25%'
    // paddingLeft:'2%'
  },
});

export { images, styles };
