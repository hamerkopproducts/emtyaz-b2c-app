import React from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity,I18nManager } from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";
import { Accordion } from "native-base";
import Icon from "react-native-vector-icons/AntDesign";
//import Icon from 'react-native-vector-icons/FontAwesome'; 
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
const dataArray = [
  {
    title: appTexts.CATEGORY_CONTENT.Title,
    content: appTexts.CATEGORY_CONTENT.Content,
  },
  {
	title: appTexts.CATEGORY_CONTENT.Title,
    content: appTexts.CATEGORY_CONTENT.Content,
  },
  {
	title: appTexts.CATEGORY_CONTENT.Title,
    content: appTexts.CATEGORY_CONTENT.Content,
  },
  {
	title: appTexts.CATEGORY_CONTENT.Title,
    content: appTexts.CATEGORY_CONTENT.Content,
  },
];

const CategoryView = (props) => {
  const {
		homeScreenClick,
	} = props;
  const _renderHeader = (item, expanded) => {
    return (
      <View style={styles.renderHeadings}>
		   <Icon style={{ fontSize: 20, color: "#ACACAC" }} name="infocirlceo" />
        <Text style={styles.rendercontentText}> {item.title}</Text>
        {expanded ? (
          <Icon style={{ fontSize: 15, color: "#ACACAC" }} name="minus" />
        ) : (
          <Icon style={{ fontSize: 15, color: "#ACACAC" }} name="plus" />
        )}
      </View>
    );
  };
  const _renderContent = (item,expanded) => {
	// return <Text style={styles.renderContents}>{item.content}</Text>;
	return (
		// <View style={styles.listWrapper}>
		// 	 <Icon style={{ fontSize: 20, color: "#ACACAC" }} name="linechart" />
		//   <Text style={styles.listItem}> {item.content}</Text>
		//   {expanded ? (
		// 	<Icon style={{ fontSize: 15, color: "#ACACAC" }} name="right" />
		//   ) : (
		// 	<Icon style={{ fontSize: 15, color: "#ACACAC" }} name="right" />
		//   )}
		// </View>

		<View style={styles.listWrapper}>
			<View style={{flexDirection:'row',alignItems:'center'}}>
			<Icon style={{ fontSize: 20, color: "#ACACAC" }} name="linechart" />
		    <Text style={styles.listItem}> {item.content}</Text>
			</View>
			<Icon style={{ fontSize: 12, color: "#ACACAC" , transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]}} name="right" />
		</View>
	  );
  };

  return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        headerTitle={appTexts.PROFILEITEM.Choosecat}
				isBackButtonRequired={true} onBackButtonPress={homeScreenClick}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
          
        }}
      />
      <View style={styles.formWrapper}>
        {/* <View style={{ paddingTop: hp("1%") }}>
          <Text style={styles.contentDescription}>
            Ut porta diam et ornare tempus?
          </Text>
          <Text style={styles.bodyDescritpion}>
            Many desktop publishingpackages and web page editors now use Lorem
            Ipsum as their default modeltext, and a search for 'lorem ipsum'
            will uncovermany web sites still intheir infancy.
          </Text>
        </View> */}

        <Accordion
          dataArray={dataArray}
          animation={true}
          expanded={true}
          renderHeader={_renderHeader}
          renderContent={_renderContent}
          style={{ borderWidth: 0 }}
        />
      </View>
    </View>
  );
};

CategoryView.propTypes = {
  logoutButtonPress: PropTypes.func,
  homeScreenClick: PropTypes.func,
  
};

export default CategoryView;
