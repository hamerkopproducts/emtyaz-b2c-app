import React, { useState, useEffect } from "react";
import { BackHandler } from "react-native";
import { connect } from "react-redux";
import OTPView from "./OTPView";
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";

const OTPScreen = (props) => {
  //Initialising states
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [otp1, setOtp1] = useState('');
  const [otp2, setOtp2] = useState('');
  const [otp3, setOtp3] = useState('');
  const [otp4, setOtp4] = useState('');


  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    BackHandler.addEventListener(
      "hardwareBackPress",
      (backPressed = () => {
        BackHandler.exitApp();
        return true;
      })
    );
  };

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {};

  const loginButtonPress = () => {
    props.doLogin({});
  };
  const toggleModal = () => {
    setIsModalVisible(!isModalVisible);
  };

  const backPress = () => {
    props.navigation.navigate('SigninScreen');
  };

  const homeScreenClick = () => {
    props.navigation.navigate('TabNavigator');
  };

  return (
    <OTPView
    otp1={otp1} 
    otp2={otp2}
    otp3={otp3}
    otp4={otp4}
    setOtp1={setOtp1}
    setOtp2={setOtp2}
    setOtp3={setOtp3}
    setOtp4={setOtp4}
      isModalVisible={isModalVisible}
      toggleModal={toggleModal}
      backPress={backPress}
      homeScreenClick={homeScreenClick}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      doLogin: LoginActions.doLogin,
    },
    dispatch
  );
};

const SignUpWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(OTPScreen);

SignUpWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default SignUpWithRedux;
