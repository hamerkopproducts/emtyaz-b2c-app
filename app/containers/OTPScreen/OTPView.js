import React , { useRef }  from "react";
import PropTypes from "prop-types";
import {
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  Image,
  TextInput,Keyboard,
  ScrollView,
} from "react-native";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import { images, styles } from "./styles";
import CustomGreenLargeButton from "../../components/CustomGreenLargeButton";
import Modal from "react-native-modal";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


const OTPView = (props) => {
  const otp1Input = useRef(null);
  const otp2Input = useRef(null);
  const otp3Input = useRef(null);
  const otp4Input = useRef(null);

  const { toggleModal, isModalVisible,  otp1,
    otp2,
      otp3,
      otp4,
      setOtp1,
      setOtp2,
      setOtp3,
      setOtp4,
    backPress,
    homeScreenClick,
   } = props;
  return (
 
    // <KeyboardAwareScrollView scrollEnabled={false}>
    <ScrollView>
    <View style={styles.screenMain}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={globals.COLOR.screenBackground}
      />

      <View style={styles.arrowWrapper}>
        <TouchableOpacity
          onPress={backPress}
            
          
        >
          <Image
            source={images.backIcon}
            resizeMode="contain"
            style={styles.arrowicon}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.logoWrapper}>
        <Image
          source={images.logoImage}
          resizeMode="contain"
          style={styles.logoImage}
        />
      </View>
      <View style={styles.contentWrapper}>
        <Text style={styles.contentHeader}>{appTexts.OTP.Heading}</Text>
        <Text style={styles.contentDescription}>
          {appTexts.OTP.Description}
        </Text>
      </View>

      <View style={styles.otpWrapper}>
        <TextInput
          maxLength={1}
          style={styles.textInput}
          placeholderTextColor="#282828"
          keyboardType="number-pad"
          autoFocus={true}
          ref={otp1Input}
          value={otp1}
          onChangeText={val => {
            setOtp1(val);
            if (val != '') {
              otp2Input.current.focus();
            }
          }}
        />
        <TextInput
          maxLength={1}
          style={styles.textInput}
          placeholderTextColor="#282828"
          keyboardType="number-pad"
          //autoFocus={true}
          ref={otp2Input}
          value={otp2}
          onChangeText={val => {
            setOtp2(val);
            if (val != '') {
              otp3Input.current.focus();
            }
          }}
          onKeyPress={({ nativeEvent }) => {
            if (nativeEvent.key === 'Backspace' && otp2 == '') {
              otp1Input.current.focus();
            }
          }}
          
        />
        <TextInput
          maxLength={1}
          style={styles.textInput}
          placeholderTextColor="#282828"
          keyboardType="number-pad"
          //autoFocus={true}
          ref={otp3Input}
          value={otp3}
          onChangeText={val => {
            setOtp3(val);
            if (val != '') {
              otp4Input.current.focus();
            }
          }}
          onKeyPress={({ nativeEvent }) => {
            if (nativeEvent.key === 'Backspace' && otp3 == '') {
              otp2Input.current.focus();
            }
          }}
        />
        <TextInput
          maxLength={1}
          style={styles.textInput}
          placeholderTextColor="#282828"
          keyboardType="number-pad"
         // autoFocus={true}
          ref={otp4Input}
          value={otp4}
          onChangeText={val => {
            setOtp4(val);
            if (val != '') {
              Keyboard.dismiss();
            }
          }}
          onKeyPress={({ nativeEvent }) => {
            if (nativeEvent.key === 'Backspace' && otp4 == '') {
              otp3Input.current.focus();
            }
          }}
        />
      </View>

      <View style={styles.buttonWrapper}>
        <TouchableOpacity onPress={homeScreenClick}>
          <CustomGreenLargeButton buttonText={appTexts.OTP.VERIFY} />
          {/* <CustomGreenLargeButton/> */}
        </TouchableOpacity>
      </View>
      <View style={styles.buttonbottomWrapper}>
        <Text style={styles.DdntreceiveText}>{appTexts.OTP.Dontreceive}</Text>
        <TouchableOpacity
          onPress={() => {
            toggleModal();
          }}
        >
          <Text style={styles.resendText}>{appTexts.OTP.Resend}</Text>
        </TouchableOpacity>
      </View>

      {/* modalpopup starting */}
      <Modal isVisible={isModalVisible} style={styles.modalMainContent}
       onSwipeComplete={() => { toggleModal();} }
       swipeDirection={["left", "right", "up", "down"]}>
        <View style={styles.modalmainView}>
          <View style={styles.resendWrapper}>
            <View style={styles.flagsectionWrapper}>
              <Image
                source={require("../../assets/images/Icons/flag.png")}
                style={{ height: 20, width: 20 }}
              />
              <TextInput
                editable={false}
                style={{
                  //height: 40,
                  fontSize: 14,
                  alignItems: "center",
                  //fontFamily: "Montserrat-Regular",
                  //fontFamily:globals.FONTS.poppinsRegular,
                  color: "#7D7B7D",
                }}
                placeholder={"+966"}
                placeholderTextColor="#7D7B7D"
              />
            </View>

            <View style={styles.numbersectionWrapper}>
              <TextInput
                //editable={false}
                style={styles.numberEnter}
                //placeholder={appTexts.OTP.Enter}
                placeholder={'9809146921'}
                placeholderTextColor="#7D7B7D"
              />
            </View>
            <View style={styles.buttonsectionWrapper}>
              <TouchableOpacity>  
                           <Text style={styles.resendbText}>{appTexts.OTP.RESEND}</Text>
              </TouchableOpacity>
 
            </View>
          </View>
        </View>
      </Modal>
      {/* modalpopup ending */}
    </View>
    </ScrollView>
      // </KeyboardAwareScrollView>
  );
};

OTPView.propTypes = {
  toggleModal: PropTypes.func,
  isModalVisible: PropTypes.bool,
  otp1: PropTypes.string,
  otp2: PropTypes.string,
  otp3: PropTypes.string,
  otp4: PropTypes.string,
  setOtp1: PropTypes.func,
  setOtp2: PropTypes.func,
  setOtp3: PropTypes.func,
  setOtp4: PropTypes.func,
  backPress:PropTypes.func,
  homeScreenClick:PropTypes.func,
};

export default OTPView;
