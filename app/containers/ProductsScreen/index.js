import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import ProductsView from './ProductsView';
import { bindActionCreators } from "redux";

import globals from "../../lib/globals";
import functions from "../../lib/functions";
import { BackHandler } from 'react-native';


const ProductsScreen = (props) => {

  const [columnCount, setColumnCount] = useState(1);
  const [isSearchModalVisible, setIsSearchModalVisible] = useState(false);
  const [isRecentSearchModalVisible, setIsRecentSearchModalVisible] = useState(false);
  const [isFilterModalVisible, setIsFilterModalVisible] = useState(false);
  const [isSortModalVisible, setIsSortModalVisible] = useState(false);
  const [serachText, setSerachText] = useState('');
  const [isCategoryModalVisible, setIsCategoryModalVisible] = useState(false);
  
  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };
  const onSearchPress = () => {
    //alert('hi')
    setIsRecentSearchModalVisible(true)
  };
  const onRecentSearchClose = () => {
    setIsRecentSearchModalVisible(false)
  }
  
  const changeListingStyle = () => {
    setColumnCount(columnCount === 1 ? 2 :1);
  };
  const onCartButtonPress = () => {
    props.navigation.navigate('CartScreen')
  };
  const showDetails = () => {
    props.navigation.navigate('DetailsScreen')
  };
  const showSelectedSearch = () => {
    setIsSearchModalVisible(true)
    setIsRecentSearchModalVisible(false)
  };
  const closeSearchModal = () => {
    setIsSearchModalVisible(false)
  };
  const closeFilterModal = () => {
    setIsFilterModalVisible(false)
  };
  const onFilterPress = () => {
    setIsFilterModalVisible(true)
    
  };
//  const onCategoryModalClick = () => {
//      setIsCategoryModalVisible(!isCategoryModalVisible)};

    // const openCategoryModal = () => {
    //   setIsCategoryModalVisible(true)};
   

    // const closeCategoryModal = () => {
    //  setIsCategoryModalVisible(false)
    // };



   
  const onSortPress = () => {
    setIsSortModalVisible(true)
  };
  const closeSortModal = () => {
    setIsSortModalVisible(false)
  };

  const backHomeClick = () => {
    props.navigation.navigate('HomeScreen')
  };
  
  
  return (
    <ProductsView columnCount={columnCount} 
    isSearchModalVisible={isSearchModalVisible} changeListingStyle={changeListingStyle} 
      onCartButtonPress={onCartButtonPress} onSearchPress={onSearchPress} serachText={serachText} 
    setSerachText={setSerachText} itemClick={showDetails} 
    isRecentSearchModalVisible={isRecentSearchModalVisible}
      onRecentSearchClose={onRecentSearchClose}
      showSelectedSearch={showSelectedSearch}
      closeSearchModal={closeSearchModal}
      isFilterModalVisible={isFilterModalVisible}
      closeFilterModal={closeFilterModal}
      isSortModalVisible={isSortModalVisible}
      closeSortModal={closeSortModal}
      onFilterPress={onFilterPress}
      
      //onCategoryModalClick={onCategoryModalClick}
      // isCategoryModalVisible={isCategoryModalVisible}
      // openCategoryModal={openCategoryModal}
      // closeCategoryModal={closeCategoryModal}
    
      onSortPress={onSortPress}
      backHomeClick={backHomeClick}
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
   
  }, dispatch)
};

const productsScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsScreen);

productsScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default productsScreenWithRedux;
