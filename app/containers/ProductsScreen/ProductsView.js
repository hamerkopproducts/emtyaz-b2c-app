import React, { useState, useRef } from 'react';
import { View, TouchableOpacity, Text, FlatList,Image, ScrollView, TouchableHighlight,Button,I18nManager} from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";
import PropTypes from 'prop-types';
import PromoProductCard from "../../components/PromoProductCard";
import ProductCard from "../../components/ProductCard";
import SearchModal from "../../components/SearchModal";
import RecentSearchModal from "../../components/RecentSearchModal";
import FilterModal from "../../components/FilterModal";
import SortModal from "../../components/SortModal";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Header from "../../components/Header";
import appTexts from "../../lib/appTexts";
import CategoryItem from "../../components/CategoryItem";
import Modal from "react-native-modal";

import CustomGreenLargeButton from "../../components/CustomGreenLargeButton";
import Category from '../../components/Category';
import CategoryModal from '../../components/CategoryModal';
import BrandsModal from '../../components/BrandsModal';


const ProductsView = (props) => {
	const {
		columnCount,
		changeListingStyle,
		onCartButtonPress,
		isSearchModalVisible,
		onSearchPress,
		serachText,
		itemClick,
		setSerachText,
		isRecentSearchModalVisible,
		onRecentSearchClose,
		closeSearchModal,
		showSelectedSearch,
		isFilterModalVisible,
		onFilterPress,
	  closeFilterModal,
	  //onCategoryModalClick,

	//   isCategoryModalVisible,
	//   closeCategoryModal,
	//   openCategoryModal,
		onSortPress,
		isSortModalVisible,
		closeSortModal,
		backHomeClick,
		
		
	} = props;

	const [scrollViewWidth, setScrollViewWidth] = useState(0);
	const [currentXOffset, setCurrentXOffset] = useState(0);
	const [isCategoryModalVisible, setIsCategoryModalVisible] = useState(false);
	const [isBrandModalVisible,setIsBrandModalVisible] = useState(false);
	
	const closeCategoryModal = () => {
		setIsCategoryModalVisible(false);
	}
	const closeBrandModal = () =>{
		setIsBrandModalVisible(false);
	}

	let listData = [
		{
			id: 1,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_01.png')

		},
		{
			id: 2,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_02.png')

		},
		{
			id: 3,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_03.png')

		},
		{
			id: 4,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_04.png')

		},
		{
			id: 5,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: false,
			image: require('../../assets/images/home/product_05.png')

		}]
	const inputEl = useRef(null);
	const renderItem = ({ item, index }) => <ProductCard item={item} itemClick={itemClick}/>;

	const renderItem1 = ({ item, index }) => <PromoProductCard item={item} itemClick={itemClick}/>;
	
	const handleScroll = (event) => {
		console.log('currentXOffset =', event.nativeEvent.contentOffset.x);
		let newXOffset = event.nativeEvent.contentOffset.x
		setCurrentXOffset(newXOffset);
	}

	const leftArrow = () => {
		let eachItemOffset = scrollViewWidth / 6; // Divide by 10 because I have 10 <View> items
		let _currentXOffset = currentXOffset - eachItemOffset;
		inputEl.current.scrollTo({ x: _currentXOffset, y: 0, animated: true })
	}

	const rightArrow = () => {
		let eachItemOffset = scrollViewWidth / 6; // Divide by 10 because I have 10 <View> items 
		let _currentXOffset = currentXOffset + eachItemOffset;
		inputEl.current.scrollTo({ x: _currentXOffset, y: 0, animated: true })
	}
		
	return (

		<View style={styles.screenMain}>
			<Header
				navigation={props.navigation}
				// isLogoRequired={true}
				// isLanguageButtonRequired={true}
				headerTitle={appTexts.STRING.products}
				isRightButtonRequired={true}
				isSearchButtonRequired={true}
				onCartButtonPress={onCartButtonPress}
				onSearchPress={onSearchPress}
				isBackButtonRequired={true} onBackButtonPress={backHomeClick}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>
			<View style={styles.screenContainer}>
				<View style={styles.container}>
					<View style={styles.leftHeadingContainer}>
						  <TouchableOpacity >
						<Text style={styles.nameLabel}>
						{(I18nManager.isRTL ? 'طلبات 450' :'450 Items')}
						</Text>
						</TouchableOpacity>   
			</View>

					<TouchableOpacity style={styles.filterContainer} onPress={() => { onSortPress(); }}>
						<Image style={styles.itemImage} source={require('../../assets/images/myOrders/filter.png')} />
						<Text style={styles.filterLabel}>{appTexts.MY_ORDERS.Sort}</Text>
					</TouchableOpacity> 
					{/* <TouchableOpacity style={styles.topDiscountContainer} onPress={() => { onSortPress() }}>
						<Text style={styles.filterLabel}>{'Top Discount'}</Text>
						<Image style={styles.itemImageDiscount} source={require('../../assets/images/products/discountC3.png')} />
					</TouchableOpacity> */}
					
					<TouchableOpacity style={styles.filterContainer} onPress={() => { onFilterPress(); }}>
						<Image style={styles.itemImage} source={require('../../assets/images/myOrders/filter.png')} />
						<Text style={styles.filterLabel}>{appTexts.MY_ORDERS.Filter}</Text>
					</TouchableOpacity>
 					
					
					{columnCount=== 2 ?	<TouchableOpacity style={styles.viewChangeContainer} onPress={()=>{changeListingStyle()}}>
						<View style={styles.greenLine}></View>
						<View style={styles.greenLine}></View>
						<View style={styles.greenLine}></View>
					</TouchableOpacity>:
					<TouchableOpacity style={styles.viewChangeContainer} onPress={() => { changeListingStyle() }}>
						<Image style={styles.itemImage} source={require('../../assets/images/products/gridC.png')} />
					</TouchableOpacity>}
					
				
				{/* <View style={styles.scrollableView}>
					<TouchableHighlight
						style={styles.leftArrowStyle}
						onPress={() => { leftArrow() }}>
						<Image style={styles.itemImage} source={require('../../assets/images/products/arrow-dropleft.png')} />
					</TouchableHighlight>
					<ScrollView
						style={styles.scrollViewStyle}
						contentContainerStyle={{alignItems: 'center' }}
						horizontal={true}
						pagingEnabled={true}
						ref={inputEl}
						showsHorizontalScrollIndicator={false}
						onContentSizeChange={(w, h) => setScrollViewWidth(w)}
						scrollEventThrottle={16}
						//scrollEnabled={false} // remove if you want user to swipe
						onScroll={(event)=>{handleScroll(event)}}
					>
						<CategoryItem itemImage={require('../../assets/images/home/cat_01.png')} badgeLabel={''} />
						<CategoryItem itemImage={require('../../assets/images/home/cat_02.png')} badgeLabel={'5'} />
						<CategoryItem itemImage={require('../../assets/images/home/cat_03.png')} badgeLabel={''} />
						<CategoryItem itemImage={require('../../assets/images/home/cat_04.png')} badgeLabel={''} />
						<CategoryItem itemImage={require('../../assets/images/home/cat_05.png')} badgeLabel={''} />
						<CategoryItem itemImage={require('../../assets/images/home/cat_06.png')} badgeLabel={''} />
					</ScrollView>
					<TouchableHighlight
						style={styles.rightArrowStyle}
						onPress={()=>{rightArrow()}}>
						<Image style={styles.itemImage} source={require('../../assets/images/products/arrow-dropright.png')} />
					</TouchableHighlight>
				</View> */}
				</View>
				<View style={styles.listContainer}>
					
					{listData.length === 0 ? <View style={styles.noDataContainer}><Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text></View> :
					
						<FlatList
							style={styles.flatListStyle}
							data={listData}
							extraData={listData}
							keyExtractor={(item, index) => index.toString()}
							/*onEndReachedThreshold={0.5}
							onEndReached={({ distanceFromEnd }) => {
								if (listData.length >= (currentPage * pageLimit)) {
									loadMoreItems();
								}
							}}*/
							//onRefresh={() => { onRefresh() }}
							//refreshing={isRefreshing}
							numColumns={columnCount}
							key={columnCount}
							showsVerticalScrollIndicator={false}
							renderItem={columnCount === 1 ? renderItem1 : renderItem} />} 
				</View>
				
				
			</View>
			<SearchModal isSearchModalVisible={isSearchModalVisible} closeModal={closeSearchModal} serachText={serachText} setSerachText={setSerachText}/>
			<RecentSearchModal isSearchModalVisible={isRecentSearchModalVisible} closeModal={onRecentSearchClose} serachText={serachText} setSerachText={setSerachText} showSelectedSearch={showSelectedSearch} />
			<FilterModal isFilterModalVisible={isFilterModalVisible} closeModal={closeFilterModal}
				setIscateogoryFilter={() => setIsCategoryModalVisible(true)}
				setIsbrandFilter = {() => setIsBrandModalVisible(true)}/>
			<SortModal isSortModalVisible={isSortModalVisible} closeModal={closeSortModal} />
			{isCategoryModalVisible &&
				<CategoryModal 
				isCategoryModalVisible={isCategoryModalVisible} toggleCategoryModal={closeCategoryModal} categoryHeading={'Category'} categorySubHeading={'Fresh Food'}/>
			}
			{isBrandModalVisible && 
				<BrandsModal
				isBrandModalVisible={isBrandModalVisible} toggleBrandModal={closeBrandModal} categoryHeading={'Brands'}/>}
			
		</View>


	);
};

ProductsView.propTypes = {
	onFilterPress: PropTypes.func,
	isFilterModalVisible: PropTypes.bool,	
	closeFilterModal:PropTypes.func,
	//onCategoryModalClick:PropTypes.func,
	backHomeClick:PropTypes.func,
	// openCategoryModal:PropTypes.func,
	// closeCategoryModal:PropTypes.func,
	// isCategoryModalVisible:PropTypes.bool,
	
	onSortPress:PropTypes.func,
};

export default ProductsView;
