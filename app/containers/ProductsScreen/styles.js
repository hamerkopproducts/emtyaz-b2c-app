
import { StyleSheet } from "react-native";
import globals from "../../lib/globals";
import { I18nManager } from "react-native";


let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginFifteen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      flexDirection:'column',
      backgroundColor: globals.COLOR.screenBackground
  },
  screenContainer:{
    flex:1,
  //  marginTop:  globals.INTEGER.heightTen,
    flexDirection: 'column',
    backgroundColor: globals.COLOR.white,
    alignItems: 'center'
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  container: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: 60,
    alignItems: 'center',
    flexDirection: 'row'
  },
  leftHeadingContainer: {
    // width: '35%', // change width @satheesh
    width: '50%',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  filterLabel:{
    marginLeft:5,
    marginRight: 5,
    color: globals.COLOR.greenTexeColor,
    // fontFamily: globals.FONTS.poppinsMedium,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
    fontSize: 11
  },
  modalMainContent:{
      
    margin:0,
    //justifyContent: "flex-end",
      
},
modalmainView:{
  //height: '85%',
   marginTop: 'auto',
   backgroundColor:'white',
   paddingLeft:'5%',
   paddingRight:'4%',
 
  
},
buttonWrapper:{
  justifyContent:'flex-end',
},
closeButton:{
  alignSelf:'flex-end',
  //paddingRight:'3%',
  paddingLeft:'1%',
  paddingTop:'2%',
  paddingBottom:'2%',
  color:'#707070',
  fontSize:16
},

categoryHeading:{
  flexDirection:'row',
  //justifyContent:'space-around',
  paddingBottom:'6%',
  flex:1,
  //backgroundColor:'red',
  
  
},
categoryTextHeading:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
  fontSize:16,
  color:'black',
},
lowerText:{
  color:'green',
  fontSize:10,
},
buttonCover:{
  flexDirection:'row',
 //justifyContent:'space-between',
 //paddingTop:'2%',
 marginLeft:'27%',
 
},
applyButton:{
  
  width:'40%',
  height:'90%',
  marginRight:'3%',
  backgroundColor:'green',
  justifyContent:'center',
  alignItems:'center',
  

},
applyButtonText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  textAlign:'center',
  color:'white',
  fontSize:10,
  
},
resetButton:{
  
  width:'38%',
  height:'90%',
  backgroundColor:'orange',
  justifyContent:'center',
  alignItems:'center',

},
resetButtonText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,    
  textAlign:'center',
  color:'white',
  fontSize:10,
},


bordersStyle:{
  borderWidth:0.5,
  borderColor:globals.COLOR.lightGray,
  

  
  
  
},
childComponent:{
  //backgroundColor:'blue',
  flex:0.5,
  paddingTop:'1%',
},
  topDiscountContainer: {
    height:30,
    width: '33%',
    marginRight:'1%',
    borderWidth:1,
    borderColor: '#37B34A',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  filterContainer: {
    borderWidth: 1,
    borderColor: '#37B34A',
    height: 30,
    width: '20%',
    marginRight: '1%',
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewChangeContainer: {
    borderWidth: 1,
    borderColor: '#37B34A',
    height: 30,
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  nameLabel: {
    top: 0,
    color: 'black',
    fontSize: 10,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,    
  },
  listContainer: {
    width: '100%',
    flex:1,
  //  marginVertical:"10%",
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  flatListStyle: {
    width: '100%',
  },
  noDataContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  
  },
  noDataText: {
    textAlign: 'center',
    color: globals.COLOR.lightTextColor,
    fontSize: 12
  },
  scrollViewStyle:{
    width: '90%',
    height: '100%',
    position: 'absolute',
    left:'5%'
  },
  scrollableView:{ 
    width: '100%',
    height:120,
  },
  leftArrowStyle:{
    position: 'absolute',
    top:45,
    left: 10,
    width:30,
    height:30,
    zIndex:2,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius:20
  },
  rightArrowStyle: {
    position: 'absolute',
    top: 45,
    right: 10,
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf:'center',
    zIndex: 2,
    borderRadius: 20
  },
  arrowStyle:{
    alignSelf: 'center',
    textAlign:'center',
    width: 40,
    height: 40,
    zIndex: 10,
    color: globals.COLOR.white,
    fontFamily: globals.FONTS.poppinsMedium,
    fontSize: 26
  },
  greenLine:{
    marginBottom:2,
    marginTop: 2,
    width:'50%',
    height:3,
    backgroundColor: globals.COLOR.greenTexeColor
  },
  itemImage:{
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
    width:17,
    height:17,
  },
  itemImageDiscount:{
    width:14,
    height:10,
  },
});

export { images, styles };
