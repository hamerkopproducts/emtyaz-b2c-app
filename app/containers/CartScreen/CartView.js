import React, { useState, useRef } from 'react';
import { View, TouchableOpacity, Text, FlatList, Image, ScrollView, TextInput} from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";
import CartCard from "../../components/CartCard";
import DividerLine from "../../components/DividerLine";
import Header from "../../components/Header";
import appTexts from "../../lib/appTexts";
import ListModal from "../../components/ListModal";
import SaveForLaterModal from '../../components/SaveForLaterModal';
import WishlistModal from '../../components/WishlistModal';
//import BannerModal from "../../components/BannerModal";
const CartView = (props) => {
	const {
		notesText,
		setNotesText,
		listModalIndex,
		closeListModal,
		onBackButtonPress,
		openSaveForLaterModal,
		isSaveForLaterModalVisible,
		toggleSaveForLaterModal,
		openWishlistModal,
		isWishlistModalVisible,
		toggleWishlistModal,
		isBannerModalVisible,
		closeBannerModal,
		checkoutPress,
		//openBannerModal
	} = props;

	
	

	let listData = [1,2,3,4,5,6]
	
	const renderItem = ({ item, index }) => <CartCard itemImage={require('../../assets/images/home/product_01.png')}/>;

	return (

		<View style={styles.screenMain}>
			<Header
				navigation={props.navigation}
				isBackButtonRequired={true}
				headerTitle={appTexts.CART.HeadingCart}
				onBackButtonPress={onBackButtonPress}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>
			<ScrollView style={styles.screenContainerScrollView} showsVerticalScrollIndicator={false}>
			<View style={styles.screenContainer}>
				<View style={styles.container}>
					<View style={styles.categoriesView}>
						<Text style={styles.itemsCount}>{appTexts.CART.Items}</Text>
						<View style={styles.selectedView}>
							 <TouchableOpacity style={styles.itemView} onPress={() => { openSaveForLaterModal();}}>
								<Text style={styles.symbolText}>{'+'}</Text>
								<Text style={styles.selectedItemText}>{appTexts.CART.Remove}</Text>
							</TouchableOpacity> 
							<TouchableOpacity style={styles.itemView} onPress={() => { openWishlistModal(); }}>
								<Text style={styles.symbolText}>{'+'}</Text>
								<Text style={styles.selectedItemText}>{appTexts.CART.AddFav}</Text>
							</TouchableOpacity>
						</View>
					</View>
					
				</View>
				<DividerLine />
				 <View style={styles.listContainer}>
					{listData.length === 0 ? <View style={styles.noDataContainer}><Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text></View> :
					
						<FlatList
							style={styles.flatListStyle}
							data={listData}
							extraData={listData}
							keyExtractor={(item, index) => index.toString()}
							showsVerticalScrollIndicator={false}
							renderItem={renderItem} />}
				</View>
				
				<View style={styles.promoText}>
					<Text style={styles.promoCodeText}>{appTexts.CART.Add}</Text>
				</View>
				<View style={styles.promoBox}>
					<View style={styles.enterText}>
						<Text style={styles.enterTextStyle}>{appTexts.CART.Enter}</Text>
					</View>
					<View style={styles.applyText}>
						<TouchableOpacity><Text style={styles.enterStyle}>{appTexts.CART.APPLY}</Text></TouchableOpacity>
					</View>
				</View>
				<View style={styles.billView}>
					<Text style={styles.promoCodeText}>{appTexts.CART.BillDetails}</Text>
				</View>
				<View style={styles.billBody}>
					<Text style={styles.bodyStyle}>{appTexts.CART.SubTotal}</Text>
					<Text style={styles.bodyStyle}>{appTexts.CART.subTotalAmount}</Text>
				</View>
				<View style={styles.billBody}>
					<Text style={styles.bodyStyle}>{appTexts.CART.DeliveryCharge}</Text>
					<Text style={styles.bodyStyle}>{appTexts.CART.DeliveryAmount}</Text>
				</View>
				<View style={styles.billBody}>
					<Text style={styles.promoCodeText}>{appTexts.CART.VAT}</Text>
					<Text style={styles.promoCodeText}>{appTexts.CART.TaxAmount}</Text>
				</View>
				<View style={styles.noteBox}>
					<Text style={styles.noteText}>{appTexts.CART.Addordernote}</Text>
				</View>
				<TouchableOpacity style={styles.requestQuoteView} onPress={checkoutPress}>
					<Text style={styles.buttonText}>{appTexts.CART.QuoteRequest}</Text>
				</TouchableOpacity>
				
			</View>
			<ListModal closeModal={closeListModal} listModalIndex={listModalIndex}/>
			<SaveForLaterModal isSaveForLaterModalVisible={isSaveForLaterModalVisible} toggleSaveForLaterModal={toggleSaveForLaterModal}/>
			<WishlistModal isWishlistModalVisible={isWishlistModalVisible} toggleWishlistModal={toggleWishlistModal}/>
			{/* <BannerModal isBannerModalVisible={isBannerModalVisible} closeModal={closeBannerModal} bannerText={'Moved To Save For Later'} /> */}
		</ScrollView>
		</View>


	);
};

CartView.propTypes = {
	
};

export default CartView;
