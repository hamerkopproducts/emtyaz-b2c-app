import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginFifteen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      flexDirection:'column',
      backgroundColor: globals.COLOR.screenBackground
  },
  screenContainer:{
    flex:1,
    marginTop:  globals.INTEGER.heightTen,
    flexDirection: 'column',
    backgroundColor: globals.COLOR.white,
    alignItems: 'center'
  },
  screenContainerScrollView:{
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  container: {
   // width: globals.INTEGER.screenWidthWithMargin ,
    width: '95%' ,
    height: 40,
    alignItems: 'center',
   
  },
  leftHeadingContainer: {
    width: '38%',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  filterLabel:{
    marginLeft:5,
    marginRight: 5,
    color: globals.COLOR.greenTexeColor,
    fontFamily: globals.FONTS.poppinsMedium,
    fontSize: 11
  },
  topDiscountContainer: {
    height:30,
    width: '30%',
    marginRight:'1%',
    borderWidth:1,
    borderColor:'green',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  filterContainer: {
    borderWidth: 1,
    borderColor: 'green',
    height: 30,
    width: '20%',
    marginRight: '1%',
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewChangeContainer: {
    borderWidth: 1,
    borderColor: 'green',
    height: 30,
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  nameLabel: {
    top: 0,
    color: 'black',
    fontSize: 10
  },
  listContainer: {
    marginTop:10,
    height:340,
    width: '100%',
    flex:1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  flatListStyle: {
    width: '100%'
  },
  noDataContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  noDataText: {
    textAlign: 'center',
    color: globals.COLOR.lightTextColor,
    fontSize: 12
  },
  scrollViewStyle:{
    width: '90%',
    height: '100%',
    position: 'absolute',
    left:'5%'
  },
  scrollableView:{ 
    width: '100%',
    height:280,
  },
  leftArrowStyle:{
    position: 'absolute',
    top:40,
    left: 10,
    width:30,
    height:30,
    zIndex:2,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#000',
    opacity:0.1,
    borderRadius:20
  },
  rightArrowStyle: {
    position: 'absolute',
    top: 40,
    right: 10,
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf:'center',
    zIndex: 2,
    backgroundColor: '#000',
    opacity: 0.1,
    borderRadius: 20
  },
  arrowStyle:{
    alignSelf: 'center',
    textAlign:'center',
    width: 40,
    height: 40,
    zIndex: 10,
    color: globals.COLOR.white,
    fontFamily: globals.FONTS.poppinsMedium,
    fontSize: 26
  },
  categoriesView: {
    marginTop: 0,
    height: 60,
    width: '100%',
    flexDirection:'row',

  },
  itemsCount: {
    marginTop:10,
    textAlign: "left",
    //marginLeft:'2%',
    color: globals.COLOR.black,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 13,
    marginBottom: 10
  },
  selectedView: {
    flexDirection: 'row',
    width: '100%',
    //justifyContent:'flex-end',

    //marginLeft:'2%',
  },
  selectedItemText: {
    marginTop:5,
    marginRight:5,
    textAlign: "left",
    color:'blue',
    //color: globals.COLOR.white,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 12
  },
  buttonText:{
    textAlign: "center",
    
    color: globals.COLOR.white,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 15
  },
  itemView: {
    flexDirection: 'row',
    //backgroundColor: globals.COLOR.greenTexeColor,
    marginLeft: '1%',
    height:40,
    alignItems:'center',
    justifyContent:'center',
  },
  locationLabelView:{
    marginTop:20,
    width:'90%',
    height:20
  },
  locationLabel:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
    fontSize: 12,
  },
  notesView:{
    marginTop: 10,
    width: '90%',
    height: 120,
    borderColor: globals.COLOR.lightGray,
    borderWidth:1
  },
  dateView:{
    marginTop: 10,
    marginBottom: 50,
    width: '90%',
    height: 40,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection:'row'
  },
  requestQuoteView: {
    marginTop: 10,
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: globals.COLOR.greenTexeColor,
  },
  notesInput:{
    padding:10,
    //textAlign: "left",
    color: globals.COLOR.black,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 14,
  },
  dateLabel:{
    textAlign: "left",
    color: globals.COLOR.black,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 14,
  },
  symbolText:{
    //marginLeft:3,
    color: 'blue',
    fontSize:14,
  },
  dateText:{
    color: globals.COLOR.black,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 12,
    padding:10
  },
  dateSelectView:{
    marginLeft:5,
    height:40,
    borderWidth:1,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row',
    borderColor: globals.COLOR.lightGray
  },
  calendarImage:{
    marginLeft:10,
    width:20,
height:20
  },
  promoText:{
  justifyContent:'flex-start',  
  alignItems:'flex-start',
  paddingBottom:'4%',
  paddingTop:'5%',
  width:'100%',
 // marginLeft:40,
 paddingLeft:'5%'
  },
  promoBox:{
    height:40,
    width:'90%',
    //width:365,
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    borderWidth:1,
    borderRadius:5,
    paddingLeft:'2%',
    paddingRight:'2%',
    borderColor: globals.COLOR.lightGray
    
  },
  enterTextStyle:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize:12,  
    color:globals.COLOR.lightGray,
  },
  enterStyle:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
    fontSize:12,  
    
  },
  billView:{
    justifyContent:'flex-start',  
    alignItems:'flex-start',
    paddingBottom:'4%',
    paddingTop:'5%',
    width:'100%',
    paddingLeft:'5%'
    //marginLeft:40, 
  },
  billBody:{
    flexDirection:'row',
    justifyContent:'space-between',
    //width:370,
    width:'90%',
    paddingBottom:'5%',
    //paddingLeft:'2%'
  },
  bodyStyle:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize:12,  
  },
  noteBox:{
    //width:370,
    width:'90%',
    height:100,
    borderWidth:1,
    borderColor:globals.COLOR.lightGray,
  },
  noteText:{
    paddingTop:'2%',
    paddingLeft:'2%',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    color:globals.COLOR.lightGray,
    fontSize:12, 
    textAlign:'left' 
  },
  promoCodeText:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
    fontSize:14,  

  }
});

export { images, styles };
