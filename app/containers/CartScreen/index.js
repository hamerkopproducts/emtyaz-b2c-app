import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import CartView from './CartView';
import { bindActionCreators } from "redux";

import globals from "../../lib/globals";
import functions from "../../lib/functions"


const CartScreen = (props) => {

  const [notesText, setNotesText] = useState('');
  const [listModalIndex, setListModalIndex] = useState(-1);
  const [isSaveForLaterModalVisible,setIsSaveForLaterModalVisible] = useState(false);
  const [isWishlistModalVisible,setIsWishlistModalVisible] = useState(false);
  //const [isBannerModalVisible, setIsBannerModalVisible] = useState(false);
  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };
  const changeListingStyle = () => {
    setColumnCount(columnCount === 1 ? 2 :1);
  };
  const onBackButtonPress = () => {
   props.navigation.goBack()
  };
  const closeListModal = () => {
    setListModalIndex(-1);
  };
  
  // const openWhishListModal = () => {
  //   setListModalIndex(1);
  // };
  const openSavedListModal = () => {
    setListModalIndex(0);
  };
   const openSaveForLaterModal = ()=>{
     setIsSaveForLaterModalVisible(true)
   };

  const toggleSaveForLaterModal = () => {
    setIsSaveForLaterModalVisible(false)
  };
  const openWishlistModal = () => {
    setIsWishlistModalVisible(true)
  };
  const toggleWishlistModal = () =>{
    setIsWishlistModalVisible(false)
  };
  
  const closeBannerModal = () => {
    setIsBannerModalVisible(false)
  };
  // const openBannerModal = () => {
  //   setIsBannerModalVisible(true)
  // };
  const checkoutPress = () => {
    props.navigation.navigate('QuotationsummeryScreen')
  };
  return (
    <CartView 
    checkoutPress={checkoutPress}
    onBackButtonPress={onBackButtonPress} 
    notesText={notesText} 
    setNotesText={setNotesText} 
    closeListModal={closeListModal} 
    listModalIndex={listModalIndex}
      //openWhishListModal={openWhishListModal}
      openSavedListModal={openSavedListModal}
      openSaveForLaterModal={openSaveForLaterModal}
      toggleSaveForLaterModal={toggleSaveForLaterModal}
      isSaveForLaterModalVisible={isSaveForLaterModalVisible}
      openWishlistModal={openWishlistModal}
      isWishlistModalVisible={isWishlistModalVisible}
      toggleWishlistModal={toggleWishlistModal}
      //isBannerModalVisible={isBannerModalVisible} closeBannerModal={closeBannerModal} openBannerModal={openBannerModal}
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
   
  }, dispatch)
};

const cartScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(CartScreen);

cartScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default cartScreenWithRedux;
