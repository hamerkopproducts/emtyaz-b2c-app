import React from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  Switch,
} from "react-native";
import globals from "../../lib/globals";
import { styles, images } from "./styles";
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";
import Modal from "react-native-modal";
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
const NotificationView = (props) => {
  const { onValueChange, istoggle } = props;

  return (
    <ScrollView>
      <View style={styles.screenMain}>
        <Header
          navigation={props.navigation}
          isLogoRequired={false}
          headerTitle={appTexts.NOTIFICATION.Notication}
          isBackButtonRequired={true}
          // isRightButtonRequired={true}
          customHeaderStyle={{
            height: globals.INTEGER.headerHeight,
            alignItems: "center",
            backgroundColor: globals.COLOR.headerColor,
          }}
        />
        <View style={styles.screenContainer}>
          <View
            style={
             styles.firstWrapper
            }
          >
            <Text style={styles.settingText}>{appTexts.NOTIFICATION.Message}</Text>
            <Switch
              trackColor={{ false: "#3fb851", true: "orange" }}
              thumbColor="white"
              ios_backgroundColor="gray"
              onValueChange={(value) => {(onValueChange)}}
              value={istoggle}
            />
          </View>
           <View
            style={
             styles.notificationText
            }
          >
            <View style={[styles.shadowContainerStyle, { width: "100%" }]}>
              <View style={styles.delivreyAddress}>
                <View style={{ width: "80%" }}>
                  <Text style={styles.descritpionText}>
                  {appTexts.NOTIFICATION.Descripton}
                  </Text>
                </View>
                <View style={{ width: "28%" }}>
                  <Text style={styles.timeText}>{appTexts.NOTIFICATION.Type} </Text>
                </View>
              </View>
              <View
                style={styles.descritpionView}
              >
                <Text style={styles.completedescritpionText}>
                {appTexts.NOTIFICATION.Body}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

NotificationView.propTypes = {
  onValueChange: PropTypes.func,
  istoggle: PropTypes.bool,
};

export default NotificationView;
