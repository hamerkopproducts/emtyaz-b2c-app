import React, { useState, useRef } from 'react';
import { View, I18nManager} from 'react-native';
import StatusDetailsCard from '../../components/StatusDetailsCard'
import Header from "../../components/Header";
import globals from "../../lib/globals";
import { styles } from "./styles";

const OrderStatusDetailsView = (props) => {
	const {
		onBackButtonPress,
		listData,
		tabIndex,
	} = props;

	console.log(tabIndex)

	//const renderItem = ({ item, index }) => 
//   <MyOrdersCard item={item} itemClick={itemClick} tabIndex={tabIndex} />;
	return (

		<View style={{flex:1}}>
			 <Header
        navigation={props.navigation}
		isLogoRequired={false}
		isBackButtonRequired={true}
		onBackButtonPress={onBackButtonPress}
        headerTitle={I18nManager.isRTL ? "طلباتي" : "My Orders"}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
			<StatusDetailsCard tabIndex={tabIndex} />
		{/* <FlatList 
		style= {styles.flatList}
		data = {listData}
		extraData = {listData}
		renderItem={renderItem}
		/> */}
		</View>


	);
};

OrderStatusDetailsView.propTypes = {

};

export default OrderStatusDetailsView;
