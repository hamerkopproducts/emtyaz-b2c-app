import PropTypes from 'prop-types';
import React, { useCallback, useState} from 'react';
import StatusView from './OrderStatusDetailsView'
const OrderStatusDetails = (props) => {

  const {tabIndex,
      } = props; 
      const onBackButtonPress = () => {
        props.navigation.goBack()
       };
       console.debug( props.route.params.tabIndex )
  return (
      <StatusView 
      tabIndex={props.route.params.tabIndex}
      onBackButtonPress={onBackButtonPress}/>
  );
};

OrderStatusDetails.propTypes = {
 
};

export default OrderStatusDetails;
