import { StyleSheet } from "react-native";
import globals from "../../lib/globals";
const styles = StyleSheet.create({
    screenMain:{
        flex:1,
        flexDirection: 'row',
        padding: 10,
    },
    flatList: {
        flex: 1,
        justifyContent: 'center'
    }
})