import * as ActionTypes from '../actions/types'

const defaultLoginState = {
    userData: {},
    isLogged: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    success: undefined
};

export default function loginReducer(state = defaultLoginState, action) {
    switch (action.type) {
        case ActionTypes.LOGIN_SERVICE_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case ActionTypes.LOGIN_SERVICE_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.LOGIN_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                userData: action.responseData,
                isLogged: true
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                userData: {},
                isLogged: false,
                isLoading: false,
                error: undefined,
                success: undefined,
                resetNavigation: action.resetNavigation,
                isSessionExpired: true
            };
        default:
            return state;
    }
}
