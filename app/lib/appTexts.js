import { I18nManager } from "react-native";
import { displayName as appName } from "../../app.json";

module.exports = {
  SCREEN_TITLE: {
    loginScreenTitle: "Login",
  },
  STRING: {
    appName: appName,
    login: I18nManager.isRTL ? "تسجيل الدخول" : "Login",
    home: I18nManager.isRTL ? "الصفحة الرئيسية" : "Home",
    products: I18nManager.isRTL ? "منتجات" : "Products",
    search: I18nManager.isRTL ? "Search" : "Search",
    myOrders: I18nManager.isRTL ? "طلباتي" : "My Orders",
    myProfile: I18nManager.isRTL ? "ملفاتي" : "My Profile",
    Location:I18nManager.isRTL ? "تحديد الموقع" : "Select Location",
  },
  ALERT_MESSAGES: {
    specifyUsername: "Enter Username.",
    specifyPassword: "Enter Password.",
    specifyCreatePassword: "Enter Create Password.",
    specifyNewPassword: "Enter New Password.",
    specifyConfirmPassword: "Enter Confirm Password.",
    passwordNotMatched: "New Password and Confirm Password entries must match.",
    passwordNotValid: "Enter a valid Password",
    specifyCurrentPassword: "Enter Current Password.",
    noInternet: "No Network Connection",
    touchIDDisabled: "Touch ID login is disabled in your application.",
    specifyFriendlyName: "Enter Friendly name.",
    invalidPin: "Invalid Pin.",
    specifyPin: "Enter Pin.",
  },
  SELECT_LANGUAGE: {
    Chooseyourlanguage: I18nManager.isRTL
      ? "Please choose  your language"
      : "Please choose  your language",
    En: I18nManager.isRTL ? "ENGLISH" : "ENGLISH",
    AR: I18nManager.isRTL ? "عربي" : "عربي",
  },
  WALKTHROUGH_SLIDER: {
    Heading: I18nManager.isRTL
      ? "الخطوة 1 : قم بالتسجيل للحصول على تسعيرة خاص بك"
      : "Step 1 : Register as a user",
    Descritpion: I18nManager.isRTL
      ? "الصفحة االصفحة الرئيسيةالصفحة الرئيسيةالصفحة الرئيسيةالصفحة الرئيسيةلصفحةالصفحةالصفحةالرئيسية"
      : "It is a long established fact that a reader will be distracted by the readable content of a page when lookin",
    Skip: I18nManager.isRTL ? "تخطي" : "Skip",
    Next: I18nManager.isRTL ? "طلتي" : "Next",
  },
  OTP: {
    Heading: I18nManager.isRTL ? "تحقق من حسابك" : "Verify Your Account",
    Description: I18nManager.isRTL
      ? "تم إرسال رمز التحقق إلى رقم هاتفك. اكتب رمز التحقق الذي تم استلامه وتحقق منه."
      : "A verification code has been sent to your phone number.Type the verification code received and verify",
    Dontreceive: I18nManager.isRTL
      ? "لم أستلم الرمز؟ "
      : "Dont receive the code ?",
    Resend: I18nManager.isRTL ? "إعادة إرسال OTP" : "Resend OTP",
    VERIFY: I18nManager.isRTL ? "التحقق" : "VERIFY",
	RESEND: I18nManager.isRTL ? "إعادة إرسال" : "Resend",
	Enter: I18nManager.isRTL ? "إعادة إرسال" : "Enter Number",
  },
  SIGNIN: {
    Heading: I18nManager.isRTL ?  "مرحباً بالمستخدم" : "Welcome User",
    Description: I18nManager.isRTL
      ? "تسجيل الدخول باستخدام رقم الجوال"
      : "Sign in using the mobile number",
    Dontreceive: I18nManager.isRTL ? "الصفحة السية" : "Dont receive the code ?",
    Resend: I18nManager.isRTL ? "الصفحة اسية" : "   Resend OTP",
    ORWITH: I18nManager.isRTL ? "الصفحة الرية" : "OR SIGN IN WITH",
    New: I18nManager.isRTL ? "مستخدم جديد؟" : "New User? ",
    SIGN: I18nManager.isRTL ? "التسجيل" : "SIGN UP",
    GUEST: I18nManager.isRTL ? "الصفحةة" : "CONTINUE AS GUEST",
    PHONE: I18nManager.isRTL ? "رقم الجوال" : "Mobile Number*",
    SIGNIN: I18nManager.isRTL ? "تسجيل الدخول" : "SIGN IN",
  },
  PRIVACY: {
    Heading: I18nManager.isRTL
      ? "الصفحة الرئيسية"
      : "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
  },
  FAQ_CONTENT: {
    Title: I18nManager.isRTL
      ? " وانتظار عميلتك"
      : "Ut porta diam et ornare tempus.?",
   
	  Content: I18nManager.isRTL
      ? " وانتظاعليك الموافقة على الحجوزات في ثوار عميلتك"
	  : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur sunt itaque adipisci quisquam pariatur qui, reiciendis architecto quod sint incidunt labore nisi totam illum numquam non magnam praesentium, maxime quaerat!",
	  Head: I18nManager.isRTL
      ? " وانتظاعليك الموافقة على الحجوزات في ثوار عميلتك"
	  : "Ut porta diam et ornare tempus?",
	  Descritpion: I18nManager.isRTL
      ? " وليك الموافقة على الحجوزليك الموافقة على الحجوزليك الموافقة على الحجوزانتظاعليك الموافقة على الحجوزات في ثوار عميلتك"
      : "Many desktop publishingpackages and web page editors now use Lorem Ipsum as their default modeltext, and a search for 'lorem ipsum' will uncovermany web sites still intheir infancy.",
  },
  CATEGORY_CONTENT: {
    Title: I18nManager.isRTL ? " Food cuboard " : "Food Cupboard",
    Content: I18nManager.isRTL ? "Fresh Food" : "Fresh Food",
    Button1:I18nManager.isRTL ? "تطبيق" : "APPLY",
    Button2:I18nManager.isRTL ? "اعاده" : "RESET",
    Title2:I18nManager.isRTL ? "Dibba Al Fujairah":'Dibba Al Fujairah',
    Title3:I18nManager.isRTL ? "Dibba Al Fujaira":'Dibba Al Fujairah',
    Heading: I18nManager.isRTL ? " أقسام" : "Categories",
    SubHeading:I18nManager.isRTL ? "Fresh Food" : "Fresh Food",
    Category:I18nManager.isRTL ? "Category" : "Category",
    Brands:I18nManager.isRTL ? "Brands" : "Brands",
    BrandOne:I18nManager.isRTL ? "Al-Ain" : "Al-Ain",
    BrandTwo:I18nManager.isRTL ? "Al Jazeera" : "Al Jazeera",
    BrandThree:I18nManager.isRTL ? "Al Khazna" : "Al Khazna",


  },

  SIGNUP: {
    Heading: I18nManager.isRTL ? "مرحباً بالمستخدم" : "Welcome User",
    Description: I18nManager.isRTL
      ? "تعبئة النموذج للتسجيل"
      : "Fill the form to sign up",
    Fullname: I18nManager.isRTL ? "الاسم الكامل" : "Full Name*",
    Email: I18nManager.isRTL ? "عنوان البريد الإلكتروني" : "Email Address*",
    Phone: I18nManager.isRTL ? "رقم الجوال" : "Mobile Number*",
    compnayname: I18nManager.isRTL ? "اسم الشركة" : "Company Name",
    companynumber: I18nManager.isRTL
      ? "رقم السجل التجاري"
      : "Company registration number",
    GUEST: I18nManager.isRTL ? "الصفحةة" : "Continue as Guest",
    PHONE: I18nManager.isRTL ?  "رقم الجوال" : "Mobile Number*",
    SIGNIN: I18nManager.isRTL ? "الصفحةة" : "SIGN IN",
    Terms: I18nManager.isRTL ? "أوافق على" : "I accept the",
    Condition: I18nManager.isRTL ? "الشروط والأحكام" : "terms and conditions",
    NEXT: I18nManager.isRTL ? 'التالي'  : "NEXT",
    VAT:I18nManager.isRTL ? 'رقم الضريبي'  : "VAT Registration Number",
  },
  PROFILEITEM: {
    FAQs: I18nManager.isRTL ? "الأسئلة المتكرر" : "FAQs",
    PrivacyPolicy: I18nManager.isRTL ? "سياسة الخصوصية" : "Privacy Policy",
    Terms: I18nManager.isRTL ? "الشروط والأحكام" : "Terms and Conditions",
    Support: I18nManager.isRTL ? "دعم" : "Support",
    Share: I18nManager.isRTL ? "مشاركة التطبيق" : "Share App",
	Logout: I18nManager.isRTL ? "الخروج" : "Logout",
  Choosecat:I18nManager.isRTL ? "اختر القسم" : "Choose Category",
  Favorites:I18nManager.isRTL ? "المفضلة" : "Favorites",
  Requestproduct:I18nManager.isRTL ? "Request a product" : "Request a product",
  Address:I18nManager.isRTL ? "Address Details" : "Address Details",
  subAd:I18nManager.isRTL ? "Manage Delivery Address/Billing Address" : "Manage Delivery Address/Billing Address",
  myOrders:I18nManager.isRTL ? "My Orders" : "My Orders",
  },
  ADDRESSDETAILS: {
    Deliverinformation:I18nManager.isRTL ? "معلومات التوصيل" : "Deliver Information",
	Heading: I18nManager.isRTL ? "تفاصيل العنوان" : "Address Details",
    DeliveryAddress: I18nManager.isRTL ? "عنوان التوصيل" : "Delivery Address",
    BillingAddress: I18nManager.isRTL ? "عنوان الفوترة" : "Billing Address",
    AddNew: I18nManager.isRTL ? "+ إضافة جديد" : "Add New +",
    AddNewAddress: I18nManager.isRTL ? "إضافة عنوان جديد" : "Add New Address",
    Name: I18nManager.isRTL ? "الصفحة الرئيسية" : "Name*",
    Apartment: I18nManager.isRTL ? "شقه" : "Apartment*",
    Name: I18nManager.isRTL ? "اسم" : "Name*",
    Street: I18nManager.isRTL ? "الشارع" : "Street",
    POBox: I18nManager.isRTL ? "الصفحة الرئيسية" : "PO Box",
    Landmark: I18nManager.isRTL ? "معلما" : "Landmark",
    POBox: I18nManager.isRTL ? "صندوق بريد" : "PO Box",
    Region: I18nManager.isRTL ? "المنطقة" : "Region",
    setas:I18nManager.isRTL ? "تعيين كـعنوان افتراضي" : "Set as default address",
    Cancel: I18nManager.isRTL ? "إلغاء" : "CANCEL",
    Save: I18nManager.isRTL ? "حفظ" : "SAVE",
  },
  ORDER_STATUS: {
    Reorder: I18nManager.isRTL ? "اعاده الطلب" : "REORDER",
    Pending: I18nManager.isRTL ? "تحت المراجع" : "Pending",
  },
  THANKUORDER_VIEW: {
    Thanku: I18nManager.isRTL ? "شكرًا لك!" : "THANK YOU",
    Orderdecritpion: I18nManager.isRTL
      ? "تم أصدر الطلب."
    : "Order has been created",
    Ordernote: I18nManager.isRTL ? "ملاحظة الطلب": "Order Note",
    Orderdecritpionemail: I18nManager.isRTL
    ? "تم إرسال تأكيد الطلب إلى البريد الإلكتروني الخاص بك."
  : " Order Confirmation has been sent to your Email.",
	  Quotationdecritpion: I18nManager.isRTL
      ? " تم إنشاء التسعيرة."
      : "Quotation has been created.",
      QuotationdecritpionEmail: I18nManager.isRTL
      ? "تم إرسال رسالة التسعيرة إلى عنوان بريدك الإلكتروني."
      : "Quotation Email sent to your email address. ",
    Orderstatus: I18nManager.isRTL ? "حالة الطلب : " : "Order Status :  ",
    OrderSummary: I18nManager.isRTL ? "ملخص الطلب" : "Order Summary",
	Orderid: I18nManager.isRTL ? "رقم الطلب : " : "Order ID: ",
	Quotationstatus: I18nManager.isRTL ? "حالة التسعيرة : " : "Quotation Status :  ",
	QuotationDelivery: I18nManager.isRTL ? "موقع التوصيل : " : " Delivery Location :  ",
    Quotationid: I18nManager.isRTL ? "رقم التسعيرة : " : "Quotation ID :",
    Deliverytime: I18nManager.isRTL ? "وقت التوصيل : " : "Delivery time:",
    Paymenttime: I18nManager.isRTL ? "الصفحة الرئيسية : " : "Payment Methode:",
    BIlldeatils: I18nManager.isRTL ? "تفاصيل الفاتورة" : "Bill Details",
    Subtotal: I18nManager.isRTL ? "المجموع الفرعي" : "Subtotal",
    DeliveryCharge: I18nManager.isRTL ? "رسوم التوصيل" : "Delivery Charge",
    CODFee: I18nManager.isRTL ? "رسوم الدفع عند الاستلام" : "COD Fee",
    TotalInclusivefVAT: I18nManager.isRTL
      ? "المجموع (مع القيمة المضافة)"
	  : "Total (Inclusive of VAT)",
	  VIEWORDER: I18nManager.isRTL ? "عرض الطلب" : "VIEW ORDER",
	  VIEWQUOATATION: I18nManager.isRTL ? "إظهار التسعيرة" : "VIEW QUOATATION",
    CONTINUESHOPPING: I18nManager.isRTL ? "استمرار التسوق" : "CONTINUE SHOPPING", 
    
  },
  PWD_CHANGE: {
    Rest: I18nManager.isRTL ? "إعادة تعيين كلمة السر" : "Reset Password",
    cpassword: I18nManager.isRTL ? "كلمة السر الحالية" : "Current Password",
    npassword: I18nManager.isRTL ? "كلمة السر جديدة" : "New Password  ",
    conpassword: I18nManager.isRTL ? "تأكيد كلمة السر" : "Confirm Password",
    account: I18nManager.isRTL ? "حسابك" : "My Account",
    updatepassword: I18nManager.isRTL ? "تحديث كلمة السر" : "UPDATE PASSWORD",
  },
  LOGOUT: {
    Message: I18nManager.isRTL
      ? "هل تريد بالتأكيد تسجيل الخروج ؟"
      : "Are you sure want to logout ?",
    No: I18nManager.isRTL ? "لا" : "NO",
    Yes: I18nManager.isRTL ? "نعم" : "YES`",
  },
  SUPPORT: {
    Message: I18nManager.isRTL ? "نحن هنا للمساعدة" : "We are here to help",
    Order: I18nManager.isRTL ? "مشكلة الطلب" : "Order issue",
    Type: I18nManager.isRTL ? "اكتب هنا" : "Type here",
    Send: I18nManager.isRTL ? "سال" : "SEND",
    contact:I18nManager.isRTL ? "اتصل بنا على" : "Contact us on",
    orderissue:I18nManager.isRTL ? "مشكلة الطلب: " : "Order issues",
  },
  NOTIFICATION: {
    Message: I18nManager.isRTL ? "إعدادات الإشعارات" : "Notification Settings",
    Notication: I18nManager.isRTL ? "الإشعارات" : "Notification",
    Descripton: I18nManager.isRTL ? "تم قبول طلب قبول طلب قبول طلبك" : "Your order has been accepted",
    Type: I18nManager.isRTL ? " اك هن ا2 " : "2 hrs ago",
    Body: I18nManager.isRTL ? "إع قبول طلب قبول طلب قبول طلبدادات الإشعارات" : "Driver info: Ahmed moosa Delivery schedule 12th july 2020, 2pm- 5pm sasass",
  },
  CHAT:{
	Type: I18nManager.isRTL ? "الرد على المسؤول" : "Reply to admin",
	Send: I18nManager.isRTL ? "سال" : "SEND",
	placeholder: I18nManager.isRTL ? "اكتب شيئاً" : "Type something",
  },
  EDIT_PROFILE:{
    Heading: I18nManager.isRTL ? "تعديل التفاصيل" : "Edit Your Details",
    Fullname: I18nManager.isRTL ? "الاسم الكامل" : "Full Name*",
    Email: I18nManager.isRTL ? "عنوان البريد الإلكتروني" : "Email Address*",
    compnayname: I18nManager.isRTL ? "اسم الشركة" : "Company Name*",
    PHONE: I18nManager.isRTL ? "رقم الجوال" : "Mobile Number*",
    Save: I18nManager.isRTL ? "حفظ" : "SAVE",
    City:I18nManager.isRTL ? "المدينة" : "City",
    },
    BILLINGDELIVERY:{
      Billing: I18nManager.isRTL ? "عنوان الفوترة" : "Billing Address",
      Delivery:I18nManager.isRTL ? "عنوان التوصيل" : "Delivery Address",
      AddNewAddress:I18nManager.isRTL ? "إضافة عنوان جديد" : "Add New Address",
    },
    USER_DATA:{
      name: I18nManager.isRTL ? "شارع النعيمية" : "Al Naemia Street",
      po: I18nManager.isRTL ? "صندوق البريد: 2211" : "PO Box: 2211",
      place: I18nManager.isRTL ? "النميم، جدة" : "Al Nameem,Jaddah",
      number: I18nManager.isRTL ? "النميم، جدة" : "+966 123330",
     
    },
    MY_ORDERS:{
      MyOrders: I18nManager.isRTL ? "طلباتك" : "Upcoming Orders",
      MyQuotations: I18nManager.isRTL ? "My Quotations" : "Completed Orders",
      Filter: I18nManager.isRTL ? "تصفيه" : "FILTER",
      Sort: I18nManager.isRTL ? "Sort" : "Sort",
      DeliverySchedule: I18nManager.isRTL ? "جدول التوصيل" : "Delivery Schedule",
      OrderAmount:I18nManager.isRTL ? "مبلغ الطلب" : "Order Amount",
      QuotationDate:I18nManager.isRTL ? "تاريخ التسعير" : "Delivery Date",
      QuotationAmount:I18nManager.isRTL ? "مبلغ التسعير": "Order Amount",
      heading:I18nManager.isRTL ? "My Orders": "My Orders",
    },
    RATING:{
      Heading: I18nManager.isRTL ? "كيف كانت خدمتنا" : "How Was Our Service ?",
      Quality: I18nManager.isRTL ? "جودة" : "Quality",
      Supportfromteam: I18nManager.isRTL ? "دعم من فريق الفني" : "Support from team",
      DeliveryTime: I18nManager.isRTL ? "وقت التوصيل" : "Delivery Time", 
      Yourreview: I18nManager.isRTL ? "ما مدى رضائك على خدمتنا" : "Your review",
      HeadingEdit: I18nManager.isRTL ? "كيف كانت خدمتنا" : "Add review",
    },
    CANCELMODAL:{
      Heading: I18nManager.isRTL ? "سبب الإلغاء" : "Reason For Cancelling",
      Reason: I18nManager.isRTL ? "تحديد سبب الإلغاء" : "Select Cancellation Reason",
      Typereason: I18nManager.isRTL ? "اكتب سببك" : "Type Your Reason",
      Done: I18nManager.isRTL ? "تم" : "DONE", 
      
    },
    MYQUATION:{
      MyQuotations: I18nManager.isRTL ? "My Quotations" : "My Quotations",
      Requested: I18nManager.isRTL ? "تم رفع طلبك" : "Requested",
      Approved:I18nManager.isRTL ? "تم الموافقة" : "Approved",
      Items: I18nManager.isRTL ? "سلع" : "items",
      DeliveryLocation: I18nManager.isRTL ? "موقع التوصيل :" : "Delivery Location : ",
      QUNOTE:  I18nManager.isRTL ? "ملاحظة" : "Quotation Note ",
      ExpectedOrderDeliveryDate: I18nManager.isRTL ? "تاريخ توصيل المتوقع:" : "Expected Order Delivery Date", 
      MODIFYQUOTAION: I18nManager.isRTL ? "تعديل التسعيرة" : "MODIFY QUOTAION",
      CANCELQUOTATION: I18nManager.isRTL ? "إلغاء التسعيرة" : "CANCEL QUOTATION",
      Nos: I18nManager.isRTL ? "nos" : "nos", 
      QuotationSummary: I18nManager.isRTL ? "ملخص التسعير" : "Quotation Summary", 
      AddQuotationnote: I18nManager.isRTL ? "إضافة ملاحظة (اختياري)" : "Add Quotation Note (optional)", 
      PayNow: I18nManager.isRTL ? "الدفع" : "PAY NOW",
    },
    DETAILSSCREEN:{
      HeadingText:I18nManager.isRTL ? "تحديد الحجم" : "Select a Variant",
      OptionOne:I18nManager.isRTL ? "1 Kg":"1 Kg",
      OptionTwo:I18nManager.isRTL ? "2 Kg" : "2 Kg",
      OptionThree:I18nManager.isRTL ? "3 Kg" : "3 Kg",
      OptionFour:I18nManager.isRTL ? "5 Kg" : "5 Kg",
      OptionFive:I18nManager.isRTL ? "8 Kg" : "8 Kg",
      OptionSix:I18nManager.isRTL ? "10 Kg" : "10 Kg",
      heading:I18nManager.isRTL ? "Quantity" : "QUANTITY",
      Location:I18nManager.isRTL ? "موقع التوصيل : " : "Delivery Location: ",
      Charge:I18nManager.isRTL ? "رسوم التوصيل : " : "Delivery Charge: ",
      Time:I18nManager.isRTL ? "وقت التوصيل المتوقع : " : "Expected Delivery Time: ",
      Description:I18nManager.isRTL ? "وصف" : "Description",
      Add:I18nManager.isRTL ? "إضافة إلى المفضلة" : "Add to Favorites",
      Share:I18nManager.isRTL ? "شارك" : "Share",
      Cart:I18nManager.isRTL ? "إضافة إلى العربة" : "Add to Cart",
      Specification:I18nManager.isRTL ? "المواصفات" : "Specification",
      similar:I18nManager.isRTL ? "Similar Products" : "Similar Products",
      see:I18nManager.isRTL ? "See All" : "See All",
      last:I18nManager.isRTL ? "Last viewed Products" : "Last viewed Products",
      Review:I18nManager.isRTL ? "Reviews" : "Reviews",
    },

    HOMETEXT: {
      seeall: I18nManager.isRTL ? "شاهد الكل" : "See all",
      shopcategory: I18nManager.isRTL ? "تسوق حسب القسم" : "Shop by category",
      promotionalitems: I18nManager.isRTL ? "عروض" : "Promotional Items",
      change: I18nManager.isRTL ? "تغيير" : "Change",
    },
    MOREITEM:{
      more: I18nManager.isRTL ? "المزيد" : "More",

    },
    QUOTATIONS_SUMMERY:{
      QuotationSummary: I18nManager.isRTL ? "ملخص التسعير" : "Quotation Summary", 
      Subtotal: I18nManager.isRTL ? "المجموع الفرعي" : "Subtotal",
    DeliveryCharge: I18nManager.isRTL ? "رسوم التوصيل" : "Delivery Charge",
    Discount: I18nManager.isRTL ? "الخصم" : "Discount",
    CODFee: I18nManager.isRTL ? "رسوم الدفع عند الاستلام" : "COD Fee",
    TotalInclusivefVAT: I18nManager.isRTL
      ? "المجموع (مع القيمة المضافة)"
    : "Total (Inclusive of VAT)",
    Billing: I18nManager.isRTL ? "عنوان الفوترة" : "Billing Address",
    Deliveryaddress:I18nManager.isRTL ? "عنوان التوصيل" : "Delivery Address",
    Scheduled:I18nManager.isRTL ? "وقت الجدول" : "Scheduled Time",
    ScheduledOn: I18nManager.isRTL ? "الموعد المحدد" : "Scheduled On",
    Delivery: I18nManager.isRTL ? "موقع التوصيل : " : "Delivery Location : ",
    Deliverytime: I18nManager.isRTL ? "تاريخ ووقت التوصيل : " : "Delivery Date & Time :",
    SelectPaymentMethod: I18nManager.isRTL ? "طريقة الدفع " : "Select Payment Method",
    Confirm:I18nManager.isRTL ? "تأكيد" : "CONFIRM",
    Pay:I18nManager.isRTL ? "Pay Now" : "Pay Now",
    TOTALPAYABLEAMOUNT:I18nManager.isRTL ? "مجموع الدفع" : "TOTAL PAYABLE AMOUNT"
    },
    PACKSIZE:{
      Heading:I18nManager.isRTL ? "Select Pack Size" : "Select Pack Size",
      ButtonTitleOne:I18nManager.isRTL ? "حفظ" : "Save",
      ButtonTitleTwo:I18nManager.isRTL ? "اعاده" : "Reset",
      SizeOne:I18nManager.isRTL ? "SMALL" : "SMALL",
      SizeTwo:I18nManager.isRTL ? "وسط" : "MEDIUM",
      SizeThree:I18nManager.isRTL ? "كبير" : "LARGE",
    },
    QUANTITY:{
      Heading:I18nManager.isRTL ? " حدد الكمية" : "Quantity",
      BodyTextOne:I18nManager.isRTL ? "1 Kg" : "1 Kg",
      BodyTextTwo:I18nManager.isRTL ? "2 Kg" : "2 Kg",
      BodyTextThree:I18nManager.isRTL ? "5 Kg" : "5 Kg",
      BodyTextFour:I18nManager.isRTL ? "8 Kg" : "8 Kg",
      BodyTextFour:I18nManager.isRTL ? "10 Kg" : "10 Kg",
      close:I18nManager.isRTL ? "CLOSE" : "CLOSE",
    },
    BANNER:{
      WishList:I18nManager.isRTL ? " أضيفت إلى المفضلة" : "Added to your favorites",
      View:I18nManager.isRTL ? "View" : "View",
    },
    CART:{
      ContactSupport:I18nManager.isRTL ? "الاتصال بالدعم" : "CONTACT SUPPORT",
      CancelOrder:I18nManager.isRTL ? "إلغاء الطلب" : "CANCEL ORDER",
      ReviewOrder:I18nManager.isRTL ? "REVIEW ORDER" : "REVIEW ORDER",
      Items:I18nManager.isRTL ? "سلع العربة" : "Cart Items (2)",
      Remove:I18nManager.isRTL ? "Add From Save For Later" : "Add From Save For Later",
      AddFav:I18nManager.isRTL ? "Add from Wishlist" : "Add from Wishlist",
      Location:I18nManager.isRTL ? "موقع التوصيل : " : "Delivery Location : ",
      Notes:I18nManager.isRTL ? "إضافة ملاحظة (اختياري)" : "Add Quotation Note (optional)",
      ExpectedDate:I18nManager.isRTL ? "تاريخ توصيل المتوقع:" : "Expected Order Delivery Date",
      QuoteRequest:I18nManager.isRTL ? "REVIEW AND CHECKOUT" : "REVIEW AND CHECKOUT",
      HeadingCart:I18nManager.isRTL ? "عربة التسوق": "My Cart",
      Nos:I18nManager.isRTL ? "SAR 680 x 2" : "SAR 680 x 2",
      Noss:I18nManager.isRTL ? "  1 x 100حبه" : "100nos X1",
      Add:I18nManager.isRTL ?"Add Promo code" : "Add Promo code",
      Enter:I18nManager.isRTL ?"Enter Promo code" : "Enter promo code",
      APPLY:I18nManager.isRTL ?"تطبيق ": "APPLY",
      BillDetails:I18nManager.isRTL ?"تفاصيل الفاتورة ": "Bill Details",
      SubTotal:I18nManager.isRTL ? "المجموع الفرعي": "Sub Total",
      subTotalAmount:I18nManager.isRTL ? "SAR 160.00" : "SAR 160.00",
      DeliveryCharge:I18nManager.isRTL ? "رسوم التوصيل": "Delivery Charge",
      DeliveryAmount:I18nManager.isRTL ? "SAR 60.00" : "SAR 60.00",
      VAT:I18nManager.isRTL ? "المجموع (مع القيمة المضافة)": "Total (inclusive of VAT)",
      TaxAmount:I18nManager.isRTL ? "SAR 212.00" : "SAR 212.00",
      Addordernote:I18nManager.isRTL ? "Add Order-Note(Optional)" : "Add Order-Note(Optional)",

    },
    PRODUCT_FLITER:{
      Quantity:I18nManager.isRTL ? "Quantity" : "Quantity",
      Brands:I18nManager.isRTL ? "العلامة التجارية" : "Brands",
      packSize:I18nManager.isRTL ? "Pack Size" : "Pack Size",
      price:I18nManager.isRTL ? "Pack Range" : "Price Range",

    },
    VALIDATION_MESSAGE:{
      Type:I18nManager.isRTL ? "Required Field" : "Required Field",
      Mobile_error_msg:I18nManager.isRTL ? "Enter a valid mobile number" : "Enter a valid mobile number",
    },
    REQUEST:{
      Request:I18nManager.isRTL ? "Request a product" : "Request a Product",
      Name:I18nManager.isRTL ? "اسم" : "Name",
      Phone:I18nManager.isRTL ? "Phone" : "Phone",
      Add:I18nManager.isRTL ? "Add new Image" : "Add new Image",
      product:I18nManager.isRTL ? "منتجات" : "Product",
      Save:I18nManager.isRTL ? "حفظ" : "SAVE",
      Nishad:I18nManager.isRTL ? "Nishad Aliyar" : "Nishad Aliyar",
      PhoneNo:I18nManager.isRTL ? "+966 23455" : "+966 23455",
      Vanilla:I18nManager.isRTL ? "Vanilla Milk Shake" : "Vanilla Milk Shake",
    },
    SUMMARY:{
      heading:I18nManager.isRTL ? "Order Summary" : "Order Summary",
      item:I18nManager.isRTL ? "2 Items" : "2 Items",
      subtotal:I18nManager.isRTL ? "Subtotal" : "Subtotal",
      sar:I18nManager.isRTL ? "SAR 120" : "SAR 120",
      discount:I18nManager.isRTL ? "Discount" : "Discount",
      delivery:I18nManager.isRTL ? "Delivery Charge" : "Delivery Charge",
      sars:I18nManager.isRTL ? "SAR 12" : "SAR 12",
      sarss:I18nManager.isRTL ? "SAR 12" : "SAR 12",
      tot:I18nManager.isRTL ? "Total(Inclusive of VAT)" : "Total(Inclusive of VAT)",
      sartot:I18nManager.isRTL ? "SAR 160.00" : "SAR 160.00",
      delAd:I18nManager.isRTL ? "Delivery Address" : "Delivery Address",
      billAd:I18nManager.isRTL ? "Billing Address" : "Billing Address",
      locCal:I18nManager.isRTL ? "Delivery Location:Jeddah Al Nameem" : "Delivery Location:Jeddah Al Nameem",
      date:I18nManager.isRTL ? "Delivery Date and Time : " : "Delivery Date and Time : ",
      time:I18nManager.isRTL ? "12 July 2020, 2PM - 5PM" : "12 July 2020, 2PM - 5PM",
      Amount:I18nManager.isRTL ? "TOTAL PAYABLE AMOUNT" : "TOTAL PAYABLE AMOUNT",
      amount:I18nManager.isRTL ? "SAR 160.00" : "SAR 160.00",
    },
    SAVE:{
      heading:I18nManager.isRTL ? "SAVE FOR LATER" : "SAVE FOR LATER",
      favourites:I18nManager.isRTL ? "FAVOURITES" : "FAVOURITES",
    }
};
