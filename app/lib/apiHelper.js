import globals from "./globals"

let productionURL = '';
let developmentURL = '';
let baseURL = developmentURL;
let loginAPI = '/user/login';


const apiHelper = {
  getAPIHeaderForDelete: (params,token) => {
  	const AuthStr = 'Bearer '.concat(token);
    let headerConfig = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthStr
      },
      data: params
    };
    return headerConfig;
  },
  getAPIHeader: (token) => {
  	const AuthStr = 'Bearer '.concat(token);
    let headerConfig = { headers: { Authorization: AuthStr } };
    return headerConfig;
  },
  getLoginAPI: () => {
    return baseURL + loginAPI;
  }
}

export default apiHelper;
