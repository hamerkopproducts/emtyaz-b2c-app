export const LOGIN_SERVICE_LOADING = 'login_service_loading'
export const LOGIN_SERVICE_ERROR = 'login_service_error'
export const LOGIN_SUCCESS = 'login_success'
export const LOGOUT_SUCCESS = 'logout_success'

export const SERVICE_LOADING = 'service_loading'
export const SERVICE_ERROR = 'service_error'
export const GET_ORDER_SUCCESS = 'get_order_success'



