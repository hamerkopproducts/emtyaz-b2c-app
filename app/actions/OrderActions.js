import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper";
//import axios from 'react-native-axios';
import globals from "../lib/globals"




export const apiServiceActionLoading = () => ({
	type: ActionTypes.LOGIN_SERVICE_LOADING
});

export const apiServiceActionError = (error) => ({
	type: ActionTypes.LOGIN_SERVICE_ERROR,
	error: error
});



export function orderActionSuccess(responseData) {
	return {
		type: ActionTypes.GET_ORDER_SUCCESS,
		responseData: responseData
	};
}

export function getOrders(params) {
	let listData = [
		{
			id: 1,
			orderId: 'QUOT123456789',
			status: 'Pending',
			orderAmount: 'SAR 120',
			deliveryTime:'20 June 2020, 2pm to 5pm'

		},
		{
			id: 2,
			orderId: 'QUOT123456710',
			status: 'Accepted',
			orderAmount: 'SAR 120',
			deliveryTime: '20 June 2020, 2pm to 5pm'

		},
		{
			id: 3,
			orderId: 'QUOT123456711',
			status: 'Delivery Scheduled',
			orderAmount: 'SAR 120',
			deliveryTime: '20 June 2020, 2pm to 5pm'

		},
		{
			id: 4,
			orderId: 'QUOT123456712',
			status: 'Delivered',
			orderAmount: 'SAR 120',
			deliveryTime: '20 June 2020, 2pm to 5pm'

		},
		{
			id: 5,
			orderId: 'QUOT123456713',
			status: 'Pending',
			orderAmount: 'SAR 120',
			deliveryTime: '20 June 2020, 2pm to 5pm'

		},
		{
			id: 6,
			orderId: 'QUOT123456713',
			status: 'Requested',
			orderAmount: 'SAR 120',
			deliveryTime: '20 June 2020, 2pm to 5pm'

		},
		{
			id: 7,
			orderId: 'QUOT123456713',
			status: 'Approved',
			orderAmount: 'SAR 120',
			deliveryTime: '20 June 2020, 2pm to 5pm'

		}];
	return async dispatch => {
		//dispatch(apiServiceActionLoading());
		dispatch(orderActionSuccess(listData));
	}
}


