import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import { defaultLoginState } from "../reducers/loginReducer";
//import axios from 'react-native-axios';
import globals from "../lib/globals"




export const apiServiceActionLoading = () => ({
	type: ActionTypes.LOGIN_SERVICE_LOADING
});

export const apiServiceActionError = (error) => ({
	type: ActionTypes.LOGIN_SERVICE_ERROR,
	error: error
});



export function loginServiceActionSuccess(responseData) {
	return {
		type: ActionTypes.LOGIN_SUCCESS,
		responseData: responseData
	};
}



export function doLogin(params) {
	return async dispatch => {
		//dispatch(apiServiceActionLoading());
		dispatch(loginServiceActionSuccess({}));
	}
}
export const introFinished = () => ({
	type: ActionTypes.INTRO_FINISHED
});

export function resetLoginErrorMeesage() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.RESET_LOGIN_ERROR_MESSAGE
		});
	};
}

export function doLogout(resetNavigation: Function) {
	return dispatch => {
		dispatch({
			type: ActionTypes.LOGOUT_SUCCESS,
			...defaultLoginState,
			resetNavigation
		});
	};
}

export function enableLogin() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.ENABLE_LOGIN_SUCCESS
		});
	};
}
