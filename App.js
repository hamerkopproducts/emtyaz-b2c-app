/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, { useEffect } from "react";
import {
  SafeAreaView,
  StatusBar,
  View,
  I18nManager
} from 'react-native';
import globals from "./app/lib/globals";
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import SplashScreen from 'react-native-splash-screen';
import { HomeContainer, LoginContainer, LanguageContainer } from './app/lib/navigationRoutes';
import Toast from 'react-native-toast-message';

const App: () => React$Node = (props) => {

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    console.log('B2C Running');
    setTimeout(() => {
      SplashScreen.hide();
    }, 2000);
  };
  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    }
  }, []);

  const handleComponentUnmount = () => {
    
  };


  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
   
  };

  return (
    <>
      {props.isLogged ? <SafeAreaView style={{ flex: 0, backgroundColor: globals.COLOR.headerColor }} /> : null}
      <SafeAreaView style={{ flex: 1, backgroundColor: globals.COLOR.screenBackground }}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor={globals.COLOR.screenBackground}
        />
        <View style={{ width: '100%', height: '100%' }}>
         <HomeContainer />
        </View>
      </SafeAreaView>
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </>
  );
};

const mapStateToProps = (state, props) => {
  return {
    isLogged: state.loginReducer.isLogged
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
   
  }, dispatch)
};

const appWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

appWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default appWithRedux;